﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class Contacts
    {
        public string Email { set; get; }
        public string Phone { set; get; }
        public List<ContactTranslator> Address { set; get; }
        public List<ContactTranslator> Description { set; get; }
    }

    public class ContactTranslator
    {
        public string Language { set; get; }
        public string Text { set; get; }
    }
}
