﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class Order
    {
        public int OrderId { set; get; }
        public string Name { set; get; }
        public string Phone { set; get; }
        public string Email { set; get; }
        public string Clarification { set; get; }
        public string Url { set; get; }
        public string Message { set; get; }
        public string ProductName { set; get; }
        public string ProductUrl { set; get; }
        public DateTime Date { set; get; }
    }
}
