﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class Image
    {
        public int ImageId { set; get; }
        public string Path { set; get; }
        public ImageTranslator ImageTranslator { set; get; }
    }

    public class ImageTranslator
    {
        public int TranslatorId { set; get; }
        public string Code { set; get; }
        public string Alt { set; get; }
        public string Description { set; get; }
    }
}
