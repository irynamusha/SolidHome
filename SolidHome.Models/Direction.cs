﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class Direction
    {
        public int DirectionId { set; get; }
        public string Url { set; get; }
        public string IconStyle { set; get; }
        public string Title { set; get; }
        public string Description { set; get; }
        public string SmallDescription { set; get; }
        
    }
}
