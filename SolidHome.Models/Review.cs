﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class Review
    {
        public int ReviewId { set; get; }
        public string Content { set; get; }
        public string Email { set; get; }
        public DateTime ApproveDate { set; get; }
        public DateTime CreateDate { set; get; }
        public bool Approved { set; get; }
        public string UserName { set; get; }
    }
}
