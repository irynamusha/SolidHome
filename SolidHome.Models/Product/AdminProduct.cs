﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class AdminProduct
    {
        public int ProductId { set; get; }
        public string MainImage { set; get; }
        public SmallDirection Direction { set; get; }
        public List<AdminImage> PreviewImages { set; get; }
        public List<Translator> ProductTranslators { set; get; }
        public List<AdminProductDescription> ProductDescriptions { set; get; }
    }
}
