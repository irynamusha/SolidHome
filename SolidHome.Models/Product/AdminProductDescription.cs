﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class AdminProductDescription
    {
        public int ProductId { set; get; }
        public int ProductDescriptionId { set; get; }
        public int Price { set; get; }
        public List<Translator> ProductDescriptionTranslators { set; get; }

    }
}
