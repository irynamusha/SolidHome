﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class ProductPreview
    {
        public int ProductId { set; get; }
        public string Title { set; get; }
        public int Price { set; get; }
        public string Url { set; get; }
        public string MainImage { set; get; }
        public string MainImageAlt { set; get; }
    }
}
