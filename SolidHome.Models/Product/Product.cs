﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class Product
    {
        public int ProductId { set; get; }
        public Translator ProductTranslator { set; get; }
        public Image MainImage { set; get; }
        public SmallDirection Direction { set; get; }
        public List<ProductDescription> ProductDescriptions { set; get; }
        public List<Image> PreviewImages { set; get; }

    }
}
