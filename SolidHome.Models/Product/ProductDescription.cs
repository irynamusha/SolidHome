﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class ProductDescription
    {
        public int ProductDescriptionId { set; get; }
        public Translator ProductTranslator { set; get; }
        public int Price { set; get; }
    }
}
