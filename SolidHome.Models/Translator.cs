﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class Translator
    {
        public int TranslatorId { set; get; }
        public string Url { set; get; }
        public string Title { set; get; }
        public string Code { set; get; }
        public string MainImageAlt { set; get; }
        public string Description { set; get; }
        public string CompanyName { set; get; }
        public string SmallDescription { set; get; }
        public string MetaTitle { set; get; }
        public string MetaDescription { set; get; }
    }
}
