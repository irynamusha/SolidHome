﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class Message
    {
        public int MessageId { set; get; }
        public string UserName { set; get; }
        public string Email { set; get; }
        public string Content { set; get; }
        public DateTime Date { set; get; }
    }
}
