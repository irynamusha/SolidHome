﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models.Works
{
    public class AdminWork
    {
        public int WorkId { set; get; }
        public DateTime WorkDate { set; get; }
        public string MainImage { set; get; }
        public List<AdminImage> WorkImages { set; get; }
        public List<Translator> WorkTranslators { set; get; }

        public List<SmallDirection> Directions { set; get; }
    }
}
