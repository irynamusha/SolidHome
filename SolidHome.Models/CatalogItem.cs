﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class CatalogItem
    {
        public int DirectionId { set; get; }
        public string Title { set; get; }
        public string Description { set; get; }
        public string Url { set; get; }
        
        public List<ProductPreview> Products { set; get; }
    }
}
