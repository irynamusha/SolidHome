﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SolidHome.Models
{
    public class News
    {
        public int NewsId { set; get; }
        public string Title { set; get; }
        public string Description { set; get; }
        public string SmallDescription { set; get; }
        public string Image { set; get; }
        public string Url { set; get; }
        public bool IsPublished { set; get; }
        public DateTime PublicationDate { set; get; }
    }
}
