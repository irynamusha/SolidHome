﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class AdminNews
    {
        public int NewsId { set; get; }
        public string Image { set; get; }
        public bool IsPublished { set; get; }
        public DateTime PublicationDate { set; get; }
        public List<Translator> NewsTranslators { set; get; }
    }
}
