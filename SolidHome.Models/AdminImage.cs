﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class AdminImage
    {
        public int ImageId { set; get; }
        public string Path { set; get; }
        public List<ImageTranslator> ImageTranslators { set; get; }
    }
}
