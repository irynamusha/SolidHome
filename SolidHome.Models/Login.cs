﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class Login
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
