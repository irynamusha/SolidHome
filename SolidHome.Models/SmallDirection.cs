﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Models
{
    public class SmallDirection : IEqualityComparer<SmallDirection>
    {
        public int DirectionId { set; get; }
        public string Url { set; get; }
        public string Title { set; get; }
        public string SmallDescription { set; get; }
        public string IconStyle { set; get; }


        public bool Equals(SmallDirection x, SmallDirection y)
        {
            if (x == null || y == null)
                return false;
            if (x.Title == y.Title && x.Url == y.Url && x.DirectionId == y.DirectionId)
                return true;
            return false;
        }

        public int GetHashCode(SmallDirection obj)
        {
            return (obj.DirectionId.ToString() + obj.SmallDescription + obj.Url).GetHashCode();
        }
    }
}
