﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using AutoMapper;
using SolidHome.Entity;
using SolidHome.Entity.TranslatorEntities;
using SolidHome.Models;
using SolidHome.Models.Works;

namespace SolidHome.Service
{
    public static class MapperService
    {
        public static void Config()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<DirectionEntity, Direction>().ReverseMap();
                cfg.CreateMap<DirectionTranslatorEntity, Direction>();
                cfg.CreateMap<DirectionEntity, SmallDirection>()
                .ForMember(c=>c.Title, opt=>opt.MapFrom(c=>c.DirectionTranslators.FirstOrDefault().Title)).ReverseMap();
                cfg.CreateMap<DirectionTranslatorEntity, SmallDirection>();
                cfg.CreateMap<DirectionEntity, CatalogItem>().ReverseMap();
                cfg.CreateMap<DirectionTranslatorEntity, CatalogItem>();

                cfg.CreateMap<ProductEntity, Product>()
                    .ForMember(c => c.PreviewImages, opt => opt.MapFrom(s => s.ProductImages));
                cfg.CreateMap<ProductEntity, AdminProduct>()
                    .ForMember(c => c.PreviewImages, opt => opt.MapFrom(s => s.ProductImages));

                cfg.CreateMap<AdminProduct, ProductEntity>()
                    .ForMember(c => c.IsDeleted, opt => opt.Ignore())
                    .ForMember(c => c.ProductImages, opt => opt.Ignore())
                    .ForMember(c=>c.Direction, opt=>opt.Ignore())
                    .ForMember(c=>c.ProductDescriptions, opt=>opt.Ignore())
                    .ForMember(c => c.ProductTranslators, opt => opt.Ignore())
                    .ForAllMembers(p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));

                cfg.CreateMap<ProductDescriptionEntity, ProductDescription>().ReverseMap();
                cfg.CreateMap<ProductDescriptionEntity, AdminProductDescription>();
                cfg.CreateMap<AdminProductDescription, ProductDescriptionEntity>()
                    .ForMember(c=>c.ProductDescriptionTranslators, opt=>opt.Ignore())
                    .ForAllMembers(p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));
                cfg.CreateMap<ProductDescriptionTranslatorEntity, Translator>().ReverseMap();

                cfg.CreateMap<ProductEntity, ProductPreview>();
                cfg.CreateMap<ProductTranslatorEntity, ProductPreview>()
                    .ForMember(c => c.MainImage, opt => opt.Ignore())
                    .ForMember(c => c.ProductId, opt => opt.Ignore());

                


                //cfg.CreateMap<List<ProductTranslatorEntity>, List<ProductPreview>>();

                cfg.CreateMap<List<Translator>, List<NewsTranslatorEntity>>()
                    .ConvertUsing(ss => ss.Select(Mapper.Map<Translator, NewsTranslatorEntity>).ToList());
                cfg.CreateMap<NewsEntity, AdminNews>()
                    .ForAllMembers(p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));
                cfg.CreateMap<AdminNews, NewsEntity>()
                    .ForMember(c => c.PublicationDate, opt => opt.Ignore())
                    .ForMember(c => c.NewsTranslators, opt => opt.Ignore())
                    .ForAllMembers(p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));
                cfg.CreateMap<NewsTranslatorEntity, Translator>();
                cfg.CreateMap<ProductTranslatorEntity, Translator>();
                cfg.CreateMap<Translator, ProductTranslatorEntity>()
                    .ForMember(c => c.Language, opt => opt.Ignore())
                    .ForMember(c => c.Product, opt => opt.Ignore())
                    .ForAllMembers(p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));
                cfg.CreateMap<Translator, NewsTranslatorEntity>()
                    .ForMember(c => c.Language, opt => opt.Ignore())
                    .ForMember(c => c.News, opt => opt.Ignore())
                    .ForMember(c => c.SmallDescription, opt => opt.MapFrom(c=>c.SmallDescription ?? ""))
                    .ForMember(c => c.NewsId, opt => opt.Ignore())
                    .ForAllMembers(p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));
                cfg.CreateMap<NewsEntity, News>();
                cfg.CreateMap<News, NewsEntity>()
                    .ForMember(c => c.CreateDate, opt => opt.Ignore())
                    .ForAllMembers(p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));
                cfg.CreateMap<NewsTranslatorEntity, News>()
                    .ReverseMap();

                cfg.CreateMap<MessageEntity, Message>().ReverseMap();
                cfg.CreateMap<OrderEntity, Order>().ReverseMap();
                cfg.CreateMap<ImageEntity, Image>();
                cfg.CreateMap<ImageEntity, AdminImage>()
                .ForMember(c=>c.ImageTranslators, opt=>opt.MapFrom(s=>s.ImageTranslators));


                cfg.CreateMap<Image, ImageEntity>()
                    .ForMember(c => c.ProductId, opt => opt.Ignore())
                    .ForMember(c => c.Product, opt => opt.Ignore())
                    .ForMember(s => s.Path,
                        p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));

                cfg.CreateMap<AdminImage, ImageEntity>()
                    .ForMember(c => c.ProductId, opt => opt.Ignore())
                    .ForMember(c => c.Product, opt => opt.Ignore())
                    .ForMember(c => c.ImageTranslators, opt => opt.MapFrom(s=>s.ImageTranslators))
                    .ForMember(s => s.Path,
                        p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));

                cfg.CreateMap<ImageTranslatorEntity, ImageTranslator>()
                    .ForMember(c => c.Alt, opt => opt.MapFrom(mf => mf.Title));

                cfg.CreateMap<List<ImageTranslator>, List<ImageTranslatorEntity>>();

                cfg.CreateMap<ImageTranslator, ImageTranslatorEntity>()
                    .ForMember(c=>c.Title, opt=>opt.MapFrom(s=>s.Alt))
                    .ForMember(c => c.Image, opt => opt.Ignore())
                    .ForMember(c => c.ImageId, opt => opt.Ignore());

                cfg.CreateMap<ImageTranslatorEntity, Product>();

                cfg.CreateMap<ReviewEntity, Review>().ReverseMap();

                cfg.CreateMap<WorkImageEntity, Image>().ReverseMap()
                    .ForMember(c => c.WorkImageId, opt => opt.MapFrom(c => c.ImageId));

                cfg.CreateMap<WorkImageTranslatorEntity, ImageTranslator>()
                    .ForMember(c => c.Alt, opt => opt.MapFrom(mf => mf.Title));
                cfg.CreateMap<ImageTranslator, WorkImageTranslatorEntity>()
                    .ForMember(c => c.Title, opt => opt.MapFrom(s => s.Alt))
                    .ForMember(c => c.Image, opt => opt.Ignore())
                    .ForMember(c => c.WorkImageId, opt => opt.Ignore());

                cfg.CreateMap<AdminImage, WorkImageEntity>()
                    .ForMember(c => c.WorkImageId, opt => opt.Ignore())
                    .ForMember(c => c.WorkId, opt => opt.Ignore())
                    .ForMember(c => c.Work, opt => opt.Ignore())
                    .ForMember(c => c.ImageTranslators, opt => opt.Ignore())
                    .ForMember(s => s.Path,
                        p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));

                cfg.CreateMap<WorkImageEntity, AdminImage>()
                    .ForMember(c=>c.ImageId, opt=>opt.MapFrom(c=>c.WorkImageId))
                    .ForMember(c => c.ImageTranslators, opt => opt.MapFrom(s => s.ImageTranslators));

                cfg.CreateMap<WorkEntity, AdminWork>()
                    .ForMember(c => c.WorkImages, opt => opt.MapFrom(s => s.WorkImages))
                    .ForMember(c => c.Directions, opt => opt.MapFrom(c => c.Directions.Select(d => d.Direction)));
                cfg.CreateMap<AdminWork, WorkEntity>()
                    .ForMember(c => c.WorkImages, opt => opt.Ignore())
                    .ForMember(c => c.Directions, opt => opt.Ignore())
                    .ForMember(c => c.WorkTranslators, opt => opt.Ignore())
                    .ForAllMembers(p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));

                cfg.CreateMap<WorkTranslatorEntity, Translator>()
                    .ForMember(c => c.MainImageAlt, opt => opt.MapFrom(s => s.WorkMainImageAlt))
                    .ForMember(c => c.MetaDescription, opt => opt.MapFrom(s => s.WorkMetaDescription))
                    .ForMember(c => c.MetaTitle, opt => opt.MapFrom(s => s.WorkMetaTitle))
                    .ForMember(c => c.Url, opt => opt.MapFrom(s => s.WorkUrl));

                cfg.CreateMap<Translator, WorkTranslatorEntity>()
                    .ForMember(c => c.Language, opt => opt.Ignore())
                    .ForMember(c => c.WorkMainImageAlt, opt => opt.MapFrom(s => s.MainImageAlt))
                    .ForMember(c => c.WorkMetaDescription, opt => opt.MapFrom(s => s.MetaDescription))
                    .ForMember(c => c.WorkMetaTitle, opt => opt.MapFrom(s => s.MetaTitle))
                    .ForMember(c => c.WorkUrl, opt => opt.MapFrom(s => s.Url))
                    .ForMember(c => c.Work, opt => opt.Ignore())
                    .ForAllMembers(p => p.Condition((source, destination, sourceMember, destMember) => (sourceMember != null)));
            });
        }
        public static TDestination Map<TSource, TDestination>(
            this TDestination destination, TSource source)
        {
            return Mapper.Map(source, destination);
        }
    }
}
