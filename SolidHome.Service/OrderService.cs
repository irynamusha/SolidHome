﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using SolidHome.Entity;
using SolidHome.Models;
using SolidHome.Repository.Interfaces;
using SolidHome.Service.Interfaces;
using SolidHome.Utils.Service;

namespace SolidHome.Service
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly EmailService _emailService;
        public OrderService(IOrderRepository orderRepository, EmailService emailService)
        {
            _orderRepository = orderRepository;
            _emailService = emailService;
        }
        public IEnumerable<Order> GetAllOrders()
        {
           return  _orderRepository.GetAllOrders().Select(Mapper.Map<OrderEntity, Order>);
        }

        public Order AddOrder(Order order)
        {
            order.Clarification = order.Clarification == "undefined" ||  String.IsNullOrEmpty(order.Clarification) ? null : order.Clarification;
            OrderEntity newOrder = Mapper.Map<Order, OrderEntity>(order);
            newOrder.Date = DateTime.Now;
            var result = _orderRepository.AddOrder(newOrder);
            order.OrderId = result.OrderId;
            string message = _emailService.GetOrderMessage(order);
            _emailService.Send("info@solidhome.com.ua", message);
            return order;
        }

        public void Remove(int id)
        {
            _orderRepository.Remove(id);
        }
    }
}
