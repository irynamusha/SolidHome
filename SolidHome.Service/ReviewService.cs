﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolidHome.Entity;
using SolidHome.Models;
using SolidHome.Repository.Interfaces;
using SolidHome.Service.Interfaces;

namespace SolidHome.Service
{
    public class ReviewService : IReviewService
    {
        private readonly IReviewRepository _reviewRepository;

        public ReviewService(IReviewRepository reviewRepository)
        {
            _reviewRepository = reviewRepository;
        }

        public List<Review> GetAllReviews(bool? approved = null)
        {
            return _reviewRepository.GetAllReviews(approved).Select(AutoMapper.Mapper.Map<ReviewEntity, Review>).ToList();
        }

        public void RemoveReview(int id)
        {
            _reviewRepository.RemoveReview(id);
        }

        public Review AddReview(Review review)
        {
            review.CreateDate = DateTime.Now;
            review.ApproveDate = DateTime.Now;
           var result = _reviewRepository.AddReview(AutoMapper.Mapper.Map<Review, ReviewEntity>(review));
            review.ReviewId = result.ReviewId;
            return review;
        }

        public Review ChangeApproveReview(int id)
        {
            var result = _reviewRepository.ChangeApproveReview(id);
            return AutoMapper.Mapper.Map<ReviewEntity, Review>(result);
        }
    }
}
