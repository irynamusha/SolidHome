﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using SolidHome.Entity;
using SolidHome.Models;
using SolidHome.Repository.Interfaces;
using SolidHome.Service.Interfaces;

namespace SolidHome.Service
{
    public class MessageService : IMessageService
    {
        private readonly IMessageRepository _messageRepository;
        public MessageService(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }
        public IEnumerable<Message> GetAllMessages()
        {
           return  _messageRepository
                .GetAllMessages()
                .ToList()
                .Select(Mapper.Map<MessageEntity, Message>);
        }

        public Message GetMessageById(int id)
        {
            return Mapper.Map<MessageEntity, Message>(_messageRepository.GetMessageById(id));
        }

        public void RemoveMessage(int id)
        {
            _messageRepository.RemoveMessage(id);
        }

        public Message SendMessage(Message message)
        {
            message.Date = DateTime.Now;
            var mails = _messageRepository.GetAllMessages();
            MessageEntity result = _messageRepository.AddMessage(Mapper.Map<Message, MessageEntity>(message));
            message.MessageId = result.MessageId;
            return message;
        }
    }
}
