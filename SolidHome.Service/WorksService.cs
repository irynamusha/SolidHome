﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using SolidHome.Entity;
using SolidHome.Entity.TranslatorEntities;
using SolidHome.Models;
using SolidHome.Models.Works;
using SolidHome.Repository.Interfaces;
using SolidHome.Service.Interfaces;
using SolidHome.Utils.Service;

namespace SolidHome.Service
{
    public class WorksService : IWorksService
    {
        private readonly IWorksRepository _worksRepository;
        private readonly IDirectionRepository _directionRepository;
        private readonly UrlService _urlService;

        public WorksService(IWorksRepository worksRepository, IDirectionRepository directionRepository, UrlService urlService)
        {
            _worksRepository = worksRepository;
            _directionRepository = directionRepository;
            _urlService = urlService;
        }

        public List<AdminWork> GetAll(string langForDirections = null)
        {
            var works = _worksRepository.GetAllWorks().ToList();
            if (langForDirections != null)
            {
                works.ForEach(c=>c.Directions.ForEach(d=>d.Direction.DirectionTranslators.RemoveAll(dt=>dt.Code != langForDirections)));
            }
            List<AdminWork> result = works.Select(Mapper.Map<AdminWork>).ToList();
            return result;
        }

        public AdminWork Add(AdminWork work)
        {
            var workEntity = Mapper.Map<AdminWork, WorkEntity>(work);
            workEntity.WorkTranslators = new List<WorkTranslatorEntity>();
            workEntity.WorkImages = new List<WorkImageEntity>();
            if (work.WorkImages != null)
                workEntity.WorkImages.AddRange(work.WorkImages.Select(Mapper.Map<AdminImage, WorkImageEntity>));
            workEntity.Directions = work.Directions.Select(
            c=> new WorkDirectionEntity()
            {
                DirectionId = c.DirectionId
            }).ToList();
            for (int i = 0; i < work.WorkTranslators.Count; i++)
            {
                workEntity.WorkTranslators.Add(
                    Mapper.Map<Translator, WorkTranslatorEntity>(work.WorkTranslators[i]));
                workEntity.WorkTranslators[i].WorkUrl = _urlService.GetEncodedUrlString(workEntity.WorkTranslators[i].Title);
            }
            var result = _worksRepository.CreateWork(workEntity);
            return Mapper.Map<WorkEntity, AdminWork>(result);
        }

        public AdminWork Update(AdminWork work)
        {
            var a = _worksRepository.GetWorkById(work.WorkId);
            if (a == null)
                return null;
            a = Mapper.Map(work, a);
            if (work.WorkImages != null)
            {
                for (int i = 0; i < a.WorkImages.Count; i++)
                {
                    a.WorkImages[i] = Mapper.Map<AdminImage, WorkImageEntity>(work.WorkImages[i], a.WorkImages[i]);
                    if (a.WorkImages[i].ImageTranslators.Count == 0)
                        a.WorkImages[i].ImageTranslators = new List<WorkImageTranslatorEntity>()
                        {
                            new WorkImageTranslatorEntity(){WorkImageId = a.WorkImages[i].WorkImageId, Code = "uk"},
                            new WorkImageTranslatorEntity(){WorkImageId = a.WorkImages[i].WorkImageId, Code = "ru"}
                        };
                    for (int j = 0; j < a.WorkImages[i].ImageTranslators.Count; j++)
                    {
                        a.WorkImages[i].ImageTranslators[j] = Mapper.Map<ImageTranslator, WorkImageTranslatorEntity>(work.WorkImages[i].ImageTranslators[j], a.WorkImages[i].ImageTranslators[j]);
                    }
                }
                var newImages = work.WorkImages.Skip(a.WorkImages.Count);
                foreach (var newImage in newImages)
                {
                    var newImageEntity = Mapper.Map<AdminImage, WorkImageEntity>(newImage, opt => opt.AfterMap((image, entity) => entity.ImageTranslators = new List<WorkImageTranslatorEntity>()));
                    foreach (var translator in newImage.ImageTranslators)
                    {
                        newImageEntity.ImageTranslators.Add(Mapper.Map<ImageTranslator, WorkImageTranslatorEntity>(translator));
                    }
                    newImageEntity.WorkId = a.WorkId;
                    a.WorkImages.Add(newImageEntity);
                }
            }
            var newDirections =
                work.Directions.Where(c => !a.Directions.Select(d => d.DirectionId).Contains(c.DirectionId)).Select(c=>new WorkDirectionEntity()
                {
                    DirectionId = c.DirectionId,
                    WorkId = a.WorkId
                });

            a.Directions = a.Directions.Where(c => work.Directions.Select(d => d.DirectionId).Contains(c.DirectionId))
                .ToList();
            a.Directions.AddRange(newDirections);

            for (int i = 0; i < a.WorkTranslators.Count; i++)
            {
                a.WorkTranslators[i] =
                    Mapper.Map<Translator, WorkTranslatorEntity>(work.WorkTranslators[i], a.WorkTranslators[i]);
                a.WorkTranslators[i].WorkUrl = _urlService.GetEncodedUrlString(a.WorkTranslators[i].Title);
            }
            _worksRepository.Update(a);
            return work;
        }

        public AdminWork GetWorkById(int id)
        {
            var a = _worksRepository.GetWorkById(id);
            if (a == null)
                return null;
            AdminWork result = Mapper.Map<AdminWork>(a);
            List<DirectionTranslatorEntity> tr = a.Directions.SelectMany(c=>c.Direction.DirectionTranslators).Where(c => c.Code == "uk").ToList();
            result.Directions = tr.Select(Mapper.Map<DirectionTranslatorEntity, SmallDirection>).ToList();
            return result;
        }

        public IEnumerable<AdminWork> GetWorkByDirectionId(int id)
        {
            var a = _worksRepository.GetWorkByDirectionId(id).ToList();
            List<AdminWork> result = a.Select(Mapper.Map<AdminWork>).ToList();
            for (int i = 0; i < a.Count; i++)
            {
                result[i].Directions = (a[i].Directions.SelectMany(c => c.Direction.DirectionTranslators)
                    .Where(c => c.Code == "uk").Select(Mapper.Map<SmallDirection>).ToList());
            }
            return result;
        }

        public void Remove(int id)
        {
            _worksRepository.RemoveWork(id);
        }

        public void RemoveImageFromWork(int imageId, int workId)
        {
            var product = _worksRepository.GetWorkById(workId);
            product.WorkImages.Remove(product.WorkImages.FirstOrDefault(c => c.WorkImageId == imageId));
            _worksRepository.Update(product);
        }
    }
}
