﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Entity;
using SolidHome.Models;
using SolidHome.Repository;
using SolidHome.Repository.Interfaces;
using SolidHome.Service.Interfaces;
using SolidHome.Utils.Service;

namespace SolidHome.Service
{

    public class NewsService : INewsService
    {
        private readonly INewsRepository _newsRepository;
        private readonly  UrlService _urlService;

        public NewsService(INewsRepository newsRepository, UrlService urlService)
        {
            _newsRepository = newsRepository;
            _urlService = urlService;
        }

        public AdminNews GetNewsById(int id)
        {
            return Mapper.Map<NewsEntity, AdminNews>(_newsRepository.GetNewsById(id));
        }

        public AdminNews AddNews(AdminNews news)
        {
            NewsEntity newNews = Mapper.Map<AdminNews, NewsEntity>(news);
            if (newNews.IsPublished)
            {
                newNews.PublicationDate = DateTime.Now;
            }
            for (int i = 0; i < news.NewsTranslators.Count; i++)
            {
                newNews.NewsTranslators.Add(Mapper.Map<Translator, NewsTranslatorEntity>(news.NewsTranslators[i]));
                newNews.NewsTranslators[i].Url = _urlService.GetEncodedUrlString(newNews.NewsTranslators[i].Title);
                news.NewsTranslators[i].Url = newNews.NewsTranslators[i].Url;
            }
            var result = _newsRepository.AddNews(newNews);
            news.NewsId = result.NewsId;
            return news;
        }

        public IEnumerable<News> GetAllNews(string languageCode, bool? isPublished = null)
        {
            List<NewsEntity> news = _newsRepository.GetAllNews().ToList();
            List<News> result = news
               .Where(c => isPublished == null || c.IsPublished == isPublished)
               .OrderByDescending(c => c.PublicationDate)
                .Select(c=>Mapper.Map<NewsEntity,News>(c)
                    .Map(c.NewsTranslators.FirstOrDefault(t => t.Code == languageCode)))
                .ToList();
            return result;
        }

        public IEnumerable<AdminNews> GetAllNews()
        {
            List<NewsEntity> news = _newsRepository
                .GetAllNews()
                .OrderBy(c => c.PublicationDate)
                .ToList();
            return news.Select(Mapper.Map<NewsEntity, AdminNews>);
        }

        public News GetNewsById(int id, string languageCode)
        {
            NewsEntity newsEntity = _newsRepository.GetNewsById(id);
            News news = Mapper.Map<News>(newsEntity)
                .Map(newsEntity.NewsTranslators.FirstOrDefault(t => t.Code == languageCode));
            return news;
        }

        public void RemoveNews(int id)
        {
            _newsRepository.RemoveNews(id);
        }

        public AdminNews UpdateNews(AdminNews news)
        {
            NewsEntity oldNews = _newsRepository.GetNewsById(news.NewsId);
            oldNews.PublicationDate = news.IsPublished && !oldNews.IsPublished ? DateTime.Now : oldNews.PublicationDate;
            oldNews = oldNews.Map<AdminNews, NewsEntity>(news);
            for(int i=0; i < oldNews.NewsTranslators.Count; i++)
            {
                oldNews.NewsTranslators[i] =
                    Mapper.Map<Translator, NewsTranslatorEntity>(news.NewsTranslators[i], oldNews.NewsTranslators[i]);
                oldNews.NewsTranslators[i].Url = _urlService.GetEncodedUrlString(oldNews.NewsTranslators[i].Title);
            }
            _newsRepository.UpdateNews(oldNews);
            return news;
        }

        public News ChangePublication(int id)
        {
            NewsEntity newsEntity = _newsRepository.GetNewsById(id);
            newsEntity.IsPublished = !newsEntity.IsPublished;
            newsEntity.PublicationDate = newsEntity.IsPublished ? DateTime.Now : newsEntity.PublicationDate;
            _newsRepository.UpdateNews(newsEntity);
            return Mapper.Map<NewsEntity, News>(newsEntity);
        }
    }
}
