﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using AutoMapper;
using SolidHome.Entity;
using SolidHome.Entity.TranslatorEntities;
using SolidHome.Models;
using SolidHome.Repository.Interfaces;
using SolidHome.Service.Interfaces;
using SolidHome.Utils.Service;

namespace SolidHome.Service
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly UrlService _urlService;
        private readonly IDirectionRepository _directionRepository;
        public ProductService(IProductRepository productRepository, UrlService urlService, IDirectionRepository directionRepository)
        {
            _productRepository = productRepository;
            _directionRepository = directionRepository;
            _urlService = urlService;
        }
        public AdminProduct GetProductByUrl(string url, string lang)
        {
            var a = _productRepository.GetProductByUrl(url);
            if (a == null)
                return null;
            AdminProduct result = Mapper.Map<AdminProduct>(a);
            DirectionTranslatorEntity tr = a.Direction.DirectionTranslators.FirstOrDefault(c => c.Code == lang);
            result.Direction =
                Mapper.Map<DirectionTranslatorEntity, SmallDirection>(tr, result.Direction);
            return result;
        }

        public Product GetProductById(int id, string lang)
        {
            var a = _productRepository.GetProductById(id);
            if (a == null)
                return null;
            Product result = Mapper.Map<Product>(a);
            result.ProductTranslator =
                result.ProductTranslator.Map(a.ProductTranslators.FirstOrDefault(c => c.Code == lang));
            DirectionTranslatorEntity tr = a.Direction.DirectionTranslators.FirstOrDefault(c => c.Code == lang);
            result.Direction =
                Mapper.Map<DirectionTranslatorEntity, SmallDirection>(tr, result.Direction);
            return result;
        }

        public AdminProduct GetProductById(int id)
        {
            var a = _productRepository.GetProductById(id);
            if (a == null)
                return null;
            AdminProduct result = Mapper.Map<AdminProduct>(a);
            DirectionTranslatorEntity tr = a.Direction.DirectionTranslators.FirstOrDefault(c => c.Code == "uk");
            result.Direction =
                Mapper.Map<DirectionTranslatorEntity, SmallDirection>(tr, result.Direction);
            return result;
        }

        public AdminProduct CreateProduct(AdminProduct product)
        {
            var productEntity = Mapper.Map<AdminProduct, ProductEntity>(product);
            productEntity.ProductTranslators = new List<ProductTranslatorEntity>();
            productEntity.ProductImages = new List<ImageEntity>();
            if (product.PreviewImages != null)
                productEntity.ProductImages.AddRange(product.PreviewImages.Select(Mapper.Map<AdminImage, ImageEntity>));
            productEntity.Direction = null;
            productEntity.DirectionId = _directionRepository.GetDirectionByUrl(product.Direction.Url).DirectionId;
            for (int i = 0; i < product.ProductTranslators.Count; i++)
            {
                productEntity.ProductTranslators.Add(
                    Mapper.Map<Translator, ProductTranslatorEntity>(product.ProductTranslators[i]));
                productEntity.ProductTranslators[i].Url = _urlService.GetEncodedUrlString(productEntity.ProductTranslators[i].Title);
            }
            var result = _productRepository.CreateProduct(productEntity);
            return Mapper.Map<ProductEntity, AdminProduct>(result);
        }

        public IEnumerable<ProductPreview> GetProductsByDirectionUrl(string url, string lang)
        {
            return _productRepository
                .GetAllProducts()
                .Where(c => c.Direction.Url == url)
                .Select(c => Mapper.Map<ProductPreview>(c)
                .Map(c.ProductTranslators.FirstOrDefault(p => p.Code == lang)));
        }

        public AdminProduct UpdateProduct(AdminProduct product)
        {
            var a = _productRepository.GetProductById(product.ProductId);
            if (a == null)
                return null;
            a = a.Map<AdminProduct, ProductEntity>(product);
            if (product.PreviewImages != null)
            {
                for (int i = 0; i < a.ProductImages.Count; i++)
                {
                    a.ProductImages[i] = Mapper.Map<AdminImage, ImageEntity>(product.PreviewImages[i], a.ProductImages[i]);
                    if(a.ProductImages[i].ImageTranslators.Count == 0)
                        a.ProductImages[i].ImageTranslators = new List<ImageTranslatorEntity>()
                        {
                            new ImageTranslatorEntity(){ImageId = a.ProductImages[i].ImageId, Code = "uk"},
                            new ImageTranslatorEntity(){ImageId = a.ProductImages[i].ImageId, Code = "ru"}
                        };
                    for (int j = 0; j < a.ProductImages[i].ImageTranslators.Count; j++)
                    {
                        a.ProductImages[i].ImageTranslators[j] = Mapper.Map<ImageTranslator, ImageTranslatorEntity>(product.PreviewImages[i].ImageTranslators[j], a.ProductImages[i].ImageTranslators[j]);
                    }
                }
                var newImages = product.PreviewImages.Skip(a.ProductImages.Count);
                foreach (var newImage in newImages)
                {
                    var newImageEntity = Mapper.Map<AdminImage, ImageEntity>(newImage);
                    foreach (var translator in newImage.ImageTranslators)
                    {
                        newImageEntity.ImageTranslators.Add(Mapper.Map<ImageTranslator, ImageTranslatorEntity>(translator));
                    }
                    newImageEntity.ProductId = a.ProductId;
                    a.ProductImages.Add(newImageEntity);
                }
            }


            for (int i = 0; i < a.ProductTranslators.Count; i++)
            {
                a.ProductTranslators[i] =
                    Mapper.Map<Translator, ProductTranslatorEntity>(product.ProductTranslators[i], a.ProductTranslators[i]);
                a.ProductTranslators[i].Url = _urlService.GetEncodedUrlString(a.ProductTranslators[i].Title);
            }
            _productRepository.Update(a);
            return product;
        }

        public void RemoveProduct(int id)
        {
            _productRepository.RemoveProduct(id);
        }

        public AdminProductDescription GetAdminProductDescriptionById(int id)
        {
            var descrEntity = _productRepository.GetProductDescriptionById(id);
            var adminDescr = Mapper.Map<ProductDescriptionEntity, AdminProductDescription>(descrEntity);
            return adminDescr;
        }

        public void UpdateProductDescription(AdminProductDescription description)
        {
            var oldDescr = _productRepository.GetProductDescriptionById(description.ProductDescriptionId);
            oldDescr = oldDescr.Map<AdminProductDescription, ProductDescriptionEntity>(description);
            for (int i = 0; i < oldDescr.ProductDescriptionTranslators.Count; i++)
            {
                oldDescr.ProductDescriptionTranslators[i] =
                    Mapper.Map<Translator, ProductDescriptionTranslatorEntity>(description.ProductDescriptionTranslators[i], oldDescr.ProductDescriptionTranslators[i]);
            }
            _productRepository.UpdateProductDescription(oldDescr);
        }

        public AdminProductDescription CreateProductDescription(AdminProductDescription description)
        {
            ProductDescriptionEntity newDescription =
                Mapper.Map<AdminProductDescription, ProductDescriptionEntity>(description);
            newDescription.Product = _productRepository.GetProductById(description.ProductId);
            newDescription.ProductDescriptionTranslators = new List<ProductDescriptionTranslatorEntity>();
            for (int i = 0; i < description.ProductDescriptionTranslators.Count; i++)
            {
                newDescription.ProductDescriptionTranslators.Add(
                    Mapper.Map<Translator, ProductDescriptionTranslatorEntity>(description.ProductDescriptionTranslators[i]));
            }

            var result = _productRepository.CreateProductDescription(newDescription);
            return Mapper.Map<ProductDescriptionEntity, AdminProductDescription>(result);
        }

        public void RemoveProductDescription(int id)
        {
            _productRepository.RemoveProductDescription(id);
        }

        public void RemoveImageFromProduct(int imageId, int productId)
        {
            var product = _productRepository.GetProductById(productId);
            product.ProductImages.Remove(product.ProductImages.FirstOrDefault(c => c.ImageId == imageId));
            _productRepository.Update(product);
        }
    }
}
