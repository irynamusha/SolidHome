﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using SolidHome.Entity;
using SolidHome.Entity.TranslatorEntities;
using SolidHome.Models;
using SolidHome.Repository.Interfaces;
using SolidHome.Service.Interfaces;

namespace SolidHome.Service
{
    public class CatalogService : ICatalogService
    {
        private readonly IDirectionRepository _directionRepository;

        public CatalogService(IDirectionRepository directionRepository)
        {
            _directionRepository = directionRepository;
        }
        public CatalogItem GetCatalogItem(string url, string lang)
        {
            DirectionEntity dir = _directionRepository.GetDirectionByUrl(url, true);
            if (dir == null)
                return null;
            var result = Mapper.Map<CatalogItem>(dir)
                .Map(dir.DirectionTranslators.FirstOrDefault(c => c.Code == lang));
            List<ProductTranslatorEntity> tr = dir.Products.Select(c =>
                c.ProductTranslators.FirstOrDefault(s => s.Code == lang))
                .ToList();
            for (int i = 0; i < tr.Count; i++)
            {
                result.Products[i] = Mapper.Map<ProductTranslatorEntity, ProductPreview>(tr[i], result.Products[i]);
            }
            result.Description = dir.DirectionTranslators.FirstOrDefault(c => c.Code == lang)?.Description;
            return result;
        }
    }
}
