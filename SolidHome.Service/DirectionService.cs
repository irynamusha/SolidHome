﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using SolidHome.Entity;
using SolidHome.Models;
using SolidHome.Repository.Interfaces;
using SolidHome.Service.Interfaces;
using SolidHome.Utils.Service;

namespace SolidHome.Service
{
    public class DirectionService : IDirectionService
    {
        private readonly IDirectionRepository _directionRepository;
        private readonly CacheService _cacheService;

        public DirectionService(IDirectionRepository directionRepository, CacheService cacheService)
        {
            _directionRepository = directionRepository;
            _cacheService = cacheService;
        }

        public Direction GetDirectionById(int id, string lang)
        {
            var dir = _directionRepository.GetDirectionById(id);
            if (dir == null)
                return null;
            var result = Mapper.Map<Direction>(dir)
                .Map(dir.DirectionTranslators.FirstOrDefault(c => c.Code == lang));
            return result;
        }

        public Direction GetDirectionByUrl(string url, string lang)
        {
            DirectionEntity dir = _directionRepository.GetDirectionByUrl(url);
            if (dir == null)
                return null;
            var result = Mapper.Map<Direction>(dir)
                .Map(dir.DirectionTranslators.FirstOrDefault(c => c.Code == lang));
            return result;
        }

        public IEnumerable<T> GetAllDirections<T>(string lang)
        {
            
            if (typeof(T) == typeof(SmallDirection))
            {
                var result = _cacheService.GetValue(lang + "_directories");

                if (result == null)
                {
                    result = _directionRepository
                        .GetAllDirections()
                        .ToList()
                        .Select(c => Mapper.Map<SmallDirection>(c).Map(c.DirectionTranslators.FirstOrDefault(l => l.Code == lang)));
                    _cacheService.SetValue(lang + "_directories", result, TimeSpan.FromDays(7));
                }
                return (IEnumerable<T>) result;
            }
            else
            {
                return _directionRepository
                    .GetAllDirections()
                    .ToList()
                    .Select(c => Mapper.Map<T>(c).Map(c.DirectionTranslators.FirstOrDefault(l => l.Code == lang)));
            }
        }

        public Direction GetDirectionByProductId(int id)
        {
            var dir = _directionRepository.GetDirectionByProductId(id);
            var result = Mapper.Map<Direction>(dir);
            return result;
        }
    }
}
