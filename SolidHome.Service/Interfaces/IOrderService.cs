﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Models;

namespace SolidHome.Service.Interfaces
{
    public interface IOrderService
    {
        IEnumerable<Order> GetAllOrders();
        Order AddOrder(Order order);
        void Remove(int id);
    }
}
