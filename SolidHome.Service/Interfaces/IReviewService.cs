﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Models;

namespace SolidHome.Service.Interfaces
{
    public interface IReviewService
    {
        List<Review> GetAllReviews(bool? approved = null);
        void RemoveReview(int id);
        Review AddReview(Review review);
        Review ChangeApproveReview(int id);
    }
}
