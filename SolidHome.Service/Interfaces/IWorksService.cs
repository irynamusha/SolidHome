﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Models.Works;

namespace SolidHome.Service.Interfaces
{
    public interface IWorksService
    {
        AdminWork Add(AdminWork work);
        AdminWork Update(AdminWork work);
        AdminWork GetWorkById(int id);
        IEnumerable<AdminWork> GetWorkByDirectionId(int id);
        void RemoveImageFromWork(int imageId, int workId);
        List<AdminWork> GetAll(string langForDirections = null);
        void Remove(int id);
    }
}
