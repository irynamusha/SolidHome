﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Models;

namespace SolidHome.Service.Interfaces
{
    public interface IMessageService
    {
        Message GetMessageById(int id);
        Message SendMessage(Message message);
        IEnumerable<Message> GetAllMessages();
        void RemoveMessage(int id);
    }
}
