﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using SolidHome.Models;

namespace SolidHome.Service.Interfaces
{
    public interface INewsService
    {
        IEnumerable<News> GetAllNews(string languageCode, bool? isPublished = null);
        IEnumerable<AdminNews> GetAllNews();
        AdminNews GetNewsById(int id);
        News GetNewsById(int id, string languageCode);
        AdminNews AddNews(AdminNews news);
        News ChangePublication(int id);
        AdminNews UpdateNews(AdminNews news);
        void RemoveNews(int id);
    }
}
