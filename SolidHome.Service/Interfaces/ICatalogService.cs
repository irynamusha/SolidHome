﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Models;

namespace SolidHome.Service.Interfaces
{
    public interface ICatalogService
    {
        CatalogItem GetCatalogItem(string url, string lang);
    }
}
