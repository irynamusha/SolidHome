﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Models;

namespace SolidHome.Service.Interfaces
{
    public interface IDirectionService
    {
        Direction GetDirectionById(int id, string lang);
        Direction GetDirectionByUrl(string url, string lang);
        IEnumerable<T> GetAllDirections<T>(string lang);
        Direction GetDirectionByProductId(int id);
    }
}
