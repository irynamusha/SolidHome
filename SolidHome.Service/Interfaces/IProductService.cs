﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Models;

namespace SolidHome.Service.Interfaces
{
    public interface IProductService
    {
        AdminProduct GetProductByUrl(string url, string lang);
        Product GetProductById(int id, string lang);
        AdminProduct GetProductById(int id);
        AdminProduct CreateProduct(AdminProduct product);
        IEnumerable<ProductPreview> GetProductsByDirectionUrl(string url, string lang);
        AdminProduct UpdateProduct(AdminProduct product);
        void RemoveImageFromProduct(int imageId, int productId);
        void RemoveProduct(int id);
        
        AdminProductDescription GetAdminProductDescriptionById(int id);
        void UpdateProductDescription(AdminProductDescription description);
        AdminProductDescription CreateProductDescription(AdminProductDescription description);
        void RemoveProductDescription(int id);

    }
}
