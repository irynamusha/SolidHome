﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class OrderEntity
    {
        [Key]
        public int OrderId { set; get; }
        public string Name { set; get; }
        public string Phone { set; get; }
        public string Url { set; get; }
        public string Email { set; get; }
        public string Message { set; get; }
        public string ProductName { set; get; }
        public DateTime Date { set; get; }
        public string Clarification { set; get; }
    }
}
