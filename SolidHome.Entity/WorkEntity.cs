﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;
using SolidHome.Entity.TranslatorEntities;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class WorkEntity
    {
        [Key]
        public int WorkId { set; get; }
        
        public DateTime WorkDate { set; get; }
        public string MainImage { set; get; }

        public List<WorkDirectionEntity> Directions { set; get; }
        public List<WorkImageEntity> WorkImages { set; get; }
        public List<WorkTranslatorEntity> WorkTranslators { set; get; }

    }
}
