﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class NewsEntity
    {
        [Key]
        public int NewsId { set; get; }
        public string Image { set; get; }
        public DateTime CreateDate { set; get; }
        public DateTime PublicationDate { set; get; }
        public bool IsPublished { set; get; }
        public IList<NewsTranslatorEntity> NewsTranslators { set; get; }

        public NewsEntity()
        {
            NewsTranslators = new List<NewsTranslatorEntity>();
        }
    }
}
