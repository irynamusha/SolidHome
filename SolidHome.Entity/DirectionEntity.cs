﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class DirectionEntity
    {
        [Key]
        public int DirectionId { set; get; }
        public string Url { set; get; }
        public string IconStyle { set; get; }

        public List<WorkDirectionEntity> Works { set; get; }
        public List<DirectionTranslatorEntity> DirectionTranslators { set; get; }
        public List<ProductEntity> Products { set; get; }

        public DirectionEntity()
        {
            DirectionTranslators = new List<DirectionTranslatorEntity>();
            Products = new List<ProductEntity>();
        }
    }
}
