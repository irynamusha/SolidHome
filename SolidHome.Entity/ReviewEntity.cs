﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class ReviewEntity
    {
        [Key]
        public int ReviewId { set; get; }
        public string Content { set; get; }
        public string Email { set; get; }
        public DateTime ApproveDate { set; get; }
        public DateTime CreateDate { set; get; }
        public bool Approved { set; get; }
        public string UserName { set; get; }
    }
}
