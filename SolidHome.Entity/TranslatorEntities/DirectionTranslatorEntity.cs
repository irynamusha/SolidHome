﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class DirectionTranslatorEntity : TranslatorEntity
    {
        public int DirectionId { set; get; }
        public DirectionEntity Direction { set; get; }
    }
}
