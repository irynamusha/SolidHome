﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class TranslatorEntity
    {
        [Key]
        public int TranslatorId { set; get; }
        //public int ItemId { set; get; }

        public string Code { set; get; }
        public LanguageEntity Language { set; get; }

        public string Title { set; get; }
        public string Description { set; get; }
        public string SmallDescription { set; get; }
    }
}