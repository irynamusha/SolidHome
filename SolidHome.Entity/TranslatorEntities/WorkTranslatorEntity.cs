﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Entity.TranslatorEntities
{
    public class WorkTranslatorEntity : TranslatorEntity
    {
        public string CompanyName { set; get; }
        public string WorkUrl { set; get; }
        public string WorkMainImageAlt { set; get; }
        public string WorkMetaTitle { set; get; }
        public string WorkMetaDescription { set; get; }

        public int WorkId { set; get; }
        public WorkEntity Work { set; get; }
        
    }
}
