﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace SolidHome.Entity.TranslatorEntities
{
    [MySqlCharset("utf8")]
    public class ProductDescriptionTranslatorEntity : TranslatorEntity
    {
        public int ProductDescriptionId { set; get; }
        public ProductDescriptionEntity ProductDescription { set; get; }
    }
}
