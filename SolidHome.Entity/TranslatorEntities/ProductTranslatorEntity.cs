﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace SolidHome.Entity.TranslatorEntities
{
    [MySqlCharset("utf8")]
    public class ProductTranslatorEntity : TranslatorEntity
    {
        public int ProductId { set; get; }
        public ProductEntity Product { set; get; }
        public string Url { set; get; }
        public string MainImageAlt { set; get; }
        public string MetaTitle { set; get; }
        public string MetaDescription { set; get; }
    }
}
