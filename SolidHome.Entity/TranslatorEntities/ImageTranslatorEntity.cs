﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolidHome.Entity.TranslatorEntities
{
    public class ImageTranslatorEntity : TranslatorEntity
    {
        public int ImageId { set; get; }
        public ImageEntity Image { set; get; }
    }
}
