﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class NewsTranslatorEntity : TranslatorEntity
    {
        public int NewsId { set; get; }
        public NewsEntity News { set; get; }
        public string Url { set; get; }
    }
}
