﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace SolidHome.Entity.TranslatorEntities
{
    public class WorkImageTranslatorEntity : TranslatorEntity
    {
        public int WorkImageId { set; get; }
        [ForeignKey("WorkImageId")]
        public WorkImageEntity Image { set; get; }
    }
}
