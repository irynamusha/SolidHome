﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;
using SolidHome.Entity.TranslatorEntities;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class ImageEntity
    {
        [Key]
        public int ImageId { set; get; }
        public string Path { set; get; }
        
        public int ProductId { set; get; }
        public ProductEntity Product { set; get; }

        public List<ImageTranslatorEntity> ImageTranslators { set; get; }
    }
}
