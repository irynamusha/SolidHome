﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;
using SolidHome.Entity.TranslatorEntities;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class WorkImageEntity
    {
        
            [Key]
            public int WorkImageId { set; get; }
            public string Path { set; get; }

            public int WorkId { set; get; }
            public WorkEntity Work{ set; get; }

            public List<WorkImageTranslatorEntity> ImageTranslators { set; get; }
        
    }
}
