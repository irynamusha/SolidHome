﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class WorkDirectionEntity
    {
        public int WorkId { set; get; }
        public WorkEntity Work { set; get; }

        public int DirectionId { set; get; }
        public DirectionEntity Direction { set; get; } 
    }
}
