﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class MessageEntity
    {
        [Key]
        public int MessageId { set; get; }
        public string UserName { set; get; }
        public string Email { set; get; }
        public string Content { set; get; }
        public DateTime Date { set; get; }

    }
}
