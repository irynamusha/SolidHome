﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;
using SolidHome.Entity.TranslatorEntities;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class ProductEntity
    {
        [Key]
        public int ProductId { set; get; }
        public string MainImage { set; get; }
        public bool IsDeleted { set; get; }

        public int DirectionId { set; get; }
        public DirectionEntity Direction { set; get; }

        public List<ImageEntity> ProductImages { set; get; }
        public List<ProductTranslatorEntity> ProductTranslators { set; get; }
        public List<ProductDescriptionEntity> ProductDescriptions { set; get; }
    }
}
