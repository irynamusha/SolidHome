﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MySql.Data.EntityFrameworkCore.DataAnnotations;
using SolidHome.Entity.TranslatorEntities;

namespace SolidHome.Entity
{
    [MySqlCharset("utf8")]
    public class ProductDescriptionEntity
    {
        [Key]
        public int ProductDescriptionId { set; get; }
        public List<ProductDescriptionTranslatorEntity> ProductDescriptionTranslators { set; get; }
        public int Price { set; get; }
        public int ProductId { set; get; }
        public ProductEntity Product { set; get; }
    }
}
