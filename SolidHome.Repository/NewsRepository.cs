﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SolidHome.Entity;
using SolidHome.Repository.Interfaces;

namespace SolidHome.Repository
{
    public class NewsRepository : INewsRepository
    {
        private readonly DataContext _db;
        public NewsRepository(DataContext db)
        {
            _db = db;
        }
        public IEnumerable<NewsEntity> GetAllNews()
        {
            return _db.News
                .Include(n => n.NewsTranslators)
                .ToList();
        }

        public NewsEntity AddNews(NewsEntity news)
        {
            var result = _db.News.Add(news);
            _db.SaveChanges();
            return result.Entity;
        }

        public NewsEntity UpdateNews(NewsEntity news)
        {
            //NewsEntity newsEntity = _db.News.Find(news.NewsId);
            //for (int i=0; i<news.NewsTranslators.Count; i++)
            //{
            //    newsEntity.NewsTranslators[i].Title = news.NewsTranslators[i].Title;
            //    newsEntity.NewsTranslators[i].Description = news.NewsTranslators[i].Description;
            //    newsEntity.NewsTranslators[i].Url = news.NewsTranslators[i].Url;
            //}
            //newsEntity.Image = news.Image;
            //newsEntity.IsPublished = news.IsPublished;
            //newsEntity.PublicationDate = news.PublicationDate;
            var result = _db.News.Update(news);
            _db.SaveChanges();
            return result.Entity;
        }

        public NewsEntity GetNewsById(int id)
        {
            return _db.News
                .Include(c=>c.NewsTranslators)
                .FirstOrDefault(c=>c.NewsId == id);
        }

        public NewsEntity GetNewsByUrl(string url)
        {
            return new NewsEntity();
        }

        public void RemoveNews(int id)
        {
            NewsEntity news = _db.News.Find(id);
            if (news != null)
                _db.News.Remove(news);
            _db.SaveChanges();
        }
    }
}
