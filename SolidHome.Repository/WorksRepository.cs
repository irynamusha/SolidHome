﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SolidHome.Entity;
using SolidHome.Repository.Interfaces;

namespace SolidHome.Repository
{
    public class WorksRepository : IWorksRepository
    {
        private readonly DataContext _db;

        public WorksRepository(DataContext db)
        {
            _db = db;
        }

        public WorkEntity GetWorkByUrl(string url)
        {
            return _db.Works
                .Include(c => c.Directions)
                .ThenInclude(c=>c.Direction)
                .ThenInclude(c => c.DirectionTranslators)
                .Include(c => c.WorkTranslators)
                .Include(c => c.WorkImages)
                .ThenInclude(c => c.ImageTranslators)
                .FirstOrDefault(s => s.WorkTranslators.Select(c => c.WorkUrl).Contains(url));
        }
        public WorkEntity GetWorkById(int id)
        {
            return _db.Works
                .Include(c => c.Directions)
                .ThenInclude(c => c.Direction)
                .ThenInclude(c => c.DirectionTranslators)
                .Include(c => c.WorkTranslators)
                .Include(c => c.WorkImages)
                .ThenInclude(c => c.ImageTranslators)
                .FirstOrDefault(s => s.WorkId == id);
        }

        public IEnumerable<WorkEntity> GetWorkByDirectionId(int id)
        {
            return _db.Works
                .Include(c => c.Directions)
                .ThenInclude(c => c.Direction)
                .ThenInclude(c => c.DirectionTranslators)
                .Include(c => c.WorkTranslators)
                .Include(c => c.WorkImages)
                .ThenInclude(c => c.ImageTranslators)
                .Where(c => c.Directions.Any(d=>d.DirectionId == id));
        }

        public WorkEntity CreateWork(WorkEntity work)
        {
            work.Directions.ForEach(dw=>dw.Direction = _db.Directions.FirstOrDefault(c=>c.DirectionId == dw.DirectionId));
            var directions = work.Directions;
            //work.Directions = new List<WorkDirectionEntity>();
            var result = _db.Works.Add(work);
            _db.SaveChanges();

            //directions.ForEach(c=>c.WorkId = result.Entity.WorkId);
            //_db.WorkDirections.AddRange(directions);
            //_db.SaveChanges();
            return result.Entity;
        }

        public IEnumerable<WorkEntity> GetAllWorks()
        {
            return _db.Works
                .Include(c => c.Directions)
                .ThenInclude(c => c.Direction)
                .ThenInclude(c=>c.DirectionTranslators)
                .Include(c => c.WorkTranslators)
                .Include(c=>c.WorkImages)
                .ThenInclude(c=>c.ImageTranslators);
        }

        public void RemoveWork(int id)
        {
            var product = _db.Works.Find(id);
            if (product != null)
            {
                _db.Works.Remove(product);
            }
            _db.SaveChanges();
        }

        public WorkEntity Update(WorkEntity work)
        {
            var result = _db.Works.Update(work);
            _db.SaveChanges();
            return result.Entity;
        }
    }
}
