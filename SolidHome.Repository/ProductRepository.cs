﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SolidHome.Entity;
using SolidHome.Repository.Interfaces;

namespace SolidHome.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly DataContext _db;

        public ProductRepository(DataContext db)
        {
            _db = db;
        }
        public ProductEntity GetProductByUrl(string url)
        {
            return _db.Products
                .Include(c => c.Direction)
                .ThenInclude(c => c.DirectionTranslators)
                .Include(c=>c.ProductDescriptions)
                .ThenInclude(c=>c.ProductDescriptionTranslators)
                .Include(c => c.ProductImages)
                .ThenInclude(c=>c.ImageTranslators)
                .Include(c => c.ProductTranslators)
                .FirstOrDefault(s => s.ProductTranslators.Select(c => c.Url).Contains(url) && !s.IsDeleted);
        }
        public ProductEntity GetProductById(int id)
        {
            return _db.Products
                .Include(c => c.Direction)
                .ThenInclude(c => c.DirectionTranslators)
                .Include(c => c.ProductDescriptions)
                .ThenInclude(c => c.ProductDescriptionTranslators)
                .Include(c => c.ProductImages)
                .ThenInclude(c=>c.ImageTranslators)
                .Include(c => c.ProductTranslators)
                .FirstOrDefault(s => s.ProductId == id && !s.IsDeleted);
        }

        public ProductEntity CreateProduct(ProductEntity product)
        {
            product.ProductDescriptions = new List<ProductDescriptionEntity>();
            var result = _db.Products.Add(product);
            _db.SaveChanges();
            return result.Entity;
        }

        public IEnumerable<ProductEntity> GetAllProducts()
        {
            return _db.Products
                .Include(c => c.Direction)
                .Include(c => c.ProductTranslators)
                .Where(c => !c.IsDeleted);
        }

        public void RemoveProduct(int id)
        {
            var product = _db.Products.Find(id);
            if (product != null)
            {
                product.IsDeleted = true;
                _db.Products.Update(product);
            }
                
            _db.SaveChanges();
        }
    
        public ProductEntity Update(ProductEntity product)
        {
            var result = _db.Update(product);
            _db.SaveChanges();
            return result.Entity;
        }

        public ProductDescriptionEntity GetProductDescriptionById(int id)
        {
            return _db.ProductDescriptions
                .Include(c=>c.Product)
                .Include(c=>c.ProductDescriptionTranslators)
                .FirstOrDefault(c => c.ProductDescriptionId == id);
        }

        public ProductDescriptionEntity UpdateProductDescription(ProductDescriptionEntity productDescription)
        {
            var result = _db.Update(productDescription);
            _db.SaveChanges();
            return result.Entity;
        }

        public ProductDescriptionEntity CreateProductDescription(ProductDescriptionEntity productDescription)
        {
            var result = _db.ProductDescriptions.Add(productDescription);
            _db.SaveChanges();
            return result.Entity;
        }

        public void RemoveProductDescription(int id)
        {
            var description = _db.ProductDescriptions.Find(id);
            if (description != null)
            {
                _db.ProductDescriptions.Remove(description);
                _db.SaveChanges();
            }
                
        }
    }
}
