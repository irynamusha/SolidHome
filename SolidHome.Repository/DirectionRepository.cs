﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SolidHome.Entity;
using SolidHome.Repository.Interfaces;

namespace SolidHome.Repository
{
    public class DirectionRepository : IDirectionRepository
    {
        private readonly DataContext _db;

        public DirectionRepository(DataContext db)
        {
            _db = db;
        }

        public DirectionEntity GetDirectionByUrl(string url, bool includeProducts = false)
        {
            if (includeProducts)
            {
                var result = _db.Directions
                    .Include(c => c.DirectionTranslators)
                    .Include(c=>c.Products)
                    .ThenInclude(t=>t.ProductTranslators)
                    .FirstOrDefault(c => c.Url == url);
                if (result == null)
                    return null;
                result.Products = result.Products.Where(c =>! c.IsDeleted).ToList();
                return result;
            }
            
            return _db.Directions
                .Include(c=>c.DirectionTranslators)
                .FirstOrDefault(c => c.Url == url);
        }

        public IEnumerable<DirectionEntity> GetAllDirections()
        {
            return _db.Directions
                .Include(c => c.DirectionTranslators);
        }

        public DirectionEntity GetDirectionByProductId(int id)
        {
            return _db.Products
                .Include(c => c.Direction)
                .FirstOrDefault(c => c.ProductId == id)
                ?.Direction;
        }

        public DirectionEntity GetDirectionById(int id)
        {
            return _db.Directions
                .Include(c => c.DirectionTranslators)
                .FirstOrDefault(c => c.DirectionId == id);
        }

        public List<DirectionEntity> GetDirectionsById(List<int> id)
        {
            return _db.Directions
                .Include(c => c.DirectionTranslators)
                .Where(c => id.Contains(c.DirectionId))
                .ToList();
        }
    }
}
