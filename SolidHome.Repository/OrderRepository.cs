﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolidHome.Entity;
using SolidHome.Repository.Interfaces;

namespace SolidHome.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly DataContext _db;
        public OrderRepository(DataContext db)
        {
            _db = db;
        }

        public IEnumerable<OrderEntity> GetAllOrders()
        {
            return _db.Orders.ToList();
        }

        public OrderEntity AddOrder(OrderEntity order)
        {
            var result = _db.Orders.Add(order);
            _db.SaveChanges();
            return result.Entity;
        }

        public void Remove(int id)
        {
            var result = _db.Orders.Find(id);
            if (result != null)
                _db.Orders.Remove(result);
            _db.SaveChanges();
        }
    }
}
