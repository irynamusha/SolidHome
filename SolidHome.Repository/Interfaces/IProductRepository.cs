﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Entity;

namespace SolidHome.Repository.Interfaces
{
    public interface IProductRepository
    {
        ProductEntity GetProductByUrl(string url);
        ProductEntity GetProductById(int id);
        Entity.ProductEntity CreateProduct(ProductEntity product);
        IEnumerable<ProductEntity> GetAllProducts();
        void RemoveProduct(int id);
        ProductEntity Update(ProductEntity product);
        ProductDescriptionEntity GetProductDescriptionById(int id);
        ProductDescriptionEntity UpdateProductDescription(ProductDescriptionEntity productDescription);
        ProductDescriptionEntity CreateProductDescription(ProductDescriptionEntity productDescription);
        void RemoveProductDescription(int id);
    }
}
