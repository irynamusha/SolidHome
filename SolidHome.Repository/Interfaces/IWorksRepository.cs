﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Entity;

namespace SolidHome.Repository.Interfaces
{
    public interface IWorksRepository
    {
        WorkEntity GetWorkByUrl(string url);
        WorkEntity GetWorkById(int id);
        IEnumerable<WorkEntity> GetWorkByDirectionId(int id);
        WorkEntity CreateWork(WorkEntity work);
        IEnumerable<WorkEntity> GetAllWorks();
        void RemoveWork(int id);
        WorkEntity Update(WorkEntity work);
    }
}
