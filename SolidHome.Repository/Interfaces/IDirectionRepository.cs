﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Entity;

namespace SolidHome.Repository.Interfaces
{
    public interface IDirectionRepository
    {
        DirectionEntity GetDirectionByUrl(string url, bool includeProducts = false);
        IEnumerable<DirectionEntity> GetAllDirections();
        DirectionEntity GetDirectionByProductId(int id);
        DirectionEntity GetDirectionById(int id);
        List<DirectionEntity> GetDirectionsById(List<int> id);
    }
}
