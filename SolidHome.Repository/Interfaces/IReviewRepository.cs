﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Entity;

namespace SolidHome.Repository.Interfaces
{
    public interface IReviewRepository
    {
        List<ReviewEntity> GetAllReviews(bool? approved = null);
        void RemoveReview(int id);
        ReviewEntity AddReview(ReviewEntity review);
        ReviewEntity ChangeApproveReview(int id);
    }
}
