﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Entity;

namespace SolidHome.Repository.Interfaces
{
    public interface INewsRepository
    {
        IEnumerable<NewsEntity> GetAllNews();
        NewsEntity AddNews(NewsEntity news);
        NewsEntity UpdateNews(NewsEntity news);
        NewsEntity GetNewsById(int id);
        NewsEntity GetNewsByUrl(string url);
        void RemoveNews(int id);
    }
}
