﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Entity;

namespace SolidHome.Repository.Interfaces
{
    public interface IOrderRepository
    {
        IEnumerable<OrderEntity> GetAllOrders();
        OrderEntity AddOrder(OrderEntity order);
        void Remove(int id);
    }
}
