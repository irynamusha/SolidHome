﻿using System;
using System.Collections.Generic;
using System.Text;
using SolidHome.Entity;

namespace SolidHome.Repository.Interfaces
{
    public interface IMessageRepository
    {
        MessageEntity AddMessage(MessageEntity message);
        IEnumerable<MessageEntity> GetAllMessages();
        MessageEntity GetMessageById(int id);
        void RemoveMessage(int id);
    }
}
