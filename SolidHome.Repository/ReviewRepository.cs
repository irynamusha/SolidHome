﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolidHome.Entity;
using SolidHome.Repository.Interfaces;

namespace SolidHome.Repository
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly DataContext _db;

        public ReviewRepository(DataContext db)
        {
            _db = db;
        }

        public List<ReviewEntity> GetAllReviews(bool? approved = null)
        {
            return _db.Reviews
                .Where(c=> approved == null || c.Approved == approved)
                .ToList();
        }

        public void RemoveReview(int id)
        {
            var review = _db.Reviews.Find(id);
            if (review != null)
                _db.Reviews.Remove(review);
            _db.SaveChanges();
        }

        public ReviewEntity AddReview(ReviewEntity review)
        {
            var result = _db.Reviews.Add(review).Entity;
            _db.SaveChanges();
            return result;
        }

        public ReviewEntity ChangeApproveReview(int id)
        {
            var review = _db.Reviews.Find(id);
            review.Approved = !review.Approved;
            if (review.Approved)
            {
                review.ApproveDate = DateTime.Now;
            }
            var result = _db.Update(review).Entity;
            _db.SaveChanges();
            return result;
        }

    }
}
