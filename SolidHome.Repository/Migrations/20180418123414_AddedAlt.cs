﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SolidHome.Repository.Migrations
{
    public partial class AddedAlt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MainImageAlt",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Alt",
                table: "Images",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MainImageAlt",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Alt",
                table: "Images");
        }
    }
}
