﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SolidHome.Repository.Migrations
{
    public partial class AddedLangAlts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MainImageAlt",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Alt",
                table: "Images");

            migrationBuilder.AddColumn<int>(
                name: "ImageId",
                table: "Translators",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MainImageAlt",
                table: "Translators",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Translators_ImageId",
                table: "Translators",
                column: "ImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Translators_Images_ImageId",
                table: "Translators",
                column: "ImageId",
                principalTable: "Images",
                principalColumn: "ImageId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Translators_Images_ImageId",
                table: "Translators");

            migrationBuilder.DropIndex(
                name: "IX_Translators_ImageId",
                table: "Translators");

            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "Translators");

            migrationBuilder.DropColumn(
                name: "MainImageAlt",
                table: "Translators");

            migrationBuilder.AddColumn<string>(
                name: "MainImageAlt",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Alt",
                table: "Images",
                nullable: true);
        }
    }
}
