﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SolidHome.Repository.Migrations
{
    public partial class AddedWorks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WorkImageId",
                table: "Translators",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "Translators",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WorkId",
                table: "Translators",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkMainImageAlt",
                table: "Translators",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkMetaDescription",
                table: "Translators",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkMetaTitle",
                table: "Translators",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkUrl",
                table: "Translators",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Works",
                columns: table => new
                {
                    WorkId = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    MainImage = table.Column<string>(nullable: true),
                    WorkDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Works", x => x.WorkId);
                });

            migrationBuilder.CreateTable(
                name: "WorkDirections",
                columns: table => new
                {
                    DirectionId = table.Column<int>(nullable: false),
                    WorkId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkDirections", x => new { x.DirectionId, x.WorkId });
                    table.ForeignKey(
                        name: "FK_WorkDirections_Directions_DirectionId",
                        column: x => x.DirectionId,
                        principalTable: "Directions",
                        principalColumn: "DirectionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorkDirections_Works_WorkId",
                        column: x => x.WorkId,
                        principalTable: "Works",
                        principalColumn: "WorkId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkImages",
                columns: table => new
                {
                    WorkImageId = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Path = table.Column<string>(nullable: true),
                    WorkId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkImages", x => x.WorkImageId);
                    table.ForeignKey(
                        name: "FK_WorkImages_Works_WorkId",
                        column: x => x.WorkId,
                        principalTable: "Works",
                        principalColumn: "WorkId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Translators_WorkImageId",
                table: "Translators",
                column: "WorkImageId");

            migrationBuilder.CreateIndex(
                name: "IX_Translators_WorkId",
                table: "Translators",
                column: "WorkId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkDirections_WorkId",
                table: "WorkDirections",
                column: "WorkId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkImages_WorkId",
                table: "WorkImages",
                column: "WorkId");

            migrationBuilder.AddForeignKey(
                name: "FK_Translators_WorkImages_WorkImageId",
                table: "Translators",
                column: "WorkImageId",
                principalTable: "WorkImages",
                principalColumn: "WorkImageId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Translators_Works_WorkId",
                table: "Translators",
                column: "WorkId",
                principalTable: "Works",
                principalColumn: "WorkId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Translators_WorkImages_WorkImageId",
                table: "Translators");

            migrationBuilder.DropForeignKey(
                name: "FK_Translators_Works_WorkId",
                table: "Translators");

            migrationBuilder.DropTable(
                name: "WorkDirections");

            migrationBuilder.DropTable(
                name: "WorkImages");

            migrationBuilder.DropTable(
                name: "Works");

            migrationBuilder.DropIndex(
                name: "IX_Translators_WorkImageId",
                table: "Translators");

            migrationBuilder.DropIndex(
                name: "IX_Translators_WorkId",
                table: "Translators");

            migrationBuilder.DropColumn(
                name: "WorkImageId",
                table: "Translators");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "Translators");

            migrationBuilder.DropColumn(
                name: "WorkId",
                table: "Translators");

            migrationBuilder.DropColumn(
                name: "WorkMainImageAlt",
                table: "Translators");

            migrationBuilder.DropColumn(
                name: "WorkMetaDescription",
                table: "Translators");

            migrationBuilder.DropColumn(
                name: "WorkMetaTitle",
                table: "Translators");

            migrationBuilder.DropColumn(
                name: "WorkUrl",
                table: "Translators");
        }
    }
}
