﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SolidHome.Entity;
using SolidHome.Entity.TranslatorEntities;

namespace SolidHome.Repository
{
    public sealed class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<WorkDirectionEntity>()
                .HasKey(t => new { t.DirectionId, t.WorkId });

            modelBuilder.Entity<WorkDirectionEntity>()
                .HasOne(sc => sc.Work)
                .WithMany(s => s.Directions)
                .HasForeignKey(sc => sc.WorkId);

            modelBuilder.Entity<WorkDirectionEntity>()
                .HasOne(sc => sc.Direction)
                .WithMany(c => c.Works)
                .HasForeignKey(sc => sc.DirectionId);


            modelBuilder.Entity<LanguageEntity>(entity => {
                entity.Property(m => m.Code).HasMaxLength(10);

            });
            modelBuilder.Entity<ProductEntity>()
                .HasOne(u => u.Direction)
                .WithMany(u => u.Products)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ProductDescriptionEntity>()
                .HasOne(u => u.Product)
                .WithMany(u => u.ProductDescriptions)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

          

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<TranslatorEntity> Translators { set; get; }
        public DbSet<DirectionTranslatorEntity> DirectionTranslators { set; get; }
        public DbSet<NewsTranslatorEntity> NewsTranslators { set; get; }
        public DbSet<ProductTranslatorEntity> ProductTranslators { set; get; }
        public DbSet<WorkTranslatorEntity> WorkTranslators { set; get; }
        public DbSet<WorkImageTranslatorEntity> WorkImageTranslators { set; get; }
        public DbSet<ProductDescriptionTranslatorEntity> ProductDescriptionTranslators { set; get; }
        public DbSet<WorkDirectionEntity> WorkDirections { set; get; }

        public DbSet<WorkEntity> Works { set; get; }
        public DbSet<MessageEntity> Messages { set; get; }
        public DbSet<LanguageEntity> Languages { set; get; }
        public DbSet<NewsEntity> News { set; get; }
        public DbSet<DirectionEntity> Directions { set; get; }
        public DbSet<ProductEntity> Products { set; get; }
        public DbSet<ImageEntity> Images { set; get; }
        public DbSet<WorkImageEntity> WorkImages { set; get; }
        public DbSet<OrderEntity> Orders { set; get; }
        public DbSet<ReviewEntity> Reviews { set; get; }
        public DbSet<ProductDescriptionEntity> ProductDescriptions { set; get; }

    }
}
