﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolidHome.Entity;
using SolidHome.Repository.Interfaces;

namespace SolidHome.Repository
{
    public class MessageRepository : IMessageRepository
    {
        private readonly DataContext _db;
        public MessageRepository(DataContext db)
        {
            _db = db;
        }
        public MessageEntity AddMessage(MessageEntity message)
        {
            var result = _db.Messages.Add(message);
            _db.SaveChanges();
            message.MessageId = result.Entity.MessageId;
            return message;
        }

        public IEnumerable<MessageEntity> GetAllMessages()
        {
            return _db.Messages.ToList();
        }

        public MessageEntity GetMessageById(int id)
        {
            return _db.Messages.Find(id);
        }

        public void RemoveMessage(int id)
        {
            MessageEntity message = _db.Messages.Find(id);
            _db.Messages.Remove(message);
            _db.SaveChanges();
        }
    }
}
