﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Routing;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using SolidHome;
using SolidHome.Localization;
using SolidHome.Middleware;
using SolidHome.Models;
using SolidHome.Repository;
using SolidHome.Repository.Interfaces;
using SolidHome.Service;
using SolidHome.Service.Interfaces;
using SolidHome.Utils.Service;

namespace SolidHome
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {


            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddDbContext<DataContext>(options =>
                options.UseMySQL(Configuration.GetConnectionString("MYSQLConnection")));
            //services.AddAutoMapper();  
            services.AddMvc()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization(options =>
                {
                    options.DataAnnotationLocalizerProvider = (type, factory) =>
                        factory.Create(typeof(Common));
                });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Account/LogIn";
                    options.LogoutPath = "/Account/Logout";
                });

            services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Optimal);
            services.AddResponseCaching();
            services.AddResponseCompression(options =>
            {
                options.EnableForHttps = true;
                options.MimeTypes = new[]
                {
                    "image/svg+xml",
                    "text/plain",
                    "text/css",
                    "application/javascript",
                    "text/html",
                    "application/xml",
                    "text/xml",
                    "application/json",
                    "text/json"
                };
            });

            services.Configure<RouteOptions>(options =>
            {
                options.ConstraintMap.Add("lang", typeof(LanguageRouteConstraint));
            });

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();

            //services
            services.AddTransient<INewsService, NewsService>();
            services.AddTransient<IMessageService, MessageService>();
            services.AddTransient<IDirectionService, DirectionService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<ICatalogService, CatalogService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IReviewService, ReviewService>();
            services.AddTransient<IWorksService, WorksService>();

            //repositories
            services.AddTransient<INewsRepository, NewsRepository>();
            services.AddTransient<IDirectionRepository, DirectionRepository>();
            services.AddTransient<IMessageRepository, MessageRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IReviewRepository, ReviewRepository>();
            services.AddTransient<IWorksRepository, WorksRepository>();

            services.AddTransient<UrlService>();
            services.AddTransient<ImageService>();
            services.AddTransient<JsonService>();
            services.AddTransient<CacheService>();
            services.AddTransient<ContactsService>();
            services.AddTransient<MessageService>();
            services.AddTransient<EmailService>();
            services.AddTransient<IpService>();
            services.AddTransient<SitemapGenerator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            app.Use((context, next) =>
            {
                context.Request.Scheme = "https";
                return next();
            });
            MapperService.Config();
            app.UseStatusCodePagesWithReExecute("/error/{0}");
            app.UseMiddleware<RouteMiddleware>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                //app.UseExceptionHandler("/error");
            }
            else
            {
                app.UseExceptionHandler("/error");
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseResponseCompression();


            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 60 * 60 * 24;
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] =
                        "public,max-age=" + durationInSeconds;
                }
            });

            app.UseFileServer(new FileServerOptions()
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(env.ContentRootPath, "node_modules")
                ),
                RequestPath = "/node_modules",
                EnableDirectoryBrowsing = false
            });



            app.UseAuthentication();
            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);

            app.UseMvc(routes =>
            {
                //routes.MapRoute("category_admin", "admin/catalog/{url}",
                //    defaults: new { area="Admin", controller = "Catalog", action = "Direction" });
                routes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{controller=News}/{action=Index}/{id?}");

                routes.MapRoute(
                    "LangHomeIndexRedirect",
                    "{lang:lang}/Home/Index",
                    defaults: new { controller = "Home", action = "Index", redirect = true }
                );
                routes.MapRoute(
                    "HomeIndexRedirect",
                    "Home/Index",
                    defaults: new { controller = "Home", action = "Index", redirect = true }
                );
                routes.MapRoute(
                    "LangHomeRedirect",
                    "{lang:lang}/Home",
                    defaults: new { controller = "Home", action = "Index", redirect = true }
                );
                routes.MapRoute(
                    "HomeRedirect",
                    "Home",
                    defaults: new { controller = "Home", action = "Index", redirect = true }
                );

                routes.MapRoute("login", "login",
                    defaults: new { controller = "Account", action = "Login" });

                //[Route("api/{lang:lang}/order")]
                //[Route("api/order")]

                routes.MapRoute("order_lang", "api/{lang:lang}/order",
                    defaults: new { controller = "OrderApi", action = "MakeOrder" });
                routes.MapRoute("order_uk", "api/order",
                    defaults: new { controller = "OrderApi", action = "MakeOrder" });

                routes.MapRoute("news_ru_amp", "{lang:lang}/news/{url}-{id:int}/amp",
                    defaults: new { controller = "News", action = "NewsItemAmp" });
                routes.MapRoute("news_uk_amp", "news/{url}-{id:int}/amp",
                    defaults: new { controller = "News", action = "NewsItemAmp" });

                routes.MapRoute("news_ru", "{lang:lang}/news/{url}-{id:int}",
                     defaults: new { controller = "News", action = "NewsItem" });
                routes.MapRoute("news_uk", "news/{url}-{id:int}",
                    defaults: new { controller = "News", action = "NewsItem" });

                routes.MapRoute("direction_lang", "{lang:lang}/works/{url}-{id:int}",
                    defaults: new { controller = "Works", action = "Work" });
                routes.MapRoute("direction_uk", "works/{url}-{id:int}",
                    defaults: new { controller = "Works", action = "Work" });

                routes.MapRoute("singleCatalogItem_lang_amp", "{lang:lang}/catalog/{url}/amp",
                    defaults: new { controller = "Catalog", action = "CatalogItemAmp" });
                routes.MapRoute("singleCatalogItem_uk_amp", "catalog/{url}/amp",
                    defaults: new { controller = "Catalog", action = "CatalogItemAmp" });

                routes.MapRoute("product_lang_amp", "{lang:lang}/catalog/{url}/{productUrl}/amp",
                    defaults: new { controller = "Catalog", action = "ProductAmp" });
                routes.MapRoute("product_uk_amp", "catalog/{url}/{productUrl}/amp",
                    defaults: new { controller = "Catalog", action = "ProductAmp" });


                routes.MapRoute("catalog_lang", "{lang:lang}/catalog/{url}",
                    defaults: new { controller = "Catalog", action = "CatalogItem" });
                routes.MapRoute("catalog_uk", "catalog/{url}",
                    defaults: new { controller = "Catalog", action = "CatalogItem" });


                routes.MapRoute("product_lang", "{lang:lang}/catalog/{url}/{productUrl}",
                    defaults: new { controller = "Catalog", action = "Product" });
                routes.MapRoute("product_uk", "catalog/{url}/{productUrl}",
                    defaults: new { controller = "Catalog", action = "Product" });


                routes.MapRoute(
                    name: "LocalizedDefault",
                    template: "{lang:lang}/{controller=Home}/{action=Index}/{id?}"
                );
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}"
                );


            });


            app.UseCors(corsPolictyBuilder =>
            {
                corsPolictyBuilder.WithOrigins("*.localhost:57864");
            });
        }
    }
}
