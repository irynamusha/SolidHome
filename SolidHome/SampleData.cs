﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SolidHome.Entity;
using SolidHome.Entity.TranslatorEntities;
using SolidHome.Repository;
using SolidHome.Utils.Service;

namespace SolidHome
{
    public class SampleData
    {
        public static void Initialize(DataContext context, UrlService urlService)
        {
            if (!context.Languages.Any())
            {
                context.Languages.AddRange(
                    new LanguageEntity() { Code = "ru", Name = "Русский" },
                    new LanguageEntity() { Code = "uk", Name = "Українська" }
                );
                context.SaveChanges();
            }
            if (!context.News.Any())
            {
                context.News.AddRange(
                    new NewsEntity() { Image = "newphoto1.png", CreateDate = DateTime.Now, PublicationDate = DateTime.Now, IsPublished = true },
                    new NewsEntity() { Image = "newphoto1.png", CreateDate = DateTime.Now, PublicationDate = DateTime.Now, IsPublished = true }
                );
                context.SaveChanges();
            }
            if (!context.NewsTranslators.Any())
            {
                context.NewsTranslators.AddRange(
                    new NewsTranslatorEntity()
                    {
                        Title = "У київських тролейбусах встановили камери спостереження",
                        Url = urlService.GetEncodedUrlString("У київських тролейбусах встановили камери спостереження"),
                        Description = "У Києві почався експеримент із впровадження камер спостереження у громадському транспорті. Перші камери встановили у столичних тролейбусах № 5, 18, 25, 26, 23. Як розповіли газеті Вести у Київпастрансі,дане нововведення здійснене з метою стеження за буйними пасажирами,для відстеження кишенькових злодіїв і тих, хто залишився на підніжці, щоб їх не затиснуло дверима, що закриваються. Камери спостереження встановили у 17 тролейбусах моделі ЛАЗ та у 20 моделі Богдан.Камери у них стоять над другими, третіми і четвертими дверима, а також зовні - для огляду ззаду. \"Вони можуть знімати вдень і вночі. У кабіні водія є екрани, на яких він моніторить ситуацію. Якщо, наприклад, побачить бійку, то викличе міліцію\", -розповів головний інженер служби автоматики і зв'язку Київпастрансу Федір Новосад.",
                        Language = context.Languages.Find("uk"), News = context.News.ToList()[0]
                    },
                    new NewsTranslatorEntity()
                    {
                        Title = "В киевских тролейбусах установили камеры наблюдения",
                        Url = urlService.GetEncodedUrlString("В киевских тролейбусах установили камеры наблюдения"),
                        Description = "В Киеве начался эксперимент по внедрению камер наблюдения в общественном транспорте. Первые камеры установили в столичных троллейбусах № 5, 18, 25, 26, 23 Как рассказали газете Вести в Киевпастрансе, данное нововведение совершенное с целью слежения за буйными пассажирами, для отслеживания карманников и тех, кто остался на подножке, чтобы их зажало закрывающейся дверью. Камеры наблюдения установили в 17 троллейбусах модели ЛАЗ и в 20 модели Богдан.Камеры в них стоят над другими, третьими и четвертыми дверью, а также снаружи - для осмотра сзади. \"Они могут снимать днем ​​и ночью.В кабине водителя есть экраны, на которых он мониторит ситуацию.Если, например, увидит драку, то вызовет милицию \", -рассказал главный инженер службы автоматики и связи Киевпастранса Федор Новосад.",
                        Language = context.Languages.Find("ru"), News = context.News.ToList()[0]
                    },
                    new NewsTranslatorEntity()
                    {
                        Title = "Охоронні системи для захисту домівки: як вони працюють та скільки коштують",
                        Url = urlService.GetEncodedUrlString("Охоронні системи для захисту домівки: як вони працюють та скільки коштують"),
                        Description = "З кожним роком в Україні зростає кількість крадіжок. Така сумна статистика від правоохоронців. Тому все більше українців вдаються до послуг охоронних систем. Сьогодні найбільш ефективним захистом охорони від крадіжок та злому є охоронні системи безпеки – це відео спостереження та охоронна сигналізація квартир. Сигналізація може бути автономною, або виведеною на пункт централізованої охорони.У першому випадку повідомлення про візит непроханих гостей надходить господарю, а у квартирі вмикається сигнал тривоги, який може відлякати порушників. Якщо ж підключити систему до централізованої охорони, майно охоронятимуть спеціальні підрозділи приватних фірм, або державних структур.У квартирі встановлюють спеціальні датчики, які реагують на рух чи тепло.Коли злодії проникають в приміщення, вони спрацьовують і передають сигнал охоронній фірмі.Група реагування одразу ж виїжджає, і через 3 - 5 хвилин буде на місці.Це за умови, що помешкання знаходиться у місті.У селі такий час збільшується до 10 - 12 хвилин.",
                        Language = context.Languages.Find("uk"),
                        News = context.News.ToList()[1]
                    },
                    new NewsTranslatorEntity()
                    {
                        Title = "Охранные системы для защиты дома: как они работают и сколько стоят",
                        Url = urlService.GetEncodedUrlString("Охранные системы для защиты дома: как они работают и сколько стоят"),
                        Description = "С каждым годом в Украине растет количество краж. Такая печальная статистика от правоохранителей. Поэтому все больше украинский прибегают к услугам охранных систем. Сегодня наиболее эффективной защитой охраны от краж и взлома есть охранные системы безопасности - это видеонаблюдения и охранная сигнализация квартир. Сигнализация может быть автономной, или выведенной на пункт централизованной охорони.У первом случае сообщение о визите незваных гостей поступает хозяину, а в квартире включается сигнал тревоги, который может отпугнуть нарушителей. Если же подключить систему к централизованной охраны, имущество охранять специальные подразделения частных фирм, или государственных структур.У квартире устанавливают специальные датчики, которые реагируют на движение или тепло.Колы воры проникают в помещение, они срабатывают и передают сигнал охранной фирми.Група реагирования сразу же выезжает, и через 3 - 5 минут будет на мисци.Це при условии, что помещение находится в мисти.У селе такое время увеличивается до 10 - 12 минут.",
                        Language = context.Languages.Find("ru"),
                        News = context.News.ToList()[1]
                    }
                );
                context.SaveChanges();
            }
            



            if (!context.Directions.Any())
            {
                context.Directions.AddRange(
                        new DirectionEntity() { Url = "video-surveillance", IconStyle = "IconVideo" },
                        new DirectionEntity() { Url = "security-alarm", IconStyle = "IconSecureSignal" },
                        new DirectionEntity() { Url = "access-controll", IconStyle = "IconLockControl" },
                        new DirectionEntity() { Url = "telephony", IconStyle = "IconPhone" },
                        new DirectionEntity() { Url = "engineering-infrastructure", IconStyle = "IconCable" },
                        new DirectionEntity() { Url = "audiovisual-system", IconStyle = "IconAudio" },
                        new DirectionEntity() { Url = "gps-tracking", IconStyle = "IconGPS" },
                        new DirectionEntity() { Url = "smart-house", IconStyle = "IconSmartHome" }

                    );
                context.SaveChanges();
            }

            if (!context.DirectionTranslators.Any())
            {
                context.DirectionTranslators.AddRange(
                    new DirectionTranslatorEntity()
                    {
                        Title = "Відеоспостереження",
                        Language = context.Languages.Find("uk"),
                        Description = "<section class=\"main-video-photo\"> <div class=\"tittle-padd\"><h1 class=\"main-tittle text-center\">ВІДЕОНАГЛЯД</h1></div></section><section class=\"short-video-info\"> <div class=\"container\"></div></section><section class=\"main-video-info\"> <div class=\"container\"> <div class=\"row justify-content-between\"> <div class=\"item col-lg-4 col-md-6 col-sm-6\"> <div class=\"video-icons d-flex\"> <i class=\"icons VideoIcon1 m-auto\"></i> </div><div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark\">Контроль за територією</span> <span class=\"video-sub-tittle m-auto text-justify\">Камери відеоспостереження будуть вашими очима , Ви зможете відслідковувати ситуації в місцях встановлення камер як на місці так і через Інтернет. Знаючи що встановлені камери моніторингу: працівники будуть більш дисциплінованими та обережними, що підвищить продуктивність праці, а також це значний стримуючий фактор для злочинців, та можливість швидкого реагування на крадіжку.</span> </div></div><div class=\"item col-lg-4 col-md-6 col-sm-6\"> <div class=\"video-icons d-flex\"> <i class=\"icons VideoIcon2 m-auto\"></i> </div><div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark\">Запис усіх подій</span> <span class=\"video-sub-tittle m-auto text-justify\">Найнадійніша система безпеки , давачі якої, охороняють ваш будинок. Безпека абсолютно всіх приміщень, а також прибудинкових територій - під найсуворішим контролем охоронної системи. Ви завжди в курсі того, що відбувається у вашому будинку, офісі чи магазині.</span> </div></div><div class=\"item col-lg-4 col-md-6 col-sm-6\"> <div class=\"video-icons d-flex\"> <i class=\"icons VideoIcon3 m-auto\"></i> </div><div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark\">Віддалений перегляд</span> <span class=\"video-sub-tittle m-auto text-justify\">Домофон веде статистику всіх ваших відвідувачів, записує для вас повідомлення та фото відвідувачів. Ведення відео архіву допоможе фіксувати всі події в місцях спостереження та допоможе  захистити вашу сторону в спірних ситуаціях. Якщо немає потреби цілодобово вести відео архів, ми зможемо налаштувати ведення відео архіву тільки в певний час або лише на момент спрацювання датчика руху камери.</span> </div></div><div class=\"item col-lg-4 col-md-6 col-sm-6\"> <div class=\"video-icons d-flex\"> <i class=\"icons VideoIcon4 m-auto\"></i> </div><div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark\">Розпізнавання лиць</span> <span class=\"video-sub-tittle m-auto text-justify\">Завдяки цьому можна більш якісно організувати систему протидії небажаного проникнення на територію об’єкту. Відеоспостереження можна пов’язати з турнікетами і організувати автоматичний пункт пропуску за принципом «свій-чужий».</span> </div></div><div class=\"item col-lg-4 col-md-6 col-sm-6\"> <div class=\"video-icons d-flex\"> <i class=\"icons VideoIcon5 m-auto\"></i> </div><div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark\">Система розпізнавання номерів</span> <span class=\"video-sub-tittle m-auto text-justify\">Ефективне зчитування номерних знаків автомобіля дозволяє контролювати транспортний потік та запобігти не санкціонованому проїзду.</span> </div></div><div class=\"item col-lg-4 col-md-6 col-sm-6\"> <div class=\"video-icons d-flex\"> <i class=\"icons VideoIcon6 m-auto\"></i> </div><div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark\">Технічна підтримка</span> <span class=\"video-sub-tittle m-auto text-justify\">Завдяки висококваліфікованим спеціалістам і найкращому обладнанню, ми даємо гарантію на всі види послуг, та гарантію на все обладнання. Індивідуальний підхід та обслуговування, можливість коректування і зміни конфігурації в залежності від ваших побажань.</span> </div></div></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"><li class=\"text-left\">Аналогові системи високої роздільної здатності</li><li>Цифрові ІР технології</li><li>Центри моніторингу</li><li>Смарт-функції</li></ul>",
                        Direction = context.Directions.ToList()[0]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Видеонаблюдение",
                        Language = context.Languages.Find("ru"),
                        Description = "<section class=\"main-video-photo\"> <div class=\"tittle-padd\"> <h1 class=\"main-tittle text-center\"> ВИДЕОНАБЛЮДЕНИЕ</h1> </div> </section> <section class=\"short-video-info\"> <div class=\"container\"></div> </section> <section class=\"main-video-info\"> <div class=\"container\"> <div class=\"row justify-content-between\"> <div class=\"item col-lg-4 col-md-6 col-sm-6\"> <div class=\"video-icons d-flex\"> <i class=\"icons VideoIcon1 m-auto\"> </i> </div> <div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark\"> Контроль за территорией </span> <span class=\"video-sub-tittle m-auto text-justify\"> Камеры видеонаблюдения будут вашими глазами, Вы будете отслеживать ситуации в местах установки камер как на месте так и через Интернет. Зная что установленные камеры мониторинга: работники будут дисциплинированными и осторожными что повысит производительность труда, а также это значительный сдерживающий фактор для преступников, и возможность быстрого реагирования на кражу.</span> </div> </div> <div class=\"item col-lg-4 col-md-6 col-sm-6\"> <div class=\"video-icons d-flex\"> <i class=\"icons VideoIcon2 m-auto\"> </i> </div > <div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark\"> Запись всех событий </span> <span class=\"video-sub-tittle m-auto text-justify\">Надежная система безопасности, датчики которой, охраняют ваш дом. Безопасность абсолютно всех помещений, а также придомовых территорий - под строжайшим контролем охранной системы. Вы будете в курсе того, что происходит в вашем доме, офисе или магазине.</span> </div> </div> <div class=\"item col-lg-4 col-md-6 col-sm-6\"> <div class=\"video-icons d-flex\"> <i class=\"icons VideoIcon3 m-auto\"> </i> </div> <div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark \"> Удаленный просмотр </span> <span class=\"video-sub-tittle m-auto text-justify\">Домофон ведет статистику всех ваших посетителей, записывает для вас сообщение и фото посетителей. Ведение видео архива поможет фиксировать все события в местах наблюдения и поможет защитить вашу сторону в спорных ситуациях. Если нет необходимости круглосуточно вести видео архив, мы сможем настроить ведение видео архива только в определенное время или только на момент срабатывания датчика движения камеры.</span> </div> </div> <div class=\"item col-lg-4 col-md-6 col-sm-6 \"> <div class=\"video-icons d-flex\"> <i class=\"icons VideoIcon4 m-auto\"> </i> </div> <div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark \"> Распознавание ликов </span> <span class=\"video-sub-tittle m-auto text-justify\">Благодаря этому можно более качественно организовать систему противодействия нежелательного проникновения на территорию объекта. Видеонаблюдения можно связать с турникетами и организовать автоматический пункт пропуска по принципу «свой-чужой».</span> </div> </div> <div class=\"item col-lg-4 col-md-6 col-sm-6 \"> <div class=\"video-icons d-flex\"> <i class=\"icons VideoIcon5 m-auto\"> </i> </div> <div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark\"> Система распознавания номеров </span> <span class=\"video-sub-tittle m-auto text-justify\">Эффективное считывание номерных знаков автомобиля позволяет контролировать транспортный поток и предотвратить несанкционированный проезд.</span> </div> </div> <div class=\"item col-lg-4 col-md-6 col-sm-6\"> <div class=\"video-icons d-flex \"> <i class=\" icons VideoIcon6 m-auto \"> </i> </div> <div class=\"prod-descript\"> <span class=\"video-prods-tittle sub-tittle_med_25 color-dark\"> Техническая поддержка </span> <span class=\"video-sub-tittle m-auto text-justify\">Благодаря хорошим специалистам и лучшему оборудованию, мы даем гарантию на все виды услуг и гарантию на все оборудование, индивидуальный подход и обслуживание, возможность корректировки и изменения конфигурации в зависимости от ваших пожеланий.</span> </div> </div> </div> </div> </section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"><li class=\"text-left\">Аналоговые системы высокого разрешения</li><li>Цифровые IP-технологии</li><li>Центры мониторинга</li><li>Смарт-функции</li></ul>",
                        Direction = context.Directions.ToList()[0]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Охоронна сигналізація",
                        Language = context.Languages.Find("uk"),
                        Description = "<section class=\"main-secure-photo\"> <div class=\"tittle-padd\"> <h1 class=\"main-tittle text-center\">ОХОРОННА СИГНАЛІЗАЦІЯ</h1></div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-6 col-md-6 col-12 p-0\"><img src=\"/images/directionAlarmSystem/SecureFoto.png\" class=\"w-100\"></div><div class=\"col-lg-4 col-md-6 col-12 m-auto\"><p class=\"sub-tittle_18\">Охоронна сигналізація скоріше необхідність, ніж розкіш. Вона гарантує безпеку вашого майна у разі відсутності власників. При необхідності сигналізація може підключатися до пульту централізованого управління.</p><p class=\"sub-tittle_18\">У разі проникнення зловмисника у будинок відразу надійде сигнал тривоги. Через 5 хвилин після його отримання прибуває група швидкого реагування. Співробітники з величезним досвідом роботи і спецпідготовкою мають все необхідне для затримання злочинця.</p><p class=\"sub-tittle_18\">Охоронну сигналізацію можемо умовно поділити на два види, які пропонуємо нашим клієнтам:</p><p class=\"sub-tittle_18\">1.&nbsp;&nbsp;&nbsp;&nbsp; Автономна охоронна сигналізація.</p><p class=\"sub-tittle_18\">Це набір засобів та обладнання,&nbsp; які замовник з нашою допомогою вибирає відповідно до своїх побажань та особливостей об&rsquo;єкту. Характеризується невеликою вартістю, і як наслідок меншою захищеністю майна. Замовник може на свій розсуд вибрати спосіб оповіщення у разі спрацювання сигналізації. Найменш ефективним вважається сирена, яка може відлякати непідготовленого злодія та повідомити сусідів про пограбування. Більш ефективним є повідомлення про пограбування у вигляді sms та телефонного дзвінка на телефон власника і його довірених осіб.</p><p class=\"sub-tittle_18\">2.&nbsp;&nbsp;&nbsp;&nbsp; Пультова охоронна сигналізація.</p><p class=\"sub-tittle_18\">&nbsp;На відмінну від першого варіанту відповідальність за безпеку бере на себе охоронна організація. Яка у разі тривоги прибуде на ваш об&rsquo;єкт, щоб нейтралізувати загрозу вашому майну. Це реалізовується за допомогою спеціального комунікатора, який передає сигнал із сигналізації на пульт цілодобового спостереження.</p></div></div></section>",
                        SmallDescription = " <ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"><li>Провідна</li><li>Безпровідна</li><li>Периметральна</li></ul>",
                        Direction = context.Directions.ToList()[1]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Охранная сигнализация",
                        Language = context.Languages.Find("ru"),
                        Description = "<section class=\"main-secure-photo\"> <div class=\"tittle-padd\"> <h1 class=\"main-tittle text-center\">ОХРАННАЯ СИГНАЛИЗАЦИЯ</h1></div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-6 col-md-6 col-12 p-0\"><img src=\"/images/directionAlarmSystem/SecureFoto.png\" class=\"w-100\"></div><div class=\"col-lg-4 col-md-6 col-12 m-auto\"><p class=\"sub-tittle_18\">Охранная сигнализация скорее необходимость, чем роскошь. Она гарантирует безопасность вашего имущества при отсутствии владельцев. При необходимости сигнализация может подключаться к пульту централизованного управления. В случае проникновения злоумышленника в дом сразу поступит сигнал тревоги. Через 5 минут после его получения прибывает группа быстрого реагирования. Сотрудники с большим опытом работы и спецподготовкой имеют все необходимое для задержания преступника.</p><p class=\"sub-tittle_18\">Охранную сигнализацию можем условно разделить на два вида, которые предлагаем нашим клиентам:</p><p class=\"sub-tittle_18\">1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Автономная охранная сигнализация.</p><p class=\"sub-tittle_18\">Это набор средств и оборудования, заказчик с нашей помощью выбирает в соответствии со своими пожеланиями и особенностей объекта. Характеризуется небольшой стоимости, и как следствие меньшей защищенностью имущества. Заказчик может по своему усмотрению выбрать способ оповещения при срабатывании сигнализации. Наименее эффективным считается сирена, которая отпугивает неподготовленного вора и сообщить соседей о взломе. Более эффективным есть сообщение о взломе в виде sms и телефонного звонка на телефон владельца и его доверенных лиц.</p><p class=\"sub-tittle_18\">2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Пультовая охранная сигнализация.</p><p class=\"sub-tittle_18\">&nbsp;В отличие от первого варианта ответственность за безопасность берет на себя охранная организация. Которая в случае тревоги прибудет на ваш объект, чтобы нейтрализовать угрозу вашему имуществу. Это реализуется с помощью специального коммуникатора, который передает сигнал с сигнализации на пульт круглосуточного наблюдения.</p></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"><li>Ведущая</li><li>Беспроводная</li><li>Периметральная</li></ul>",
                        Direction = context.Directions.ToList()[1]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Контроль за доступом",
                        Language = context.Languages.Find("uk"),
                        Description = "<section class=\"main-lock-photo\"> <div class=\"tittle-padd\"> <h1 class=\"main-tittle text-center\">КОНТРОЛЬ ЗА ДОСТУПОМ</h1></div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-4 col-md-6 col-12 m-auto\"><p class=\"sub-tittle_18\">Сучасна система контролю і управління доступом (СКУД) надає власнику такі &nbsp;переваги:</p><p class=\"sub-tittle_18\">&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ідентифікація особи, яка має право на доступ</p><p class=\"sub-tittle_18\">&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Відстеження переміщення транспорту або людей на території</p><p class=\"sub-tittle_18\">&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Визначити час перебування працівника на робочому місці</p><p class=\"sub-tittle_18\">&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Управління автоматичними режимами</p><p class=\"sub-tittle_18\">&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; А також багато інших переваг</p><p class=\"sub-tittle_18\">Контроль доступу зарекомендував себе в спільному застосуванні з системою відеоспостереження. Оскільки в основі системи чітко відзначені події за часом, що дозволить знайти в відеоархіві ту інформацію, яка була не відразу помічена.</p><p class=\"sub-tittle_18\">Основними перевагами контролю за доступом є можливість інтегрувати в неї чи не всі системи безпеки. І таким чином створити інтегровану систему безпеки з: турнікетами, шлагбаумами, автоматикою різного ступеня складності (механізми для перекривання води та ін.), з відеоспостереженням і охоронними системами.</p><p class=\"sub-tittle_18\">Система контролю і управління доступу необхідна для підприємств, оскільки автоматизовує значну частину процесів. Що дозволить економити кошти на щоденних повторюваних операціях.</p><p class=\"sub-tittle_18\">Можна також відзначити, що жоден готель не обходиться без системи доступу за номерами. Область застосування контролю доступу дуже великий і обмежується лише Вашою фантазією.</p></div><div class=\"col-lg-6 col-md-6 col-12 p-0\"><img src=\"/images/directionControl/LockControl.png\" class=\"w-100\"></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"><li>Автономні точки проходу</li><li>Мережевий контроль доступу</li><li>Турнікети,шлагбауми</li><li>Домофонні системи</li><li>Безпровідна карткова система</li></ul>",
                        Direction = context.Directions.ToList()[2]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Контроль доступа",
                        Language = context.Languages.Find("ru"),
                        Description = "<section class=\"main-lock-photo\"> <div class=\"tittle-padd\"> <h1 class=\"main-tittle text-center\">КОНТРОЛЬ ДОСТУПА</h1></div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-4 col-md-6 col-12 m-auto\"><p class=\"sub-tittle_18\">Современная система контроля и управления доступом (СКУД) даст вам много преимуществ:</p><ul  class=\"sub-tittle_18 \"><li>Идентификация лица, имеющего право на доступ</li><li>Отслеживание перемещения транспорта или людей на территории</li><li>Определить время пребывания работника на рабочем месте</li><li>Управление автоматическими режимами</li><li>А также много других преимуществ</li></ul><p class=\"sub-tittle_18\">Контроль доступа&nbsp; зарекомендовал себя в совместном применении с системой видеонаблюдения. Поскольку в базе системы четко отмечены события по времени, что позволит найти в видеоархиве ту информацию, которая была не сразу замечена.</p><p class=\"sub-tittle_18\">Основными преимуществами контроля за доступом является возможность интегрировать в нее едва ли не все системы безопасности. И таким образом создать интегрированную систему безопасности с: турникетами ,шлагбаумами, автоматикой разной степени сложности (механизмы для перекрывания воды и т.п.), с видеонаблюдением и охранными системами.</p><p class=\"sub-tittle_18\">Система контроля и управления доступа требуется для предприятий, поскольку автоматизирует значительную часть процессов. Что позволит экономить средства на ежедневных повторяющихся операциях.</p><p class=\"sub-tittle_18\">Можно также отметить, что ни один отель не обходится без системы доступа по номерам. Область применения контроля доступа очень большой и ограничивается только Вашей фантазией.</p></div><div class=\"col-lg-6 col-md-6 col-12 p-0\"><img src=\"/images/directionControl/LockControl.png\" class=\"w-100\"></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"><li>Автономные точки прохода</li><li>Сетевой контроль доступа</li><li>Турникеты, шлагбаумы</li><li>Домофонные системы</li><li>Беспроводная карточная система</li></ul>",
                        Direction = context.Directions.ToList()[2]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Телефонія",
                        Language = context.Languages.Find("uk"),
                        Description = "<section class=\"main-telephone-photo\"> <div class=\"tittle-padd\"><h1 class=\"main-tittle text-center\">ТЕЛЕФОНІЯ</h1></div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-6 col-md-6 col-12 p-0\"><img src=\"/images/directionPhone/TelephonePhoto.png\" class=\"w-100\"></div><div class=\"col-lg-5 col-md-6 col-12 m-auto\"><p class=\"sub-tittle_18 \">Телефонія чи не найважливіший інструмент для організації роботи на підприємтсві чи у офісі. Якщо у Вас більше десяти працівників рекомендуємо задуматися над впровадженням у себе в офісі цього чудового інструменту.</p><p class=\"sub-tittle_18\">В залежності від кількості робочих місць та фінансових можливостей пропонуємо нашим клієнтам ІР-телефонію або цифрову чи аналогову телефонію. Детально на них зупинятися не будемо, адже вибір обладнання для тих чи інших потреб є дуже індивідуальною задачею. Розглянемо краще те, що ж Ви отримаєте ставши щасливим власником офісної АТС.</p><p class=\"sub-tittle_18\">&nbsp;По-перше. Найважливіше - це те, що жоден клієнт не буде втрачений. У разі наступного вхідного дзвінка телефонна станція переадресує на наступного вільного працівника. Також є можливість записати текст вхідного привітання, що дозволить клієнту одразу попасти до бухгалтерії чи до тех. спеціалістів не відволікаючи при цьому офіс-менеджера. Крім того Ви отримаєте внутрішню коротку (переважно 4-ох цифрову) нумерацію, що дозволить спілкуватися всередині організації без проблем, а також легко переадресовувати вхідні дзвінки, якщо помилково зателефонували не до того працівника.</p><p class=\"sub-tittle_18\">&nbsp;По &ndash;друге. У разі вхідних дзвінків до Вас в позаробочий час, телефонна стація зможе записати вхідне повідомлення чи прийме факс. Погодьтесь це дуже зручно як Вам, так і Вашим клієнтам.</p><p class=\"sub-tittle_18\">&nbsp;По-третє. Коли Ваша організація буде розширюватися, Вам не потрібно буде міняти обладнання, а лише доставити додаткові модулі, що є дуже економно та практично.</p></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"> <li>Програмування АТС Samsung OS</li><li>Адміністрування АТС</li><li>ІР і SIP технології</li><li>Мережеві рішення на базі mini ATC</li></ul>",
                        Direction = context.Directions.ToList()[3]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Телефония",
                        Language = context.Languages.Find("ru"),
                        Description = "<section class=\"main-telephone-photo\"> <div class=\"tittle-padd\"><h1 class=\"main-tittle text-center\">ТЕЛЕФОНИЯ</h1></div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-6 col-md-6 col-12 p-0\"><img src=\"/images/directionPhone/TelephonePhoto.png\" class=\"w-100\"></div><div class=\"col-lg-5 col-md-6 col-12 m-auto\"><p class=\"sub-tittle_18 \">Телефония ли не самый важный инструмент для организации работы на предприятии или в офисе. Если у Вас больше десяти работников рекомендуем задуматься над внедрением у себя в офисе этого замечательного инструмента.</p><p class=\"sub-tittle_18\">В зависимости от количества рабочих мест и финансовых возможностей предлагаем нашим клиентам IP-телефонию или цифровую или аналоговую телефонию. Подробно на них останавливаться не будем, ведь выбор оборудования для тех или иных нужд очень индивидуальной задачей. Рассмотрим лучше то, что же Вы получите став счастливым обладателем офисной АТС.</p><p class=\"sub-tittle_18\">&nbsp;Во-первых. Самое важное - это то, что ни один клиент не будет утрачено. В случае последующего вызова телефонная станция переадресует на следующий свободного работника. Также есть возможность записать текст входящего поздравления, что позволит клиенту сразу попасть в бухгалтерию или к&nbsp; тех. специалистам не отвлекая при этом офис-менеджера. Кроме того, Вы получите внутреннюю короткую (преимущественно 4-х цифровую) нумерацию, что позволит общаться внутри организации без проблем, а также легко переадресация входящих вызовов, если ошибочно позвонили не до того работника.</p><p class=\"sub-tittle_18\">&nbsp;Во-вторых. В случае входящих звонков к Вам в нерабочее время, телефонная стация сможет записать входящее сообщение или примет факс. Согласитесь это очень удобно как Вам, так и Вашим клиентам.</p><p class=\"sub-tittle_18\">&nbsp;В-третьих. Когда Ваша организация будет расширяться, Вам не нужно будет менять оборудование, а лишь доставить дополнительные модули, очень экономно и практично.</p></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"><li>Программирование АТС Samsung OS</li><li>Администрирование АТС</li><li>ІР і SIP технологии</li><li>Сетевые решения на базе mini ATC</li></ul>",
                        Direction = context.Directions.ToList()[3]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Структурована кабельна система (СКС)",
                        Language = context.Languages.Find("uk"),
                        Description = "<section class=\"main-inzener-photo\"> <div class=\"tittle-padd\"><h1 class=\"main-tittle text-center\">СТРУКТУРОВАНА КАБЕЛЬНА СИСТЕМА (СКС)</h1></div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-6 col-md-6 col-12 p-0\"><img src=\"/images/directionCable/InzPhoto.png\" class=\"w-100\"></div><div class=\"col-lg-4 col-md-6 col-12 m-auto\"><p class=\"sub-tittle_18 \">Структурована кабельна система (СКС) являє собою ієрархічну кабельну систему будівлі або групи будівель, розділену на структурні підсистеми. Ми прокладаємо всі типи СКС, а також Волоконно-оптичні лінії зв'язку які забезпечують швидку передачу даних навіть на великих відстанях без втрат. Для продовження терміну використання, а також для гармонійного поєднання інженерних систем в інтер‘єрі наполегливо рекомендується прокладати кабель в кабеленесучих системах: перфоровані та січасті лотки; металеві та пластмасові кабельні канали, гофровані та гладкі електротехнічні труби. </p></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"><li>Структуровані кабельні системи</li><li>Телебачення (Супутникове та ІРТV)</li><li>Кабеленесучі системи</li></ul>",
                        Direction = context.Directions.ToList()[4]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Структурированная кабельная система (СКС)",
                        Language = context.Languages.Find("ru"),
                        Description = "<section class=\"main-inzener-photo\"> <div class=\"tittle-padd\"><h1 class=\"main-tittle text-center\">СТРУКТУРИРОВАННАЯ КАБЕЛЬНАЯ СИСТЕМА (СКС)</h1></div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-6 col-md-6 col-12 p-0\"><img src=\"/images/directionCable/InzPhoto.png\" class=\"w-100\"></div><div class=\"col-lg-4 col-md-6 col-12 m-auto\"><p class=\"sub-tittle_18 \">Структурированная кабельная система (СКС) представляет собой иерархическую кабельную систему здания или группы зданий, разделенную на структурные подсистемы. Мы прокладываем все типы СКС, а также Волоконно-оптические линии связи обеспечивающие быструю передачу данных даже на больших расстояниях без потерь. Для продления срока использования, а также для гармоничного сочетания инженерных систем в интерьере настоятельно рекомендуется прокладывать кабель в кабеленесущих системах: перфорированные и сичасти лотки; металлические и пластмассовые кабельные каналы, гофрированные и гладкие электротехнические трубы. </p></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"><li>Структурированные кабельные системы</li><li>Телевидение (Спутниковое и ИРТV)</li><li>Кабеленесущие системы</li></ul>",
                        Direction = context.Directions.ToList()[4]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Аудіовізуальний комплекс",
                        Language = context.Languages.Find("uk"),
                        Description = "<section class=\"main-audio-photo top-direct-section\"> <div class=\"tittle-padd\"> <h1 class=\"main-tittle text-center\">АУДІОВІЗУАЛЬНИЙ КОМПЛЕКС</h1> </div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-6 col-md-6 col-12 p-0 direct-photo fromLeftBig\"><img src=\"/images/directionAudio/AudioPhoto.png\" class=\"w-100 \"></div><div class=\"col-lg-4 col-md-6 col-12 m-auto\" ><p class=\"sub-tittle_18 \">В епоху дорогоцінності часу, все актуальніше стає дистанційне проведення конференцій, нарад, переговорів та презентацій, тому приміщення конференц-залу стало обов‘язковою умовою при проектуванні сучасного офісу. Професійно підібране обладнання забезпечує оперативну трансформацію приміщення від конферензалу в тренінгову кімнату чи кімнату переговорів або залу для проведення корпоративного заходу.</p></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"> <li>Конференц-зв’язок</li><li>Візуалізація презентацій</li><li>Відеостіни</li><li>Системи озвучення</li></ul>",
                        Direction = context.Directions.ToList()[5]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Аудиовизуальный комплекс",
                        Language = context.Languages.Find("ru"),
                        Description = "<section class=\"main-audio-photo top-direct-section\"> <div class=\"tittle-padd\"> <h1 class=\"main-tittle text-center\">АУДИОВИЗУАЛЬНЫЙ КОМПЛЕКС</h1> </div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-6 col-md-6 col-12 p-0 direct-photo fromLeftBig\"><img src=\"/images/directionAudio/AudioPhoto.png\" class=\"w-100 \"></div><div class=\"col-lg-4 col-md-6 col-12 m-auto\" ><p class=\"sub-tittle_18 \">В эпоху драгоценности времени, все актуальнее становится дистанционное проведение конференций, совещаний, переговоров и презентаций, поэтому помещение конференц-зала стало обязательным условием при проектировании современного офиса. Профессионально подобранное оборудование обеспечивает оперативную трансформацию помещения от конферензалу в тренинговую комнату или комнату переговоров или зала для проведения корпоративного мероприятия.</p></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"> <li>Конференц-связь</li><li>Визуализация презентаций</li><li>Видеостены</li><li>Системы озвучивания</li></ul>",
                        Direction = context.Directions.ToList()[5]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "GPS-трекінг",
                        Language = context.Languages.Find("uk"),
                        Description = "<section class=\"main-gps-photo\"> <div class=\"tittle-padd\"> <h1 class=\"main-tittle text-center\">GPS ТРЕКІНГ</h1></div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-4 col-md-6 col-12 m-auto\"><p class=\"sub-tittle_18\">Керуйте ефективніше транспортом та людьми завдяки системі GPS-трекінгу. Управління засноване на постійному контролі, що покращить:</p><ul class=\"sub-tittle_18\"><li>якість обслуговування клієнтів,</li><li>збільшить швидкість обслуговування,</li><li>дозволить швидко вирішити виникаючі спірні ситуації,</li><li>підвищить ефективність використання транспорту,</li><li>підвищить ступінь завантаження вантажного транспорту,</li><li>присікання нецільового використання транспорту та &laquo;лівих рейсів&raquo;,</li><li>скоротить пробіг автотранспорту за рахунок максимальної оптимізації маршрутів переміщення.</li></ul><p class=\"sub-tittle_18\">Ми пропонуємо нашим клієнтам окреме програмне забезпечення для відслідковування пересування, історії, статистики свого транспорту. Команда оперативного реагування завжди до ваших послуг.</p><p class=\"sub-tittle_18\">Швидко ліквідуємо неполадки у разі виникнення. Термін окупності такої системи в залежності від кількості транспортних засобів від 1 міс до 6 міс.</p></div><div class=\"col-lg-6 col-md-6 col-12 p-0\"><img src=\"/images/directionGPS/gpsSyst.png\" class=\"w-100\"></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"> <li>Портативні трекери</li><li>Трекінг системи з онлайн контролем</li><li>Відеореєстратори для авто</li></ul>",
                        Direction = context.Directions.ToList()[6]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "GPS-трекинг",
                        Language = context.Languages.Find("ru"),
                        Description = "<section class=\"main-gps-photo\"> <div class=\"tittle-padd\"> <h1 class=\"main-tittle text-center\">GPS ТРЕКИНГ</h1></div></section><section class=\"main-secure-info\"> <div class=\"row\"> <div class=\"col-lg-4 col-md-6 col-12 m-auto\"><p class=\"sub-tittle_18\">Управляйте эффективнее транспортом и людьми благодаря системе GPS-трекинга. Управление основано на постоянном контроле, что улучшит:</p><ul class=\"sub-tittle_18\"><li>качество обслуживания клиентов;</li><li>увеличит скорость обслуживания;</li><li>позволит быстро решить возникающие спорные ситуации;</li><li>повысит эффективность использования транспорта;</li><li>повысит степень загрузки грузового транспорта;</li><li>пресечению нецелевого использования транспорта и &laquo;левых рейсов&raquo;;</li><li>сократит пробег автотранспорта за счет максимальной оптимизации маршрутов перемещения.</li></ul><p class=\"sub-tittle_18\">Мы предлагаем нашим клиентам отдельное программное обеспечение для отслеживания передвижения, истории, статистики своего транспорта. Команда оперативного реагирования всегда к вашим услугам.</p><p class=\"sub-tittle_18\">Быстро ликвидируем неполадки в случае возникновения. Срок окупаемости такой системы в зависимости от количества транспортных средств от 1 мес до 6 мес.</p></div><div class=\"col-lg-6 col-md-6 col-12 p-0\"><img src=\"/images/directionGPS/gpsSyst.png\" class=\"w-100\"></div></div></section>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"> <li>Портативные трекеры</li><li>Трекинг системы с онлайн контролем</li><li>Видеорегистраторы для авто</li></ul>",
                        Direction = context.Directions.ToList()[6]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Розумний дім",
                        Language = context.Languages.Find("uk"),
                        Description = "<section class=\"main-smartHouse-photo\"> <div class=\"tittle-padd\"><h1 class=\"main-tittle text-center\">РОЗУМНИЙ ДІМ</h1></div></section><section class=\"short-smartHome-info\"> <div class=\"container\"> <p class=\"short-info sub-tittle_18 text-center\">Система “розумний дім” допомагає більш результативно використовувати комерційні пересування, автоматизувати певні побутові процеси, урізноманітнити дозвілля. Попри те, що smart-home – дорога технологія, яка вимагає планування із самого початку зведення будинку та якісного устаткування, існують альтернативні рішення. Найпростіший за проектом дім можна доповнити певним прогресивним обладнанням, яке розширить функціональні можливості житлової площі та усучаснить пересування.</p></div></section><section class=\"main-smartHome-info\"> <div class=\"dots-data\"> <img src=\"images/directionSmartHome/SmartSystMainImage.png\" class=\"w-100\"> <div class=\"smart-dot-block jalousie-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white \"> <button class=\"pulse-button\"></button>Розумні жалюзі</span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Розумні жалюзі SolarGaps - це сонячні модулі, які монтуються на віконні жалюзі з внутрішньої або зовнішньої сторони віконного отвору. Ці модулі перетворять сонячну енергію випромінювання в теплову та електричну енергію, роблячи приміщення менше залежним від зовнішніх електричних мереж. Потужність SolarGaps на квадратному метрі віконного отвору досягає 150 Ватт при зовнішньому розміщенні жалюзі і до 100 Ватт при внутрішньому. Перетворювач зберігає отриману енергію і направляє її в мережу, для живлення електроприладів в будинку. А надлишкову енергію можна зберегти і накопичувати, а при бажанні навіть продавати по зеленому тарифу обленерго.</p></div></div><div class=\"smart-dot-block sensor-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\"> <button class=\"pulse-button\"></button>Сенсор руху</span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Сенсори руху виявлять переміщення людини або тварин і, відповідно з їх допомогою «Розумний Дім» керує освітленням, функцією мульти-рум яка визначає хто із членів родини пересувається по помешканню, і включити таке освітлення (температуру/музику і т.д.), яке влаштовує саме цю людину. Також «Розумний Дім» з допомогою сенсорів руху зможе відрізнити пересування мешканців дому від сторонніх осіб та при необхідності включити тривогу.</p></div></div><div class=\"smart-dot-block video-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\"> <button class=\"pulse-button\"></button>Відеонагляд</span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Тепер ви можете контролювати ситуацію цілодобово, навіть перебуваючи поза домом! Система відеоспостереження весь час напоготові. Ви можете мирно спати вночі, а кожна камера фіксуватиме будь-який рух в будинку і на прибудинковій території. Якщо на території хтось з'явиться, камери моментально зафіксують його присутність і повідомлять про це.</p></div></div><div class=\"smart-dot-block watering-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\">Полив<button class=\"pulse-button\"></button></span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Керуйте системою поливу через Інтернет, встановлюйте графіки поливу або довіртесь системі автоматичного контролю поливу яка самостійно визначить потрібну кількість вологи та час поливу, в залежності від погодніх умов. </p></div></div><div class=\"smart-dot-block heating-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\"> Опалення<button class=\"pulse-button\"></button></span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Для оптимізації використання енергії в будинку не можна обійтися без системи «розумний дім». Температура може також регулюватися, у той час, коли нікого немає вдома. Перед поверненням мешканців система опалення почне більшу генерацію тепла і забезпечити комфортну температуру. Вказані процедури вмикання та вимикання системи опалення повинні виконуватися автоматизованими пристроями керування. Вони програмуються по годинах доби і днях тижня. у домашньому використанні: готувати список покупок.</p></div></div><div class=\"smart-dot-block electrical-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\">Електроприлади <button class=\"pulse-button\"></button></span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Наприклад вже тепер, за допомогою технологій інтелектуального будинку, піч може повідомити хазяїв, коли вона потребує чистки. А коли холодильнику стане необхідний техогляд, він “скаже” про це. «Розумний» будильник розбудить вас так, як ви цього захочете. Більш складні систему можуть вести облік продукції у комерційних закладах, облік її використання через зчитування штрих-кодів або RFID-тегів. А у домашньому використанні: готувати список покупок.</p></div></div><div class=\"smart-dot-block light-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\">Освітлення <button class=\"pulse-button\"></button></span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Інтегрувавши систему освітлення приміщень в розумний дім, ви з легкістю зможете керувати всім освітленням в будинку однією DDP панеллю, кнопковою панеллю, або з допомогою смартфону. Крім того розумний дім дозволяє з однієї кнопки, або по будь-якому інакшому сигналу системи запускати цілі сценарії освітлення, вмикаючи, вимикаючи або змінюючи яскравість ряду світильників.</p></div></div></div></section><div class=\"own-container little-dots-data-container pt-5\"> <div class=\"little-dots-data row\"> <div class=\"col-md-6 text-center dot-info-block\"> <div class=\"dot-num text-white sub-tittle_med_25\"><span>1</span></div><div class=\"sub-tittle_med_25 pt-4 pb-3\">Розумні жалюзі</div><div class=\"dot-item-info text-justify\">Розумні жалюзі SolarGaps - це сонячні модулі, які монтуються на віконні жалюзі з внутрішньої або зовнішньої сторони віконного отвору. Ці модулі перетворять сонячну енергію випромінювання в теплову та електричну енергію, роблячи приміщення менше залежним від зовнішніх електричних мереж. Потужність SolarGaps на квадратному метрі віконного отвору досягає 150 Ватт при зовнішньому розміщенні жалюзі і до 100 Ватт при внутрішньому. Перетворювач зберігає отриману енергію і направляє її в мережу, для живлення електроприладів в будинку. А надлишкову енергію можна зберегти і накопичувати, а при бажанні навіть продавати по зеленому тарифу обленерго. </div></div><div class=\"col-md-6 text-center dot-info-block\"> <div class=\"dot-num text-white sub-tittle_med_25\"><span>2</span></div><div class=\"sub-tittle_med_25 pt-4 pb-3\">Сенсор руху</div><div class=\"dot-item-info text-justify\">Сенсори руху виявлять переміщення людини або тварин і, відповідно з їх допомогою «Розумний Дім» керує освітленням, функцією мульти-рум яка визначає хто із членів родини пересувається по помешканню, і включити таке освітлення (температуру/музику і т.д.), яке влаштовує саме цю людину. Також «Розумний Дім» з допомогою сенсорів руху зможе відрізнити пересування мешканців дому від сторонніх осіб та при необхідності включити тривогу. </div></div><div class=\"col-md-6 text-center dot-info-block\"> <div class=\"dot-num text-white sub-tittle_med_25\"><span>3</span></div><div class=\"sub-tittle_med_25 pt-4 pb-3\">Відеонагляд</div><div class=\"dot-item-info text-justify\">Тепер ви можете контролювати ситуацію цілодобово, навіть перебуваючи поза домом! Система відеоспостереження весь час напоготові. Ви можете мирно спати вночі, а кожна камера фіксуватиме будь-який рух в будинку і на прибудинковій території. Якщо на території хтось з'явиться, камери моментально зафіксують його присутність і повідомлять про це. </div></div><div class=\"col-md-6 text-center dot-info-block\"> <div class=\"dot-num text-white sub-tittle_med_25\"><span>4</span></div><div class=\"sub-tittle_med_25 pt-4 pb-3\">Полив</div><div class=\"dot-item-info text-justify\">Керуйте системою поливу через Інтернет, встановлюйте графіки поливу або довіртесь системі автоматичного контролю поливу яка самостійно визначить потрібну кількість вологи та час поливу, в залежності від погодніх умов. </div></div></div></div>",
                        SmallDescription = "<ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"><li>Контроль за електрикою</li><li> Клімат-контроль</li><li>Інтегрована система безпеки</li></ul>",
                        Direction = context.Directions.ToList()[7]
                    },
                    new DirectionTranslatorEntity()
                    {
                        Title = "Умный дом",
                        Language = context.Languages.Find("ru"),
                        Description = "<section class=\"main-smartHouse-photo\"> <div class=\"tittle-padd\"><h1 class=\"main-tittle text-center\">РОЗУМНИЙ ДІМ</h1></div></section><section class=\"short-smartHome-info\"> <div class=\"container\"> <p class=\"short-info sub-tittle_18 text-center\">Система “розумний дім” допомагає більш результативно використовувати комерційні пересування, автоматизувати певні побутові процеси, урізноманітнити дозвілля. Попри те, що smart-home – дорога технологія, яка вимагає планування із самого початку зведення будинку та якісного устаткування, існують альтернативні рішення. Найпростіший за проектом дім можна доповнити певним прогресивним обладнанням, яке розширить функціональні можливості житлової площі та усучаснить пересування.</p></div></section><section class=\"main-smartHome-info\"> <div class=\"dots-data\"> <img src=\"images/directionSmartHome/SmartSystMainImage.png\" class=\"w-100\"> <div class=\"smart-dot-block jalousie-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white \"> <button class=\"pulse-button\"></button>Розумні жалюзі</span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Розумні жалюзі SolarGaps - це сонячні модулі, які монтуються на віконні жалюзі з внутрішньої або зовнішньої сторони віконного отвору. Ці модулі перетворять сонячну енергію випромінювання в теплову та електричну енергію, роблячи приміщення менше залежним від зовнішніх електричних мереж. Потужність SolarGaps на квадратному метрі віконного отвору досягає 150 Ватт при зовнішньому розміщенні жалюзі і до 100 Ватт при внутрішньому. Перетворювач зберігає отриману енергію і направляє її в мережу, для живлення електроприладів в будинку. А надлишкову енергію можна зберегти і накопичувати, а при бажанні навіть продавати по зеленому тарифу обленерго.</p></div></div><div class=\"smart-dot-block sensor-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\"> <button class=\"pulse-button\"></button>Сенсор руху</span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Сенсори руху виявлять переміщення людини або тварин і, відповідно з їх допомогою «Розумний Дім» керує освітленням, функцією мульти-рум яка визначає хто із членів родини пересувається по помешканню, і включити таке освітлення (температуру/музику і т.д.), яке влаштовує саме цю людину. Також «Розумний Дім» з допомогою сенсорів руху зможе відрізнити пересування мешканців дому від сторонніх осіб та при необхідності включити тривогу.</p></div></div><div class=\"smart-dot-block video-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\"> <button class=\"pulse-button\"></button>Відеонагляд</span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Тепер ви можете контролювати ситуацію цілодобово, навіть перебуваючи поза домом! Система відеоспостереження весь час напоготові. Ви можете мирно спати вночі, а кожна камера фіксуватиме будь-який рух в будинку і на прибудинковій території. Якщо на території хтось з'явиться, камери моментально зафіксують його присутність і повідомлять про це.</p></div></div><div class=\"smart-dot-block watering-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\">Полив<button class=\"pulse-button\"></button></span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Керуйте системою поливу через Інтернет, встановлюйте графіки поливу або довіртесь системі автоматичного контролю поливу яка самостійно визначить потрібну кількість вологи та час поливу, в залежності від погодніх умов. </p></div></div><div class=\"smart-dot-block heating-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\"> Опалення<button class=\"pulse-button\"></button></span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Для оптимізації використання енергії в будинку не можна обійтися без системи «розумний дім». Температура може також регулюватися, у той час, коли нікого немає вдома. Перед поверненням мешканців система опалення почне більшу генерацію тепла і забезпечити комфортну температуру. Вказані процедури вмикання та вимикання системи опалення повинні виконуватися автоматизованими пристроями керування. Вони програмуються по годинах доби і днях тижня. у домашньому використанні: готувати список покупок.</p></div></div><div class=\"smart-dot-block electrical-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\">Електроприлади <button class=\"pulse-button\"></button></span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Наприклад вже тепер, за допомогою технологій інтелектуального будинку, піч може повідомити хазяїв, коли вона потребує чистки. А коли холодильнику стане необхідний техогляд, він “скаже” про це. «Розумний» будильник розбудить вас так, як ви цього захочете. Більш складні систему можуть вести облік продукції у комерційних закладах, облік її використання через зчитування штрих-кодів або RFID-тегів. А у домашньому використанні: готувати список покупок.</p></div></div><div class=\"smart-dot-block light-item\"> <span class=\"smart-dot-item sub-tittle_med_25 text-white\">Освітлення <button class=\"pulse-button\"></button></span> <div class=\"smart-info-block\"> <p class=\"text-white sub-tittle_18\">Інтегрувавши систему освітлення приміщень в розумний дім, ви з легкістю зможете керувати всім освітленням в будинку однією DDP панеллю, кнопковою панеллю, або з допомогою смартфону. Крім того розумний дім дозволяє з однієї кнопки, або по будь-якому інакшому сигналу системи запускати цілі сценарії освітлення, вмикаючи, вимикаючи або змінюючи яскравість ряду світильників.</p></div></div></div></section><div class=\"own-container little-dots-data-container pt-5\"> <div class=\"little-dots-data row\"> <div class=\"col-md-6 text-center dot-info-block\"> <div class=\"dot-num text-white sub-tittle_med_25\"><span>1</span></div><div class=\"sub-tittle_med_25 pt-4 pb-3\">Розумні жалюзі</div><div class=\"dot-item-info text-justify\">Розумні жалюзі SolarGaps - це сонячні модулі, які монтуються на віконні жалюзі з внутрішньої або зовнішньої сторони віконного отвору. Ці модулі перетворять сонячну енергію випромінювання в теплову та електричну енергію, роблячи приміщення менше залежним від зовнішніх електричних мереж. Потужність SolarGaps на квадратному метрі віконного отвору досягає 150 Ватт при зовнішньому розміщенні жалюзі і до 100 Ватт при внутрішньому. Перетворювач зберігає отриману енергію і направляє її в мережу, для живлення електроприладів в будинку. А надлишкову енергію можна зберегти і накопичувати, а при бажанні навіть продавати по зеленому тарифу обленерго. </div></div><div class=\"col-md-6 text-center dot-info-block\"> <div class=\"dot-num text-white sub-tittle_med_25\"><span>2</span></div><div class=\"sub-tittle_med_25 pt-4 pb-3\">Сенсор руху</div><div class=\"dot-item-info text-justify\">Сенсори руху виявлять переміщення людини або тварин і, відповідно з їх допомогою «Розумний Дім» керує освітленням, функцією мульти-рум яка визначає хто із членів родини пересувається по помешканню, і включити таке освітлення (температуру/музику і т.д.), яке влаштовує саме цю людину. Також «Розумний Дім» з допомогою сенсорів руху зможе відрізнити пересування мешканців дому від сторонніх осіб та при необхідності включити тривогу. </div></div><div class=\"col-md-6 text-center dot-info-block\"> <div class=\"dot-num text-white sub-tittle_med_25\"><span>3</span></div><div class=\"sub-tittle_med_25 pt-4 pb-3\">Відеонагляд</div><div class=\"dot-item-info text-justify\">Тепер ви можете контролювати ситуацію цілодобово, навіть перебуваючи поза домом! Система відеоспостереження весь час напоготові. Ви можете мирно спати вночі, а кожна камера фіксуватиме будь-який рух в будинку і на прибудинковій території. Якщо на території хтось з'явиться, камери моментально зафіксують його присутність і повідомлять про це. </div></div><div class=\"col-md-6 text-center dot-info-block\"> <div class=\"dot-num text-white sub-tittle_med_25\"><span>4</span></div><div class=\"sub-tittle_med_25 pt-4 pb-3\">Полив</div><div class=\"dot-item-info text-justify\">Керуйте системою поливу через Інтернет, встановлюйте графіки поливу або довіртесь системі автоматичного контролю поливу яка самостійно визначить потрібну кількість вологи та час поливу, в залежності від погодніх умов. </div></div></div></div>",
                        SmallDescription = " <ul class=\"step-sub-tittle color-dark sub-tittle_ulight_18\"><li>Контроль за електрикою</li><li> Клімат-контроль</li><li>Інтегрована система безпеки</li></ul>",
                        Direction = context.Directions.ToList()[7]
                    }
                );
                context.SaveChanges();
            }

            if (!context.Products.Any())
            {
                context.Products.AddRange(
                        new ProductEntity()
                        {
                            MainImage = "/images/catalogVideo/Video1.png",
                            Direction = context.Directions.ToList()[0],
                            IsDeleted = false
                        },
                        new ProductEntity()
                        {
                            MainImage = "/images/catalogVideo/Video2.png",
                            Direction = context.Directions.ToList()[0],
                            IsDeleted = false
                        },
                        new ProductEntity()
                        {
                            MainImage = "/images/catalogVideo/Video3.png",
                            Direction = context.Directions.ToList()[0],
                            IsDeleted = false
                        },
                        new ProductEntity()
                        {
                            MainImage = "/images/catalogVideo/Video4.png",
                            Direction = context.Directions.ToList()[0],
                            IsDeleted = false
                        },
                        new ProductEntity()
                        {
                            MainImage = "/images/catalogVideo/Video5.png",
                            Direction = context.Directions.ToList()[0],
                            IsDeleted = false
                        }
                    );
                context.SaveChanges();
            }
            if (!context.ProductTranslators.Any())
            {
                context.ProductTranslators.AddRange(
                        new ProductTranslatorEntity()
                        {
                            Title = "Відеонагляд для квартир",
                            Url = urlService.GetEncodedUrlString("Відеонагляд для квартир"),
                            Language = context.Languages.Find("uk"),
                            Product = context.Products.ToList()[0]
                        },
                        new ProductTranslatorEntity()
                        {
                            Title = "Видеонаблюдение для квартир",
                            Url = urlService.GetEncodedUrlString("Видеонаблюдение для квартир"),
                            Language = context.Languages.Find("ru"),
                            Product = context.Products.ToList()[0]
                        },
                        new ProductTranslatorEntity()
                        {
                            Title = "Відеонагляд для квартир",
                            Url = urlService.GetEncodedUrlString("Відеонагляд для квартир"),

                            Language = context.Languages.Find("uk"),
                            Product = context.Products.ToList()[1]
                        },
                        new ProductTranslatorEntity()
                        {
                            Title = "Видеонаблюдение для квартир",
                            Url = urlService.GetEncodedUrlString("Видеонаблюдение для квартир"),
                            Language = context.Languages.Find("ru"),
                            Product = context.Products.ToList()[1]
                        },
                        new ProductTranslatorEntity()
                        {
                            Title = "Відеонагляд для квартир",
                            Url = urlService.GetEncodedUrlString("Відеонагляд для квартир"),

                            Language = context.Languages.Find("uk"),
                            Product = context.Products.ToList()[2]
                        },
                        new ProductTranslatorEntity()
                        {
                            Title = "Видеонаблюдение для квартир",
                            Url = urlService.GetEncodedUrlString("Видеонаблюдение для квартир"),
                            Language = context.Languages.Find("ru"),
                            Product = context.Products.ToList()[2]
                        },
                        new ProductTranslatorEntity()
                        {
                            Title = "Відеонагляд для квартир",
                            Url = urlService.GetEncodedUrlString("Відеонагляд для квартир"),

                            Language = context.Languages.Find("uk"),
                            Product = context.Products.ToList()[3]
                        },
                        new ProductTranslatorEntity()
                        {
                            Title = "Видеонаблюдение для квартир",
                            Url = urlService.GetEncodedUrlString("Видеонаблюдение для квартир"),
                            Language = context.Languages.Find("ru"),
                            Product = context.Products.ToList()[3]
                        },
                        new ProductTranslatorEntity()
                        {
                            Title = "Відеонагляд для квартир",
                            Url = urlService.GetEncodedUrlString("Відеонагляд для квартир"),

                            Language = context.Languages.Find("uk"),
                            Product = context.Products.ToList()[4]
                        },
                        new ProductTranslatorEntity()
                        {
                            Title = "Видеонаблюдение для квартир",
                            Url = urlService.GetEncodedUrlString("Видеонаблюдение для квартир"),
                            Language = context.Languages.Find("ru"),
                            Product = context.Products.ToList()[4]
                        }
                    );
                context.SaveChanges();
            }
            if (!context.ProductDescriptions.Any())
            {
                context.ProductDescriptions.AddRange(
                        new ProductDescriptionEntity()
                        {
                            Product = context.Products.ToList()[0],
                            Price = 300
                        },
                        new ProductDescriptionEntity()
                        {
                            Product = context.Products.ToList()[0],
                            Price = 500
                        },
                        new ProductDescriptionEntity()
                        {
                            Product = context.Products.ToList()[0],
                            Price = 1000
                        }
                    );
                context.SaveChanges();
            }


            if (!context.ProductDescriptionTranslators.Any())
            {
                context.ProductDescriptionTranslators.AddRange(
                        new ProductDescriptionTranslatorEntity()
                        {
                            Title = "1 кам.",
                            Description = "Система включає: Відеореєстратор з HD диском Камери 1 шт. Блок живлення Монтаж ліній і всіх пристроїв Налаштування системи",
                            Code = "uk",
                            ProductDescription = context.ProductDescriptions.ToList()[0]
                        },
                        new ProductDescriptionTranslatorEntity()
                        {
                            Title = "1 кам.",
                            Description = "Система включает: \n\nВидеореестратор с HD диском \nКамеры 1 шт. \nБлок питания \nМонтаж линий и всех устройств \nНалаштування системы",
                            Code = "ru",
                            ProductDescription = context.ProductDescriptions.ToList()[0]
                        },
                        new ProductDescriptionTranslatorEntity()
                        {
                            Title = "2-4 кам.",
                            Description = "Система включає: Відеореєстратор з HD диском Камери 2 - 4 шт. Блок живлення Монтаж ліній і всіх пристроїв Налаштування системи",
                            Code = "uk",
                            ProductDescription = context.ProductDescriptions.ToList()[1]
                        },
                        new ProductDescriptionTranslatorEntity()
                        {
                            Title = "2-4 кам.",
                            Description = "Система включает: \n\nВидеореестратор с HD диском \nКамеры 2 - 4 шт. \nБлок питания \nМонтаж линий и всех устройств \nНалаштування системы",
                            Code = "ru",
                            ProductDescription = context.ProductDescriptions.ToList()[1]
                        },
                        new ProductDescriptionTranslatorEntity()
                        {
                            Title = "4-8 кам.",
                            Description = "Система включає: Відеореєстратор з HD диском Камери 4 - 8 шт. Блок живлення Монтаж ліній і всіх пристроїв Налаштування системи",
                            Code = "uk",
                            ProductDescription = context.ProductDescriptions.ToList()[2]
                        },
                        new ProductDescriptionTranslatorEntity()
                        {
                            Title = "4-8 кам.",
                            Description = "Система включает: \n\nВидеореестратор с HD диском \nКамеры 4 - 8 шт. \nБлок питания \nМонтаж линий и всех устройств \nНалаштування системы",
                            Code = "ru",
                            ProductDescription = context.ProductDescriptions.ToList()[2]
                        }

                    );
                context.SaveChanges();
            }
            if (!context.Images.Any())
            {
                context.Images.AddRange(
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[0] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[0] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[0] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[0] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[0] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[0] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[1] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[1] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[1] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[1] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[2] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[2] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[2] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[2] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[2] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[3] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[3] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[3] },
                        new ImageEntity() { Path = "/images/ProdItemPhoto.png", Product = context.Products.ToList()[4] }

                    );
                context.SaveChanges();
            }

            if (!context.Reviews.Any())
            {
                context.Reviews.AddRange(
                    new ReviewEntity()
                    {
                        Content = "В 21м веке и в эру технологий, когда люди не тратят время на посещение отделений, при такой необходимости банк не в состоянии обеспечить быстрое и качественное обслуживание клиентов. Руководитель сообщила, что обработка двух клиентов ВСЕМИ менеджерами (около 6 работающих и 6 гуляющих) — очень высокий показатель скорости работы. Как результат — час потерянного времени! Как клиент — крайне возмущена!",
                        UserName = "Анастасія",
                        Approved = true,
                        CreateDate = DateTime.Now,
                        ApproveDate = DateTime.Now,
                        Email = "anasteisha@gmail.com"
                    },
                    new ReviewEntity()
                    {
                        Content = "Сегодня произошел преотвратительный инциндент при съеме денег.После завершение процедуры оформление снятие терминал сказал« возьмите деньги», а денег не дал. А после проверки состояния счета, еще и сожрал карточку. Когда обратились в отделение банка с письменным заявлением, те сказали что минимум через месяц и когда инкосаторы снимут деньги, пересчитают. Это что такое: во первых, карточка зарплатная. во вторых перебоя со светом не было, то есть он просто не выдал деньги. в третьих-деньги пересчитают если выявят лишние деньги, то отдадут — это с нашими людьми, хотелось бы верить? А что делать месяц? Это зарплата, причем небольшая. Полное отвратительное поведение. Разговор про «Эксимбанк»",
                        UserName = "Сергій",
                        Approved = true,
                        CreateDate = DateTime.Now,
                        ApproveDate = DateTime.Now,
                        Email = "serg@gmail.com"
                    },
                    new ReviewEntity()
                    {
                        Content = "С банкоматами просто беда, их и так не много было, так теперь еще и некоторые убрали. Зарплатное рабство в действии, компания заключила договор с этим банком на выплату зарплаты. Продукт — отстой полный! СМС информирование, банкинг — платные, на остатки денег начисление 0%. Снять деньги в банкоматах проблематично, лимит 2 000 в день. Катайся за СВОИМИ деньгами черт знает куда. Сразу все суммы снимаю под остаток, кладу в другой банк. Не банк а сплошной геморрой.",
                        UserName = "Павел",
                        Approved = true,
                        CreateDate = DateTime.Now,
                        ApproveDate = DateTime.Now,
                        Email = "pavel@gmail.com"
                    },
                    new ReviewEntity()
                    {
                        Content = "Скоро заканчивается срок депозита. Так как в этом банке от пролонгации не отмажешься, пришлось заключить договор с пролонгацией. А вот чтобы забрать деньги, нужно приехать за 3 дня до окончания срока и написать отказ от пролонгации. И вот я, отпросившись с работы, приезжаю в отделение, куда отнесла деньги (г. Харьков, №2), а там вместо банка — магазинчик. Банк куда-то делся и даже не потрудился сообщить мне куда. Теперь мне придется еще раз отпрашиваться (терять зарплату), и искать, куда делся банк. Я может и плюнула бы, пусть себе продлевается, но проценты — 1%. Всем советую держаться подальше от этого банка с его сюрпризами.",
                        UserName = "Роман",
                        Approved = true,
                        CreateDate = DateTime.Now,
                        ApproveDate = DateTime.Now,
                        Email = "roman@gmail.com"
                    }
                );
                context.SaveChanges();
            }


        }
    }
}
