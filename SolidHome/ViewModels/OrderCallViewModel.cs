﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolidHome.ViewModels
{
    public class OrderCallViewModel
    {
        public string Name { set; get; }
        public string PhoneNumber { set; get; }
    }
}
