﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolidHome.ViewModels
{
    public class ReviewCreateViewModel
    {
        public string UserName { set; get; }
        public string Email { set; get; }
        public string Content { set; get; }
    }
}
