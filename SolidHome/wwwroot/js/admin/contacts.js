﻿$(document).on("submit",
    "#contacts-update",
    function (e) {
        e.preventDefault();
        var url = $(this).attr("action");
        var formData = new FormData(document.getElementById("contacts-update"));
        SendForm.call(this, url, formData);
    });

let SendForm = function (url, data) {
    console.log(url);
    console.log(data);
    $.ajax({
        type: "POST",
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        success: function () {
            $.toaster({
                priority: "success",
                title: "Операція успішна",
                message: `Зміни збережено!`,
                settings: {
                    'timeout': 4000
                }
            });
        }
    });
}