﻿$(function ($) {
    var path = window.location.pathname.toLowerCase();
    $('.catalog-items').each(function () {

        if ((path === "/admin" || path.includes("/admin/news")) && $(this).attr("href") === "/admin") {
            $(this).parent().addClass("active-menu-item");
        }
        else if (path.includes($(this).attr("href")) && $(this).attr("href") !== "/admin") {
            $(this).parent().addClass("active-menu-item");
        } else {
            $(this).parent().removeClass("active-menu-item");
        }

    });
});