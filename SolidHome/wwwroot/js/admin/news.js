﻿$(document).on('click', ".publish-news,.unpublish-news", function (e) {
        e.preventDefault();
        var element = $(this);
        changeActive.call(this, element);
        $.ajax({
            url: $(this).attr("href"),
            method: "POST",
            success: function (data) {
                changeActive.call(this, element);
            },
            error: function (data) {
                if (element.hasClass("edit-btn")) {
                    changeActive.call(this, element.parent().next().children().children());
                } else if (elem.hasClass("block-btn")) {
                    changeActive.call(this, element.parent().prev().children().children());
                }
                
            }
        });

    });


let changeActive = function(elem) {
    if (elem.hasClass("edit-btn")) {
        elem.addClass("active-btn-edit a-disabled");
        elem.parent().next().children().removeClass("active-btn-block");
        elem.parent().next().children().removeClass("a-disabled");
    } else if (elem.hasClass("block-btn")) {
        elem.addClass("active-btn-block a-disabled");
        elem.parent().prev().children().removeClass("active-btn-edit");
        elem.parent().prev().children().removeClass("a-disabled");
    }
}

$(document).on('click', ".prevent", function (e) {
    e.preventDefault();
});

$(document).on('change', `.image-input`, function (e) {
    if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.image').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);
    }
});

$(document).on("click", ".edit-publish", function (e) {
    console.log("dfgdfg");
    $("input[name='IsPublished']").attr("checked", "checked");
    return true;
});