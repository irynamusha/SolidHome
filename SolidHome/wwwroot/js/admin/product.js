﻿$(document).on('click', "a.file-remove", function (e) {
    e.preventDefault();
    var $this = $(this);
    $.ajax({
        url: $(this).attr("href"),
        method: "POST",
        success: function (data) {
            $this.parent().remove();
            $.toaster({
                priority: "danger",
                title: "Видалення успішне",
                message: `Елемент успішно видалено!`,
                settings: {
                    'timeout': 4000
                }
            });
        }
    });
});

$(document).on('click', "a.edit-descr, a.add-descr", function (e) {
    e.preventDefault();
    var $this = $(this);
    $.ajax({
        url: $(this).attr("href"),
        method: "GET",
        success: function (data) {
            $(".modal-footer").empty();
            $(".modal-footer").append(data);
            $(".product-id").val($("#ProductId").val());
            tinymce.remove();
            tinymce.init({
                mode: "specific_textareas",
                selector: '.tinymce',
                height: 300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                oninit: "setPlainText",
                toolbar: "paste | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    { title: 'Bold text', inline: 'b' },
                    { title: 'Red text', inline: 'span', styles: { color: '#ff0000' } },
                    { title: 'Red header', block: 'h1', styles: { color: '#ff0000' } },
                    { title: 'Example 1', inline: 'span', classes: 'example1' },
                    { title: 'Example 2', inline: 'span', classes: 'example2' },
                    { title: 'Table styles' },
                    { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
                ]
            });
        }
    });
        
        //if (tinymce.editors.length > 0) {
        //    tinymce.execCommand('mceFocus', true, 'Article');
        //    tinymce.execCommand('mceRemoveEditor', true, 'Article');
        //    tinymce.execCommand('mceAddEditor', true, 'Article');
        //}
});

$(document).on('submit', ".descr-update", function (e) {
    e.preventDefault();
    var $this = $(this);
    var formData = new FormData(document.getElementsByClassName("descr-update")[0]);
    $.ajax({
        url: $this.attr("action"),
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        method: "POST",
        success: function (data) {
            $('.modal').modal('hide');
            $(".descr-table").empty();
            $(".descr-table").append(data);
            $.toaster({
                priority: "success",
                title: "Операція успішна",
                message: `Елемент успішно збережено!`,
                settings: {
                    'timeout': 4000
                }
            });
        }
    });
});

$(document).on('submit', ".product-form", function (e) {
    //e.preventDefault();
    var $this = $(this);
    var formData = new FormData(document.getElementsByClassName("product-form")[0]);
    var images = $(".MultiFile-applied");
    var alts = $(".img-alt");
    

    for (var i = 0; i < images.length-1; i++) {
        formData.append(`PreviewImages[${i}]`, images[i].files[0]);
    }
    return true;
});


