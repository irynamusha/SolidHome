﻿$(document).on('submit', ".work-form", function (e) {
    //e.preventDefault();
    var $this = $(this);
    var formData = new FormData(document.getElementsByClassName('work-form')[0]);
    var images = $(".MultiFile-applied");

    for (var i = 0; i < images.length - 1; i++) {
        formData.append(`PreviewImages[${i}]`, images[i].files[0]);
    }
    return true;
});
