﻿$(document).on('click', ".remove-item", function (e) {
    e.preventDefault();
    console.log($(this).attr("href"));
    var element = $(this);
    $.ajax({
        url: $(this).attr("href"),
        method: "POST",
        success: function (data) {
            $(".main-content").empty();
            $(".main-content").append(data);
            $.toaster({
                priority: "danger",
                title: "Видалення успішне",
                message: `Елемент успішно видалено!`,
                settings: {
                    'timeout': 4000
                }
            });
        }
    });
});

$(document).on('click', ".remove-item-description", function (e) {
    e.preventDefault();
    console.log($(this).attr("href"));
    var element = $(this);
    $.ajax({
        url: $(this).attr("href"),
        method: "POST",
        success: function (data) {
            $(".descr-table").empty();
            $(".descr-table").append(data);
            $.toaster({
                priority: "danger",
                title: "Видалення успішне",
                message: `Елемент успішно видалено!`,
                settings: {
                    'timeout': 4000
                }
            });
        }
    });
});