// для відео
var myVideo = document.getElementById("video");

function play() {
	myVideo.play();
	$(".btn-play").fadeOut("slow");
}

function pause() {
	myVideo.pause();
}

$('#video').click(function () {
	$(".video-section .btn-play").fadeIn("slow");
});

//для лоадера на головній сторінці

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return true;
		}
	}
	return false;
}

$(document).ready(function () {
	if ($('*').is('.introduction')) {
		$(window).scroll(function () {
			var offsetTopbtn = $(".introduction").offset().top - 200;
			var scroll = $(window).scrollTop();
			if (scroll > offsetTopbtn) {
				$('.toTopBtn').css('opacity', '1');

			} else {
				$('.toTopBtn').css('opacity', '0');
			}
		});

		$('.toTopBtn').click(function () {
			$('html, body').animate({
				scrollTop: 0
			}, 1000);

		});
	}
 
	if(getCookie("animation")){
		main_page_animation();
	}
	else{
		$("#loader").addClass("loader-wrapper");
		setTimeout(function () {
			$('body').addClass('loaded');
		}, 1000);
		setTimeout(function () {
			$('.loader-wrapper').addClass('loaderZ');
		}, 250);
		setTimeout(main_page_animation,1590);
	    setCookie("animation", "true", 1);
	}




	//верхня анімація для головної сторінки
	function main_page_animation() {
		$(".main-page-animation").each(function (index) {
			(function (that, i) {
				var t = setTimeout(function () {
					if (index == 0) {
						$(that).addClass('LeftToRight');
					} else if (index == 1) {
						$(".menu-animate").each(function (index) {
							(function (that, k) {
								var h = setTimeout(function () {
									$(that).addClass('TopToBottom');
								}, 100 * k);
							})(this, index);
						});
					} else if (index == 7) {
						$(that).addClass('RightToLeft');
					}
					else if (index == 8) {
						$(that).addClass('LeftToRight');
					}
					else if (index == 9) {
						$(that).addClass('RightToLeft');
					}
					else if (index == 10) {
						$(that).addClass('BottomToTop');
					}
					else if (index == 11) {
						$(that).addClass('BottomToTop');
					}
					else if (index == 12) {
						$(that).addClass('RightToLeft');
					}
					else if (index == 13) {
						$(that).addClass('TopBottomTopBottom');
					}
				}, 65 * i);
			})(this, index);
		});
	}


//direction animation

	function direction_item_animation() {
		$(".direction-item-animation").each(function (index) {
			(function (that, i) {
				setTimeout(function () {
					$(that).addClass('OpasityAnim');
					$('.circle-animate').each(function (index) {
						(function (that, k) {
							setTimeout(function () {
								$(that).addClass('circle-animate-visible');
							}, 110 * k);
						})(this, index);
					});
				}, 500 * i);
			})(this, index);
		});
	}

	setTimeout(direction_item_animation, 50);


// скрол до інфи
	if ($('*').is('.intro-description')) {
		var offsetTopInfo = $(".intro-description").offset().top - 190;
	}
	$(function () {
		$('.IconArrow').click(function () {
			$('html, body').animate({
				scrollTop: offsetTopInfo
			}, 1000);

		});
	});

// скрол до відео
	//if ($('*').is('.video-section')) {
	//	var offsetTopVideo = $(".video-section").offset().top - 100;
	//}
	//$(function () {
	//	$('.btn-dark-light').click(function () {
	//		$('html, body').animate({
	//			scrollTop: offsetTopVideo
	//		}, 1600);

	//	});
	//});
    $('.slider.testimonials').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        infinite: true,
        arrows: true,
        prevArrow: "<div class='paralax-arrow-left'><i class='icons icon-more-categ'></i></div>",
        nextArrow: "<div class='paralax-arrow-right'><i class='icons icon-more-categ'></i></div>",
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 650,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });
});

$(".navbar-toggler").click(function () {
	var clicks = $(this).data('clicks');
	if (clicks) {
		$('.navbar-collapse').css('left', '-100%');
		$('.navbar-toggler span:first-child').css('transform','rotateZ(0deg) translate(0, 0)').css('transition','.5s linear');
		$('.navbar-toggler span:last-child').css('transform','rotateZ(0deg) translate(0, 0)').css('transition','.5s linear');
		$('.navbar-toggler span:nth-child(2)').css('opacity','1').css('transition','.5s linear');

	} else {
		$('.navbar-collapse').css('width', '80vw').css('left', '0');
		$('.navbar-toggler span:first-child').css('transform','rotateZ(45deg) translate(5px, 8px)').css('transition','.5s linear');
		$('.navbar-toggler span:last-child').css('transform','rotateZ(-45deg) translate(5px,-8px)').css('transition','.5s linear');
		$('.navbar-toggler span:nth-child(2)').css('opacity','0').css('transition','.5s linear');
	}
	$(this).data("clicks", !clicks);

});


$(window).on('scroll resize', function () {
	var scroll = $(window).scrollTop();
//для кроків на головній сторінці
	if ($('*').is('.steps-row')) {
		var offsetTopSteps = $(".steps").offset().top - 800;
		if (scroll >= offsetTopSteps) {
			function step_animation() {
				$(".step-item").each(function (index) {
					(function (that, i) {
						var t = setTimeout(function () {
							$(that).addClass('slideInUp LeftToRightDots');
						}, 600 * i);
					})(this, index);
				});
			}

			step_animation();
		} else {
			$('.step-item').removeClass('slideInUp LeftToRightDots');
		}
	}
//для збільшення чисел
	if ($('*').is('.achievements')) {
		var offsetTopAchievements = $(".achievements").offset().top - 800;
		if (scroll >= offsetTopAchievements) {
			$('.achievement-num').each(function () {
				$(this).prop('Counter', 0).animate({
					Counter: $(this).data('number')
				}, {
					duration: 2000,
					easing: 'swing',
					step: function (now) {
						$(this).text(Math.ceil(now).toLocaleString('en'));
					}
				});

			});
		}
	}

	if ($('*').is('.our-prods')) {
		var offsetTopProds = $(".our-prods").offset().top - 1000;
		if (scroll >= offsetTopProds) {
			function prods_animation() {
				$(".prods-row .item").each(function (index) {
					(function (that, i) {
						var t = setTimeout(function () {
							$(that).addClass('TopToBottom');
						}, 350 * i);
					})(this, index);
				});
			}

			prods_animation();
		}

	}

	if ($('*').is('.introduction')) {
		var offsetTopProds = $(".introduction").offset().top - 500;
		if (scroll >= offsetTopProds) {
			$('.introd-tittle').addClass('TopToBottom');
			$('.intro-description').addClass('RightToLeft');
			$('.introduction img').addClass('LeftToRight');
		}
	
	}


	//для футера
	var offsetTopSteps = $(".top-footer").offset().top - 900;
	if (scroll >= offsetTopSteps) {
		function footer_animation() {
			$(".footer-title").addClass('BottomToTop');
			$(".footer-quick ul li").each(function (index) {
				(function (that, i) {
					setTimeout(function () {
						$(that).addClass('BottomToTop');
					}, 40 * i);
				})(this, index);
			});
			$(".down-footer .down-footer-item").each(function (index) {
				(function (that, i) {
					setTimeout(function () {
							$(that).addClass('BottomToTop');
					}, 150);
				})(this, 60);
			});
		}

		footer_animation();
	}
	else {
		 $('.footer-quick *').removeClass('BottomToTop');
		 $('.down-footer *').removeClass('BottomToTop');
    }
    $(".more-direct-about").click(function () {
        $(".direct-about-info").css("-webkit-line-clamp", "100");
        $(".more-direct-about").css("display", "none");
    }); 
});
