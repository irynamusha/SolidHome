﻿$('.autoplay').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false,
    infinite: false,
    prevArrow: "<div class='paralax-arrow-left'><i class='icons icon-more-categ'></i></div>",
    nextArrow: "<div class='paralax-arrow-right'><i class='icons icon-more-categ'></i></div>",
    responsive: [
              {
            breakpoint: 1200,
            settings: {
                slidesToShow: 2,

            }
        },
        {
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
            }
        }
    ]
});
