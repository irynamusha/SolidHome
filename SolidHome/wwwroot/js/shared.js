﻿$(function ($) {
    var path = window.location.toString().toLowerCase();

    $('.navbar-nav a').each(function () {

        var hosthref = window.location.protocol + "//" + window.location.host + "/";
        var pathname = window.location.pathname;
        if ((window.location.pathname === "/directions" || window.location.pathname === "/ru/directions") && $(this).attr("href") === path) {
            $(this).parent().addClass('active-dark');
        }

        else if (path.startsWith($(this).attr("href")) && ($(this).attr("href") !== hosthref && $(this).attr("href") !== hosthref + "ru")) {
            $(this).parent().addClass('active-' + $(this).data("active"));
        }
        else if ((pathname === "/" || pathname==="/ru") && $(this).attr("href") === path) {
            $(this).parent().addClass('active-' + $(this).data("active"));
        }

    });

    $('.catalog-items').each(function () {
        console.log($(this).attr("href"));
        console.log(path);
        if (path.includes($(this).attr("href"))) {
            $(this).parent().removeClass("sub-tittle_18");
            $(this).parent().addClass("sub-tittle_med_18");
            $(this).parent().addClass("active-menu-item");
        } else {
            $(this).parent().addClass("sub-tittle_18");
            $(this).parent().removeClass("sub-tittle_med_18");
            $(this).parent().removeClass("active-menu-item");
        }

    });


    if (window.location.pathname === "/directions" || window.location.pathname === "/ru/directions") {
        $(".top-header").addClass("header-dark");
        $(".navbar-brand").children().attr("src", "/images/Logo/LogoDarkGreen.svg");
    }
});
$('.smart-dot-item').hover(function () {
    $(this).parents('.dots-data').addClass('dark-back');
}, function () {
    $(this).parents('.dots-data').removeClass('dark-back');
});