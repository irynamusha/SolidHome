﻿function rotate(li, d) {
    $({ d: 0 }).animate({ d: d }, {
        step: function (now) {
            $(li)
                .css({ transform: 'rotate(' + now + 'deg)' })
                .find('a')
                .css({ transform: 'rotate(' + (-now) + 'deg)' });
        }, duration: 0
    });
}

function toggleOptions(s) {
    $(s).toggleClass('open');
    var li = $(s).find('li');
    var deg = $(s).hasClass('half') ? 180 / (li.length - 1) : 360 / li.length;
    for (var i = 0; i < li.length; i++) {
        var d = $(s).hasClass('half') ? (i * deg) - 90 : i * deg;
        $(s).hasClass('open') ? rotate(li[i], d) : rotate(li[i], d);
    }
}

setTimeout(function () {
    toggleOptions('.selector');
}, 0);
$('.more-about').click(function () {
    $('.intro-description').css('-webkit-line-clamp', '100');
    $('.more-about').css('display', 'none');
});

function initMap() {
    var uluru = { lat: 49.82699866, lng: 23.99028891 };
    var uluru1 = { lat: 50.345967, lng: 30.48746 };
    var uluruCenter = { lat: 49.87774513, lng: 27.30621437 };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        center: uluruCenter,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#444444"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },

            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#dbdbdb"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ]

    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "/images/marker.png"
    });
    var marker1 = new google.maps.Marker({
        position: uluru1,
        map: map,
        icon: "/images/marker.png"
    });
}
$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    arrows: false,
    focusOnSelect: true,
    verticalSwiping: true,
    vertical: true,
    infinite: false,
});