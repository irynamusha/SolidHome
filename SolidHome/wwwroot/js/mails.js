﻿$(document).on("submit", ".contact-form", function (e) {
    e.preventDefault();
    console.log(document.getElementsByClassName("contact-form")[0]);
    var formData = new FormData(document.getElementsByClassName("contact-form")[0]);
    var path = $(this).attr("action");

    $.ajax({
        url: path,
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {

            $(".contact-us-form").empty();
            $(".contact-us-form").append(data);
            //$(".top-header").addClass("header-dark");
        }
    });
});