﻿$(document).on("submit", ".order-form", function (e) {

    e.preventDefault();
    var $this = $(this);
    var modal = $this.closest(".modal-content");
    var oldContent = $(".modal-content").html();
    var formData = new FormData(document.getElementById($(this).attr("id")));
    formData.append("Url", window.location.href);
    formData.append("Clarification", $(".order-active").html());
    var path = $(this).attr("action");
    //$('.modal').modal('hide');
    
    modal.empty();
    var content =
        "<div class=\"text-center\"><h1 class=\"sub-tittle unvis-tittle m-0\"></h1><div class=\"loadCircle\"><div class=\"littleLoader\"></div></div><a  class=\"btn btn-unvisible bottom-big-m\"></a></div>";
   modal.append(content);
    $.ajax({
        url: path,
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,     
        processData: false,
        success: function (data) {
            modal.empty();
            modal.append(data);
            console.log(modal.find("#call-captcha").get(0));
            var widgetId = grecaptcha.render(modal.find("#call-captcha").get(0), {
                'sitekey': modal.find("#call-captcha").data("sitekey")
            });
            //$(".top-header").addClass("header-dark");
        },
        error: function (data) {
            console.log($(oldContent).find("input[name='Name']").attr("name"));
            $(oldContent).find("input[name='Name']").val("dfg");
            $this.closest(".modal-content").empty();
            $this.closest(".modal-content").append(oldContent);
           
        }
    });
});
$(document).on("click", ".order-item", function(e) {
    $(".order-active").html($(this).html());
});