﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SolidHome.Repository;
using SolidHome.Utils.Service;

namespace SolidHome
{
    public class Program
    {

        //public static void Main(string[] args)
        //{

        //    BuildWebHost(args).Run();
        //}
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var dbContext = services.GetService<DataContext>();
                var urlService = services.GetService<UrlService>();
                SampleData.Initialize(dbContext, urlService);
            }
            host.Run();

        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://localhost:6969")
                .UseKestrel()
                .UseStartup<Startup>()
                .Build();
    }
}
