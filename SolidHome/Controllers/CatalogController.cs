﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using SolidHome.Localization;
using SolidHome.Models;
using SolidHome.Service.Interfaces;
using SolidHome.Utils.Service;

namespace SolidHome.Controllers
{
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class CatalogController : Controller
    {
        private readonly IProductService _productService;
        private readonly IDirectionService _directionService;
        private readonly ICatalogService _catalogService;
        private readonly IOrderService _orderService;
        private IConfiguration _configuration;


        public CatalogController(IProductService productService,
            IDirectionService directionService, 
            ICatalogService catalogService, 
            IHttpContextAccessor httpContextAccessor, 
            IOrderService orderService, IConfiguration configuration)
        {
            CultureInfo.CurrentCulture = new CultureInfo(httpContextAccessor.HttpContext.GetRouteValue("lang") as string ?? "uk");
            _productService = productService;
            _directionService = directionService;
            _catalogService = catalogService;
            _orderService = orderService;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult CatalogItem(string url)
        {
            
            var a = _catalogService.GetCatalogItem(url, CultureInfo.CurrentCulture.Name);
            if (a.Products.Count == 1)
            {
                ViewBag.Single = true;
                var product = _productService.GetProductByUrl(a.Products[0].Url, CultureInfo.CurrentCulture.Name);
                return View("~/Views/Catalog/Product.cshtml", product);
            }
            return View(a);
        }

        public IActionResult CatalogItemAmp(string url)
        {
            var a = _catalogService.GetCatalogItem(url, CultureInfo.CurrentCulture.Name);
            if (a.Products.Count == 1)
            {
                ViewBag.Single = true;
                var product = _productService.GetProductByUrl(a.Products[0].Url, CultureInfo.CurrentCulture.Name);
                return View("~/Views/Catalog/ProductAmp.cshtml", product);
            }
            return StatusCode(404);
        }

        public IActionResult Product(string url, string productUrl)
        {      
            var product = _productService.GetProductByUrl(productUrl, CultureInfo.CurrentCulture.Name);
            if (product == null || url != product.Direction.Url || product.ProductTranslators.FirstOrDefault(c => c.Code == CultureInfo.CurrentCulture.Name)?.Url !=
                productUrl)
                return StatusCode(404);

            var products = _productService.GetProductsByDirectionUrl(url, CultureInfo.CurrentCulture.Name).ToList();
            if(products.Count < 2)
            {
                return StatusCode(404);
            }
            int currentIndex = products.FindIndex(c => c.Url == productUrl);
            ViewBag.Next = products[(currentIndex + 1) % (products.Count)].Url;
            ViewBag.Previous = products[(currentIndex + products.Count - 1) % (products.Count)].Url;
            ViewBag.SiteKey = _configuration.GetSection("GoogleReCaptcha:key").Value;
            return View(product);
        }

        
        public IActionResult ProductAmp(string url, string productUrl)
        {
            var product = _productService.GetProductByUrl(productUrl, CultureInfo.CurrentCulture.Name);
            if (product == null || url != product.Direction.Url || product.ProductTranslators.FirstOrDefault(c => c.Code == CultureInfo.CurrentCulture.Name)?.Url !=
                productUrl)
                return StatusCode(404);

            var products = _productService.GetProductsByDirectionUrl(url, CultureInfo.CurrentCulture.Name).ToList();
            if (products.Count < 2)
            {
                return StatusCode(404);
            }
            int currentIndex = products.FindIndex(c => c.Url == productUrl);
            ViewBag.Next = products[(currentIndex + 1) % (products.Count)].Url;
            ViewBag.Previous = products[(currentIndex + products.Count - 1) % (products.Count)].Url;
            return View(product);
        }




    }
}
