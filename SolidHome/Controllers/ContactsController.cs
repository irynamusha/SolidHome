﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Localization;
using SolidHome.Service.Interfaces;
using SolidHome.Models;

namespace SolidHome.Controllers
{
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class ContactsController : Controller
    {
        private readonly IMessageService _messageService;
        public ContactsController(IMessageService messageService)
        {
            _messageService = messageService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SendMessage(Message message)
        {
            _messageService.SendMessage(message);
            return PartialView("_MailSuccess");
        }
    }
}
