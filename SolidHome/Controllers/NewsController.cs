﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using SolidHome.Localization;
using SolidHome.Models;
using SolidHome.Service.Interfaces;

namespace SolidHome.Controllers
{
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class NewsController : Controller
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService, IHttpContextAccessor httpContextAccessor)
        {
            CultureInfo.CurrentCulture = new CultureInfo(httpContextAccessor.HttpContext.GetRouteValue("lang") as string ?? "uk");
            _newsService = newsService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult NewsItem(string url, int id)
        {
            List<News> news = _newsService.GetAllNews(CultureInfo.CurrentCulture.Name, true).ToList();
            News model = news.Find(c => c.NewsId == id);
            if (!model.IsPublished)
                return new StatusCodeResult(404);
            AdminNews requiredNews = _newsService.GetNewsById(id);
            if (model.Url != url)
            {
                if (requiredNews.NewsTranslators.Exists(c=>c.Url == url))
                {
                    return RedirectToActionPermanent("NewsItem", new {url = model.Url, id = model.NewsId });
                }
                else return new StatusCodeResult(404);
            }

            int index = news.IndexOf(model);
            ViewBag.Next = news[(index + 1)%(news.Count)];
            ViewBag.Previous = news[(index + news.Count - 1) % (news.Count)];
            ViewBag.LangNews = requiredNews;
            return View(model);
        }

        public IActionResult NewsItemAmp(string url, int id)
        {
            List<News> news = _newsService.GetAllNews(CultureInfo.CurrentCulture.Name, true).ToList();
            News model = news.Find(c => c.NewsId == id);
            if (!model.IsPublished)
                return new StatusCodeResult(404);
            AdminNews requiredNews = _newsService.GetNewsById(id);
            if (model.Url != url)
            {
                if (requiredNews.NewsTranslators.Exists(c => c.Url == url))
                {
                    return RedirectToActionPermanent("NewsItem", new { url = model.Url, id = model.NewsId });
                }
                else return new StatusCodeResult(404);
            }

            int index = news.IndexOf(model);
            ViewBag.Next = news[(index + 1) % (news.Count)];
            ViewBag.Previous = news[(index + news.Count - 1) % (news.Count)];
            ViewBag.LangNews = requiredNews;
            return View(model);
        }
    }
}
