﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using SolidHome.Localization;
using SolidHome.Models;
using SolidHome.Service.Interfaces;

namespace SolidHome.Controllers
{
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class WorksController : Controller
    {
        private readonly IDirectionService _directionService;
        private readonly IWorksService _worksService;

        public WorksController(IDirectionService directionService, IHttpContextAccessor httpContextAccessor, IWorksService worksService)
        {
            CultureInfo.CurrentCulture = new CultureInfo(httpContextAccessor.HttpContext.GetRouteValue("lang") as string ?? "uk");
            _directionService = directionService;
            _worksService = worksService;
        }

        public IActionResult Index()
        {
            var works = _worksService.GetAll(CultureInfo.CurrentCulture.Name);
            ViewBag.Directions = works.SelectMany(c => c.Directions).Distinct(new SmallDirection());
            return View(works);
        }

        public IActionResult Work(string url, int id)
        {
            var work = _worksService.GetWorkById(id);
            if (work == null || work.WorkTranslators.FirstOrDefault(c => c.Code == CultureInfo.CurrentCulture.Name)?.Url != url)
                return StatusCode(404);

            return View(work);
        }
    }
}
