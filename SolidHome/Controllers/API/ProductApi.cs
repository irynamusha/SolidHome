﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using SolidHome.Localization;
using SolidHome.Models;
using SolidHome.Service.Interfaces;

namespace SolidHome.Controllers.API
{
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    [Area("Admin")]
    public class ProductApi : Controller
    {
        private readonly IProductService _productService;

        public ProductApi(IProductService _productService, IHttpContextAccessor httpContextAccessor, IProductService productService)
        {
            this._productService = productService;
            _productService = productService;
            CultureInfo.CurrentCulture = new CultureInfo(httpContextAccessor.HttpContext.GetRouteValue("lang") as string ?? "uk");
        }

        [HttpPost]
        public JsonResult RemoveImageFromProduct(int imageId, int productId)
        {
            _productService.RemoveImageFromProduct(imageId, productId);
            return Json(new {success = true});

        }
    }
}
