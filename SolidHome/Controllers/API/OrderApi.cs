﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SolidHome.Localization;
using SolidHome.Models;
using SolidHome.Service.Interfaces;
using SolidHome.Utils.Service;
using SolidHome.ViewModels;

namespace SolidHome.Controllers.API
{
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class OrderApi : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IProductService _productService;
        private readonly IReviewService _reviewService;
        private static readonly HttpClient _client = new HttpClient();
        private readonly IpService _ipService;
        private readonly EmailService _emailService;
        private readonly IConfiguration _configuration;
        private readonly IHtmlLocalizer<Common> _localizer;

        public OrderApi(IOrderService orderService, IHttpContextAccessor httpContextAccessor, IProductService productService, IpService ipService, EmailService emailService, IConfiguration configuration, IHtmlLocalizer<Common> localizer, IReviewService reviewService)
        {
            _ipService = ipService;
            _emailService = emailService;
            _configuration = configuration;
            _localizer = localizer;
            _reviewService = reviewService;
            _orderService = orderService;
            _productService = productService;
            CultureInfo.CurrentCulture = new CultureInfo(httpContextAccessor.HttpContext.GetRouteValue("lang") as string ?? "uk");
        }

        [HttpPost]
        public IActionResult MakeOrder(Order order)
        {
            if (!GoogleCaptcha.ReCaptchaPassed(
                Request.Form["g-recaptcha-response"], // that's how you get it from the Request object
                _configuration.GetSection("GoogleReCaptcha:secret").Value
            ))
            {
                ViewBag.SiteKey = _configuration.GetSection("GoogleReCaptcha:key").Value;
                ViewBag.Error = _localizer["BotError"];
                ViewBag.Order = order;
                var product = _productService.GetProductByUrl(order.ProductUrl, CultureInfo.CurrentCulture.Name);
                return PartialView("~/Views/Catalog/ModalContent.cshtml",
                    product);
            }
            else
            {
                var a = _orderService.AddOrder(order);
                var product = _productService.GetProductByUrl(order.ProductUrl, CultureInfo.CurrentCulture.Name);
                return PartialView("~/Views/Catalog/_OrderSuccess.cshtml", product);
            }
        }
        [HttpPost]
        public IActionResult OrderCall(string name, string phoneNumber)
        {
            if (!GoogleCaptcha.ReCaptchaPassed(
                Request.Form["g-recaptcha-response"], // that's how you get it from the Request object
                _configuration.GetSection("GoogleReCaptcha:secret").Value
            ))
            {
                ViewBag.SiteKey = _configuration.GetSection("GoogleReCaptcha:key").Value;
                ViewBag.Error = _localizer["BotError"];
                return PartialView("~/Views/Home/_OrderCallForm.cshtml",
                    new OrderCallViewModel {Name = name, PhoneNumber = phoneNumber});
            }
            else
            {
                string message = _emailService.GetCallMessage(name, phoneNumber);
                _emailService.Send("info@solidhome.com.ua", message);
                return PartialView("~/Views/Home/_OrderCallSuccess.cshtml");
            }    
        }
        [HttpPost]
        public IActionResult CreateReview(ReviewCreateViewModel reviewCreateViewModel)
        {
            if (!GoogleCaptcha.ReCaptchaPassed(
                Request.Form["g-recaptcha-response"], // that's how you get it from the Request object
                _configuration.GetSection("GoogleReCaptcha:secret").Value
            ))
            {
                ViewBag.SiteKey = _configuration.GetSection("GoogleReCaptcha:key").Value;
                ViewBag.Error = _localizer["BotError"];
                return PartialView("~/Views/Home/_ReviewCreateForm.cshtml",
                    reviewCreateViewModel);
            }
            else
            {
                _reviewService.AddReview(new Review()
                {
                    Content = reviewCreateViewModel.Content,
                    Email = reviewCreateViewModel.Email,
                    UserName = reviewCreateViewModel.UserName
                    
                });
                return PartialView("~/Views/Home/_ReviewCreateSuccess.cshtml");
            }
        }

       

    }
}
