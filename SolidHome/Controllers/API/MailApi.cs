﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using SolidHome.Localization;
using SolidHome.Models;
using SolidHome.Service.Interfaces;

namespace SolidHome.Controllers.API
{
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class MailApi : Controller
    {
        private readonly IMessageService _messageService;

        public MailApi(IMessageService messageService, IHttpContextAccessor httpContextAccessor, IProductService productService)
        {
            _messageService = messageService;
            CultureInfo.CurrentCulture = new CultureInfo(httpContextAccessor.HttpContext.GetRouteValue("lang") as string ?? "uk");
        }

        [HttpPost]
        public IActionResult SendMail(Message message)
        {
            var a = _messageService.SendMessage(message);
            return PartialView("~/Views/Contacts/_MailSuccess.cshtml");
        }
    }
}
