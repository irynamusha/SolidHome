﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using FluentEmail.Core;
using FluentEmail.Smtp;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SolidHome.Localization;
using SolidHome.Models;
using SolidHome.Utils.Service;

namespace SolidHome.Controllers
{
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    [Route("{lang:lang}")]
    [Route("")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _configuration;
        public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }
        [Route("")]
        public IActionResult Index(bool? redirect)
        {
            ViewBag.SiteKey = _configuration.GetSection("GoogleReCaptcha:key").Value;
            if (redirect.HasValue && redirect.Value)
                return RedirectToActionPermanent("Index", new { redirect = null as object });
            return View();
        }
        [Route("/error/{statusCode}")]
        public IActionResult Error(int statusCode)
        {
            var reExecute = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            _logger.LogInformation($"Unexpected Status Code: {statusCode}, OriginalPath: {reExecute.OriginalPath}");
            return View();
        }
    }
}
