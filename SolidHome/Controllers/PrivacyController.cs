﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Localization;

namespace SolidHome.Controllers
{
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class PrivacyController : Controller

    {
    public IActionResult Index()
    {
        string viewName = "Index_" + CultureInfo.CurrentCulture.Name;
        return View(viewName);
    }
    }
}
