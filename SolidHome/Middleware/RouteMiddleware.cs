﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SolidHome.Middleware
{
    public class RouteMiddleware
    {
        private readonly RequestDelegate _next;

        public RouteMiddleware(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            
            var uri = context.Request.GetUri();
            var url = uri.AbsolutePath;
            Regex rgx = new Regex(@"//+");

			if(!rgx.IsMatch(url) && (!url.EndsWith("/") || url.Length==1))
			{
				await _next.Invoke(context);
			}
            else 
            {
                url = rgx.Replace(url, @"/");
                if (url.EndsWith("/"))
                {
                    url = url.Remove(url.Length - 1);
                }
                context.Response.Redirect($"https://{uri.Authority}{url}");
            }
            
        }
    }
}
