﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Models;
using SolidHome.Service.Interfaces;
using SolidHome.Utils.Service;

namespace SolidHome.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    public class NewsController : Controller
    {
        private readonly INewsService _newsService;
        private readonly ImageService _imageService;
        private readonly SitemapGenerator _sitemapGenerator;

        public NewsController(INewsService newsService, ImageService imageService, SitemapGenerator sitemapGenerator)
        {
            _newsService = newsService;
            _imageService = imageService;
            _sitemapGenerator = sitemapGenerator;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View("EditNews");
        }

        public IActionResult Edit(int id)
        {
            var a = _newsService.GetNewsById(id);
            return View("EditNews", a);
        }

        [HttpPost]
        public IActionResult Edit(AdminNews news, IFormFile image)
        {
            news.Image = image != null ? _imageService.SaveImage(image, "news") : null;
            
            _newsService.UpdateNews(news);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Create(AdminNews news, IFormFile image)
        {
            news.Image = image != null ? _imageService.SaveImage(image, "news") : null;
            var result = _newsService.AddNews(news);
            foreach (Translator t in result.NewsTranslators)
            {
                _sitemapGenerator.Generate(Url.Action("NewsItem", "News", new { id = result.NewsId, url=t.Url, lang=t.Code == "uk" ? "" : "ru", area="" }, "https"), "0.6");
            }
            
            return RedirectToAction("Index");
        }

        
    }
}
