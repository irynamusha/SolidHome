﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using SolidHome.Models;
using SolidHome.Service.Interfaces;
using SolidHome.Utils.Service;

namespace SolidHome.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class CatalogController : Controller
    {
        private readonly IProductService _productService;
        private readonly IDirectionService _directionService;
        private readonly ICatalogService _catalogService;
        private readonly ImageService _imageService;
        private readonly SitemapGenerator _sitemapGenerator;

        public CatalogController(IProductService productService, 
            IDirectionService directionService, 
            ICatalogService catalogService, 
            IHttpContextAccessor httpContextAccessor, 
            IOrderService orderService,
            ImageService imageService, SitemapGenerator sitemapGenerator)
        {
            CultureInfo.CurrentCulture = new CultureInfo(httpContextAccessor.HttpContext.GetRouteValue("lang") as string ?? "uk");
            _productService = productService;
            _directionService = directionService;
            _catalogService = catalogService;
            _imageService = imageService;
            _sitemapGenerator = sitemapGenerator;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Direction(string url)
        {
            var direction = _catalogService.GetCatalogItem(url, "uk");
            return View("CatalogItem", direction);
        }

        public IActionResult Product(int id)
        {
            var product = _productService.GetProductById(id);
            return View(product);
        }
        [Route("/admin/catalog/{url}/createproduct")]
        public IActionResult CreateProduct(string url)
        {
            ViewBag.Url = _directionService.GetDirectionByUrl(url, "uk").Url;
            return View("Product");
        }

        //[HttpGet]
        //public IActionResult Edit(int id)
        //{
        //    var result = _productService.GetAdminProductDescriptionById(id);
        //    return PartialView("~/Areas/Admin/Views/Catalog/_ProductDescription.cshtml", result);

        //}

        [HttpPost]
        [Route("editproduct")]
        public IActionResult EditProduct(AdminProduct product, IFormFile MainImage, List<AdminImage> Alts,  List<IFormFile> PreviewImages)
        {
            if (MainImage != null)
            {
                product.MainImage = _imageService.SaveProductImage(MainImage, "products", product.Direction.Url, "product" + product.ProductId);
            }
            if (product.PreviewImages == null)
            {
                product.PreviewImages = new List<AdminImage>();
            }
            if (PreviewImages != null)
            {
                IList<ImageTranslator> imageTranslators = new List<ImageTranslator>();
                for(int i=0; i<PreviewImages.Count; i++)
                {
                    product.PreviewImages.Add(new AdminImage()
                    {
                        Path = _imageService.SaveProductImage(PreviewImages[i], "products", product.Direction.Url,
                            "product" + product.ProductId),
                        ImageTranslators = Alts[i].ImageTranslators
                    });

                }
            }
            _productService.UpdateProduct(product);
            return RedirectToAction("Direction", new{url= product.Direction.Url });
        }

        [HttpPost]
        public IActionResult AddProduct(AdminProduct product, IFormFile MainImage, List<AdminImage> Alts, List<IFormFile> PreviewImages)
        {
            if (MainImage != null)
            {
                product.MainImage = _imageService.SaveProductImage(MainImage, "products", product.Direction.Url, "product" + product.ProductId);
            }
            product.PreviewImages = new List<AdminImage>();
            if (PreviewImages != null)
            {
                IList<ImageTranslator> imageTranslators = new List<ImageTranslator>();
                for (int i = 0; i < PreviewImages.Count; i++)
                {
                    product.PreviewImages.Add(new AdminImage()
                    {
                        Path = _imageService.SaveProductImage(PreviewImages[i], "products", product.Direction.Url,
                            "product" + product.ProductId),
                        ImageTranslators = Alts[i].ImageTranslators
                    });

                }
            }
            var result = _productService.CreateProduct(product);
            foreach (Translator t in result.ProductTranslators)
            {
                _sitemapGenerator.Generate(Url.Action("Product", "Catalog",  new { productUrl = t.Url, url=result.Direction.Url, lang = t.Code == "uk" ? "" : "ru", area = "" }, "https"), "0.4");
            }
            return RedirectToAction("Product", new {id = result.ProductId});
        }

    }
}
