﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using SolidHome.Service.Interfaces;

namespace SolidHome.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        public OrderController(IHttpContextAccessor httpContextAccessor, IOrderService orderService)
        {
            CultureInfo.CurrentCulture = new CultureInfo(httpContextAccessor.HttpContext.GetRouteValue("lang") as string ?? "uk");
            _orderService = orderService;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
