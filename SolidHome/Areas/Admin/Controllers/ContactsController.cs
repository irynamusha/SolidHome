﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Models;
using SolidHome.Utils.Service;

namespace SolidHome.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class ContactsController : Controller
    {
        private readonly ContactsService _contactsService;

        public ContactsController(ContactsService contactsService)
        {
            _contactsService = contactsService;
        }

        public IActionResult Index()
        {
            Contacts contacts = _contactsService.GetContacts();
            return View(contacts);
        }

        
    }
}
