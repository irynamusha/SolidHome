﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using SolidHome.Models;
using SolidHome.Service.Interfaces;

namespace SolidHome.Areas.Admin.Controllers.API
{
    [Area("Admin")]
    [Authorize]
    public class ProductApi : Controller
    {
        private readonly IProductService _productService;
        private readonly IDirectionService _directionService;
        private readonly ICatalogService _catalogService;

        public ProductApi(IProductService productService, IDirectionService directionService, ICatalogService catalogService, IHttpContextAccessor httpContextAccessor, IOrderService orderService)
        {
            CultureInfo.CurrentCulture = new CultureInfo(httpContextAccessor.HttpContext.GetRouteValue("lang") as string ?? "uk");
            _productService = productService;
            _directionService = directionService;
            _catalogService = catalogService;
        }

        [HttpGet]
        public IActionResult EditDescription(int descriptionId)
        {
            var result = _productService.GetAdminProductDescriptionById(descriptionId);
            return PartialView("~/Areas/Admin/Views/Catalog/_ProductDescription.cshtml", result);
        }

        [HttpGet]
        public IActionResult CreateDescription()
        {
            return PartialView("~/Areas/Admin/Views/Catalog/_ProductDescription.cshtml");

        }

        [HttpPost]
        public IActionResult CreateDescription(AdminProductDescription productDescription)
        {
            productDescription = _productService.CreateProductDescription(productDescription);
            var descriptions = _productService.GetProductById(productDescription.ProductId).ProductDescriptions;

            return PartialView("~/Areas/Admin/Views/Catalog/_ProductDescriptionsTable.cshtml", descriptions);

        }

        [HttpPost]
        public IActionResult EditDescription(AdminProductDescription productDescription)
        {
            _productService.UpdateProductDescription(productDescription);
            var descriptions = _productService.GetProductById(productDescription.ProductId).ProductDescriptions;
            return PartialView("~/Areas/Admin/Views/Catalog/_ProductDescriptionsTable.cshtml", descriptions);
        }

        [HttpPost]
        public IActionResult RemoveDescription(int id)
        {
            var description = _productService.GetAdminProductDescriptionById(id);
            _productService.RemoveProductDescription(id);
            var descriptions = _productService.GetProductById(description.ProductId).ProductDescriptions;
            return PartialView("~/Areas/Admin/Views/Catalog/_ProductDescriptionsTable.cshtml", descriptions);
        }
        
        [HttpPost]
        public IActionResult RemoveProduct(int id)
        {
            var product = _productService.GetProductById(id);
            _productService.RemoveProduct(id);
            var catalogItem = _catalogService.GetCatalogItem(product.Direction.Url, "uk");
            return PartialView("~/Areas/Admin/Views/Catalog/CatalogItem.cshtml", catalogItem);
        }

        [HttpPost]
        public JsonResult RemoveImageFromProduct(int imageId, int productId)
        {
            _productService.RemoveImageFromProduct(imageId, productId);
            return Json(new { success = true });
        }




        
        
    }
}
