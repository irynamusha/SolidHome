﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Service.Interfaces;

namespace SolidHome.Areas.Admin.Controllers.API
{
    [Area("Admin")]
    [Authorize]
    public class ReviewApi : Controller
    {
        private readonly IReviewService _reviewService;

        public ReviewApi(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [HttpPost]
        public JsonResult ChangeApprove(int id)
        {
            _reviewService.ChangeApproveReview(id);
            return new JsonResult(new {success = true});
        }

        [HttpPost]
        public IActionResult RemoveReview(int id)
        {
            _reviewService.RemoveReview(id);
            return PartialView("~/Areas/Admin/Views/Review/Index.cshtml");
        }

        //[HttpPost]

    }
}
