﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Models;
using SolidHome.Utils.Service;

namespace SolidHome.Areas.Admin.Controllers.API
{
    [Area("Admin")]
    [Authorize]
    public class ContactsApi : Controller
    {
        private readonly ContactsService _contactsService;
        public ContactsApi(ContactsService contactsService)
        {
            _contactsService = contactsService;
        }

        [HttpPost]
        public JsonResult SetContacts(Contacts contacts)
        {
            _contactsService.SetContacts(contacts);
            return new JsonResult(new { success = true });
        }
    }
}
