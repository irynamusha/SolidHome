﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Service;
using SolidHome.Service.Interfaces;

namespace SolidHome.Areas.Admin.Controllers.API
{
    [Area("Admin")]
    [Authorize]
    public class OrderApi : Controller
    {
        private readonly IOrderService _orderService;

        public OrderApi(IOrderService orderService)
        {
            _orderService = orderService;
        }
        [HttpPost]
        public IActionResult RemoveOrder(int id)
        {
            _orderService.Remove(id);
            return PartialView("~/Areas/Admin/Views/Order/Index.cshtml");
        }
    }
}
