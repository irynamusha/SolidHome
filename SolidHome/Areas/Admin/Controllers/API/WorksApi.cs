﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Service.Interfaces;

namespace SolidHome.Areas.Admin.Controllers.API
{
    [Area("Admin")]
    [Authorize]
    public class WorksApi : Controller
    {
        private readonly IWorksService _worksService;

        public WorksApi(IWorksService worksService)
        {
            _worksService = worksService;
        }

       [HttpPost]
        public IActionResult RemoveWork(int id)
        {
            var work = _worksService.GetWorkById(id);
            _worksService.Remove(id);
            var works = _worksService.GetAll().ToList();
            return PartialView("~/Areas/Admin/Views/Works/Index.cshtml", works);
        }

        [HttpPost]
        public JsonResult RemoveImageFromWork(int imageId, int workId)
        {
            _worksService.RemoveImageFromWork(imageId, workId);
            return Json(new { success = true });
        }
    }
}
