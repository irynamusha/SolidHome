﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Models;
using SolidHome.Service.Interfaces;

namespace SolidHome.Areas.Admin.Controllers.API
{

    [Authorize]
    [Area("Admin")]
    public class NewsApi : Controller
    {
        private readonly INewsService _newsService;
        public NewsApi(INewsService newsService)
        {
            _newsService = newsService;
        }
        [HttpPost]
        public JsonResult ChangePublication(int id)
        {
            News news = _newsService.ChangePublication(id);
            return new JsonResult(new {success = true});
        }

        [HttpPost]
        public IActionResult RemoveNews(int id)
        {
            _newsService.RemoveNews(id);
            return PartialView("~/Areas/Admin/Views/News/Index.cshtml");
        }
    }
}
