﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Service;
using SolidHome.Service.Interfaces;

namespace SolidHome.Areas.Admin.Controllers.API
{
    [Area("Admin")]
    [Authorize]
    public class MailsApi : Controller
    {
        private readonly IMessageService _messageService;

        public MailsApi(IMessageService messageService)
        {
            _messageService = messageService;
        }
        [HttpPost]
        public IActionResult RemoveMail(int id)
        {
            _messageService.RemoveMessage(id);
            return PartialView("~/Areas/Admin/Views/Mails/Index.cshtml");
        }
    }
}
