﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SolidHome.Models;
using SolidHome.Models.Works;
using SolidHome.Service.Interfaces;
using SolidHome.Utils.Service;

namespace SolidHome.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class WorksController : Controller
    {
        private readonly IWorksService _worksService;
        private readonly  IDirectionService _directionService;
        private readonly ImageService _imageService;
        private readonly UrlService _urlService;
        private readonly SitemapGenerator _sitemapGenerator;

        public WorksController(IWorksService worksService, IDirectionService directionService, ImageService imageService, UrlService urlService, SitemapGenerator sitemapGenerator)
        {
            _worksService = worksService;
            _directionService = directionService;
            _imageService = imageService;
            _urlService = urlService;
            _sitemapGenerator = sitemapGenerator;
        }

        public IActionResult Index()
        {
            var works = _worksService.GetAll().ToList();
            return View(works);
        }

        [Route("/admin/works/{id:int}")]
        public IActionResult Edit(int id)
        {
            var work = _worksService.GetWorkById(id);
            
            return View("Create", work);
        }

        [Route("/admin/works/create")]
        public IActionResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public IActionResult AddWork(AdminWork work, IFormFile MainImage, List<AdminImage> Images, List<int> DirectionsIds, List<IFormFile> WorkImages)
        {
            if (MainImage != null)
            {
                work.MainImage = _imageService.SaveProductImage(MainImage, "works", _urlService.GetEncodedUrlString(work.WorkTranslators?[0].Title));
            }
            work.Directions = DirectionsIds.Select(c => new SmallDirection() {DirectionId = c}).ToList();
            work.WorkImages = new List<AdminImage>();
            if (WorkImages != null)
            {
                IList<ImageTranslator> imageTranslators = new List<ImageTranslator>();
                for (int i = 0; i < WorkImages.Count; i++)
                {
                    work.WorkImages.Add(new AdminImage()
                    {
                        Path = _imageService.SaveProductImage(WorkImages[i], "works", _urlService.GetEncodedUrlString(work.WorkTranslators?[0].Title)),
                        ImageTranslators = Images[i].ImageTranslators
                    });

                }
            }
            var result = _worksService.Add(work);
            foreach (Translator t in result.WorkTranslators)
            {
                _sitemapGenerator.Generate(Url.Action("Work", "Works", new { id = result.WorkId, url = t.Url, lang = t.Code == "uk" ? "" : "ru", area = "" }, "https"), "0.6");
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult EditWork(AdminWork work, IFormFile MainImage, List<AdminImage> Images, List<int> DirectionsIds, List<IFormFile> WorkImages)
        {
            if (MainImage != null)
            {
                work.MainImage = _imageService.SaveProductImage(MainImage, "works", _urlService.GetEncodedUrlString(work.WorkTranslators?[0].Title));
            }
            if (work.WorkImages == null)
            {
                work.WorkImages = new List<AdminImage>();
            }
            work.Directions = DirectionsIds.Select(c => new SmallDirection() { DirectionId = c }).ToList();
            if (WorkImages != null)
            {
                for (int i = 0; i < WorkImages.Count; i++)
                {
                    work.WorkImages.Add(new AdminImage()
                    {
                        Path = _imageService.SaveProductImage(WorkImages[i], "works", _urlService.GetEncodedUrlString(work.WorkTranslators?[0].Title)),
                        ImageTranslators = Images[i].ImageTranslators
                    });

                }
            }
            var result = _worksService.Update(work);
            //foreach (Translator t in result.WorkTranslators)
            //{
            //    _sitemapGenerator.Generate(Url.Action("Work", "Works", new { id = result.WorkId, url = t.Url, lang = t.Code == "uk" ? "" : "ru", area = "" }, "https"), "0.6");
            //}
            return RedirectToAction("Index");
        }
    }
}
