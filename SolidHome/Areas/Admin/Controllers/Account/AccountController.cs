﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using SolidHome.Areas.Admin.Controllers.Account;
using SolidHome.Models;

namespace SolidHome.Areas.Admin.Controllers
{
    
    public class AccountController : Controller
    {
        [HttpGet]
        public IActionResult Login()
        {
            return View("~/Areas/Admin/Views/Account/Login.cshtml");
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect("/Account/Login");
        }

        [HttpPost]
        public async Task<IActionResult> Login(Login loginModel)
        {
            if (LoginUser(loginModel))
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, loginModel.Username)
                };
                var userIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);
                await HttpContext.SignInAsync(principal);
                return Redirect("/admin/news");
            }
            return View("~/Areas/Admin/Views/Account/Login.cshtml");
        }

        private bool LoginUser(Login loginModel)
        {
            if (loginModel.Username == UserCredentials.NAME && loginModel.Password == UserCredentials.PASSWORD)
                return true;
            return false;
        }
    }
}
