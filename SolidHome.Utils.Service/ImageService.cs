﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;

namespace SolidHome.Utils.Service
{
    public class ImageService
    {
        IHostingEnvironment _env;

        public ImageService(IHostingEnvironment env)
        {
            _env = env;
        }

        public string SaveImage(IFormFile image, params string[] folder)
        {
            string imageExtension = Path.GetExtension(image.FileName);
            string originFileName = CreateMD5(image.FileName + image.Length);
            string fullOriginalFileName = originFileName + imageExtension;
            string imageFolderPath = Path.Combine(_env.WebRootPath, "images");
            for (int i = 0; i < folder.Length; i++)
            {
                imageFolderPath = Path.Combine(imageFolderPath, folder[i]);
            }

            using (var stream = new FileStream(Path.Combine(imageFolderPath, fullOriginalFileName), FileMode.Create))
            {
                image.CopyTo(stream);
            }
            ResizeImage(imageFolderPath, fullOriginalFileName, new Size(340, 253));
            return fullOriginalFileName;
        }

        public string SaveProductImage(IFormFile image, params string[] folder)
        {
            string imageExtension = Path.GetExtension(image.FileName);
            string originFileName = CreateMD5(image.FileName + image.Length);
            string fullOriginalFileName = originFileName + imageExtension;
            string imageFolderPath = Path.Combine(_env.WebRootPath, "images");
            string imageRelatedPath = "/images";
            for (int i = 0; i < folder.Length; i++)
            {
                imageRelatedPath = Path.Combine(imageRelatedPath, folder[i]);
                imageFolderPath = Path.Combine(imageFolderPath, folder[i]);
                if (!Directory.Exists(imageFolderPath))
                {
                    Directory.CreateDirectory(imageFolderPath);
                }
            }

            using (var stream = new FileStream(Path.Combine(imageFolderPath, fullOriginalFileName), FileMode.Create))
            {
                image.CopyTo(stream);
            }
            return imageRelatedPath.Replace('\\', '/') + "/" + fullOriginalFileName;
        }

        private void ResizeImage(string imageFolderPath, string originalImageName, Size size)
        {
            using (var img = Image.Load(Path.Combine(imageFolderPath, originalImageName)))
            {
                img.Mutate(x => x.Resize(new ResizeOptions
                {
                    Size = size,
                    Mode = ResizeMode.Crop
                }));

                img.Save(Path.Combine(imageFolderPath, originalImageName));
            }
        }

        private string CreateMD5(string input)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                StringBuilder sb = new StringBuilder();
                foreach (byte t in hashBytes)
                {
                    sb.Append(t.ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}
