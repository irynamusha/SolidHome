﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SolidHome.Models;

namespace SolidHome.Utils.Service
{
    public class JsonService
    {
        public JsonService()
        {

        }
        public T Read<T>(string filePath)
        {
            return JsonConvert.DeserializeObject<T>(File.ReadAllText(filePath));
        }
        public void Write<T>(T model, string filePath)
        {
            File.WriteAllText(filePath, JsonConvert.SerializeObject(model));
        }
    }
}
