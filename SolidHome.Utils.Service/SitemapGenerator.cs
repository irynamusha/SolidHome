﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Microsoft.AspNetCore.Hosting;
using X.Web.Sitemap;

namespace SolidHome.Utils.Service
{
    public class SitemapGenerator
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        public SitemapGenerator(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        public void Generate(string url, string priorityStr)
        {
            
            var xDoc = GetDocument();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            XmlNode node = xDoc.CreateElement("url", xRoot.NamespaceURI);
            var loc = xDoc.CreateElement("loc", xRoot.NamespaceURI);
            loc.AppendChild(xDoc.CreateTextNode(url));
            var lastmod = xDoc.CreateElement("lastmod", xRoot.NamespaceURI);
            lastmod.AppendChild(xDoc.CreateTextNode(DateTime.Now.ToString("O")));
            var priority = xDoc.CreateElement("priority", xRoot.NamespaceURI);
            priority.AppendChild(xDoc.CreateTextNode(priorityStr));
            node.AppendChild(loc);
            node.AppendChild(lastmod);
            node.AppendChild(priority);
            xRoot.AppendChild(node);
            Save(xDoc);

        }

        private XmlDocument GetDocument()
        {
            XmlDocument xDoc = new XmlDocument();
            string filepath = Path.Combine(_hostingEnvironment.WebRootPath, "sitemap.xml");
            if (File.Exists(filepath))
            {
                xDoc.Load(filepath);
            }
            return xDoc;
        }

        private void Save(XmlDocument document)
        {
            string filepath = Path.Combine(_hostingEnvironment.WebRootPath, "sitemap.xml");
            document.Save(filepath);
        }
    }
}
