﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using SolidHome.Models;

namespace SolidHome.Utils.Service
{
    public class ContactsService
    {
        private readonly JsonService _jsonService;
        private readonly CacheService _cacheService;
        private readonly IHostingEnvironment _env;

        public ContactsService(JsonService jsonService, CacheService cacheService, IHostingEnvironment env)
        {
            _jsonService = jsonService;
            _cacheService = cacheService;
            _env = env;
        }

        public Contacts GetContacts()
        {
            var result = _cacheService.GetValue("Contacts");
            string FolderPath = _env.ContentRootPath;
            if (result != null)
            {
                return (Contacts) result;
            }
            else
            {
               Contacts contacts = _jsonService.Read<Contacts>(Path.Combine(FolderPath, "contacts.json"));
               _cacheService.SetValue("Contacts", contacts, TimeSpan.FromDays(7));
                return contacts;
            }
        }

        public void SetContacts(Contacts contacts)
        {
            _jsonService.Write<Contacts>(contacts, (Path.Combine(_env.ContentRootPath, "contacts.json")));
            _cacheService.SetValue("Contacts", contacts, TimeSpan.FromDays(7));
        }
         
    }
}
