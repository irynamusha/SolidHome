﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using SolidHome.Models;
using UnidecodeSharpFork;

namespace SolidHome.Utils.Service
{
    public class UrlService
    {
        public string GetEncodedUrlString(string url)
        {
            return (Regex.Replace(SimplifyString(url.Unidecode()), @"\s+", "-")).ToLower();
        }

        private string SimplifyString(string str)
        {
            StringBuilder sb = new StringBuilder();
            while (str.EndsWith(' '))
            {
                str = str.Remove(str.Length - 1, 1);
            }
            while (str.StartsWith(' '))
            {
                str = str.Remove(0, 1);
            }
            foreach (var c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public string GetPostRelativePath(News news)
        {
            return $"/news/{news.Url}-{news.NewsId}";
        }
    }
}
