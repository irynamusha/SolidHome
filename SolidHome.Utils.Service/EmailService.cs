﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using SolidHome.Models;

namespace SolidHome.Utils.Service
{
    public class EmailService
    {
        public string GetOrderMessage(Order order)
        {
            order.Message = String.IsNullOrEmpty(order.Message) ? "відсутній" : order.Message;
            return $"Доброго дня, Павло! У вас нове замовлення. " +
                   $"\nТовар: {order?.ProductName};" +
                   (String.IsNullOrEmpty(order.Clarification) ? "" : $"\nДеталі: {order?.Clarification}; ")+
                   $"\nЗамовник: {order?.Name}; " +
                   $"\nНомер Телефону: {order?.Phone}; " +
                   $"\nE-mail: {order?.Email}; " +
                   $"\nКоментар: {order?.Message}; " +
                   $"\nПосилання: {order.Url}";
        }

        public string GetCallMessage(string name, string phoneNumber)
        {
            return $"Доброго дня, Павло! {name} замовив(ла) дзвінок на номер {phoneNumber}. Зателефонуйте будь ласка.";
        }

        public void Send(string email, string message)
        {
            var smtp = new SmtpClient
            {
                Port = 587,
                Host = "smtp.gmail.com",
                EnableSsl = true,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential("solidhome2018@gmail.com", "solidhome2018!")
            };

            MailMessage mm = new MailMessage("solidhome2018@gmail.com", email, "Замовлення", message);
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            smtp.Send(mm);
        }
    }
}
